Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__#cpu_profile=all,
	#measurement_directory: ./control_cpmem_cpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92ef1a20>
}
Command: /home/root/stage/thermobench --name data__#cpu_profile=all --output_dir ./control_cpmem_cpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: little-one,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__#cpu_profile=little-one,
	#measurement_directory: ./control_cpmem_cpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92ef1a20>
}
Command: /home/root/stage/thermobench --name data__#cpu_profile=little-one --output_dir ./control_cpmem_cpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 0, 0, 0, 0, 0]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: little-all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__#cpu_profile=little-all,
	#measurement_directory: ./control_cpmem_cpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92ef1a20>
}
Command: /home/root/stage/thermobench --name data__#cpu_profile=little-all --output_dir ./control_cpmem_cpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 0, 0]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: big-one,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__#cpu_profile=big-one,
	#measurement_directory: ./control_cpmem_cpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92ef1a20>
}
Command: /home/root/stage/thermobench --name data__#cpu_profile=big-one --output_dir ./control_cpmem_cpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [0, 0, 0, 0, 1, 0]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: big-all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__#cpu_profile=big-all,
	#measurement_directory: ./control_cpmem_cpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92ef1a20>
}
Command: /home/root/stage/thermobench --name data__#cpu_profile=big-all --output_dir ./control_cpmem_cpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [0, 0, 0, 0, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Setting fan speed to 0.0
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
