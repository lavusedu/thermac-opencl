include("common.jl")

using Gnuplot, Printf, Statistics, Measurements, Colors
const M = Measurements

# const PLOT_PNG_SIZE = "3200,1800"
const PLOT_PNG_SIZE = "1600,900"
Gnuplot.options.gpviewer = false
Gnuplot.options.init = [linetypes(:Set1_5)]

mutable struct PlotContext
	session::Symbol
	layout::Tuple{Int64, Int64}
	figure_index::Union{Nothing, Int64}

	yrange::Union{Nothing, Tuple{Float64, Float64}}
	y2range::Union{Nothing, Tuple{Float64, Float64}}
	cbrange::Union{Nothing, Tuple{Float64, Float64}}

	x_ticks_angle::Real
	x_ticks_size::Int
	
	file_name::Union{Nothing, String}
	dir_path::Union{Nothing, String}

	work_idle_split::Bool

	PlotContext(session::Symbol) = PlotContext(session, (1, 1))
	PlotContext(session::Symbol, layout::Tuple{Int64, Int64}) = new(
		session,
		layout,
		layout[1] * layout[2],
		# yrange, y2range, cbrange
		nothing, nothing, nothing,
		# x_tics_angle, x_ticks_size
		0, 19,
		# file_name, dir_path
		nothing, nothing,
		# work_idle_split
		false
	)
end
function all_figures_filled(plot::PlotContext)::Bool
	return plot.figure_index == plot.layout[1] * plot.layout[2]
end
function next_figure(plot::PlotContext)::Int64
	if all_figures_filled(plot)
		plot.figure_index = 1
		@gp plot.session "set multiplot layout $(plot.layout[1]),$(plot.layout[2])"
	else
		plot.figure_index += 1
	end

	return plot.figure_index
end
function setup_figure_common(plot::PlotContext)
	@gp plot.session :- "set key default" :-
	@gp plot.session :- "set key font \",15\"" :-
	@gp plot.session :- "unset grid" :-
	
	@gp plot.session :- "unset xlabel" :-
	@gp plot.session :- "set xlabel font \",17\"" :-
	@gp plot.session :- "unset xtics" :-
	@gp plot.session :- "unset mxtics" :-
	@gp plot.session :- "set xtics font \",$(plot.x_ticks_size)\"" :-
	@gp plot.session :- "unset xrange" :-

	@gp plot.session :- "unset ylabel" :-
	@gp plot.session :- "unset yrange" :-
	# @gp plot.session :- "unset ytics" :-

	@gp plot.session :- "unset mytics" :-
	@gp plot.session :- "unset y2tics" :-
	@gp plot.session :- "unset y2label" :-
	@gp plot.session :- "unset y2range" :-

	@gp plot.session :- "unset cbrange" :-

	@gp plot.session :- "set xtics rotate by $(plot.x_ticks_angle)" :-
	# if plot.x_ticks_angle < 0
	# 	@gp plot.session :- "set xtics center offset 0,-1" :-
	# end

	if plot.yrange !== nothing
		@gp plot.session :- "set yrange [$(plot.yrange[1]):$(plot.yrange[2])]" :-
	end
	if plot.y2range !== nothing
		@gp plot.session :- "set y2range [$(plot.y2range[1]):$(plot.y2range[2])]" :-
	end
	if plot.cbrange !== nothing
		@gp plot.session :- "set cbrange [$(plot.cbrange[1]):$(plot.cbrange[2])]" :-
	end
end

function save_plot(plot::PlotContext, file_name, force_save = false)
	if !force_save && !all_figures_filled(plot)
		return
	end

	@gp plot.session

	if plot.file_name !== nothing
		file_name = plot.file_name
	end

	dir = "plots"
	if plot.dir_path !== nothing
		dir = plot.dir_path
	end

	output = joinpath(dir, "$(file_name).png")
	println("Saving \"$output\"")

	mkpath(dir)
	Gnuplot.save(
		plot.session,
		term = "pngcairo size $PLOT_PNG_SIZE font \"Sans,20\"",
		output = output
	)
end

struct PlotColor
	inner
end
function Base.show(io::IO, v::PlotColor)
	print(io, string(v))
end
function Base.string(v::PlotColor)
	hex_str = hex(v.inner)
	return "rgb \"#$(hex_str)\""
end
function from_gnuplot(x::Int64)::PlotColor
	if x == -1
		return PlotColor(colorant"#000000")
	elseif x == 0
		return PlotColor(colorant"#E3E3E3")
	end
	
	x = (x - 1) % 5
	if x == 0
		return PlotColor(colorant"#D1352B")
	elseif x == 1
		return PlotColor(colorant"#4A7DB3")
	elseif x == 2 
		return PlotColor(colorant"#68AD57")
	elseif x == 3
		return PlotColor(colorant"#8E529F")
	else
		return PlotColor(colorant"#EE8632")
	end
end
function lighten(c::PlotColor, f::Float64)::PlotColor
	hsl = convert(HSL{Float32}, c.inner)
	PlotColor(HSL(
		hsl.h,
		hsl.s,
		clamp(hsl.l + f, 0.0, 1.0)
	))
end

function _generate_indices_and_labels(
	datas::Vector{Thermobench.Data},
	param::Symbol
)::Tuple{Dict{String, Int}, Vector{String}}
	all_params = map(data -> data.meta[:params][param], datas)
	sort!(
		all_params,
		by = param -> param.sort_key
	)

	index_map::Dict{String, Int} = Dict()
	labels::Vector{String} = []

	index = 0
	for param_data in all_params
		key = string(param_data.value)
		if !haskey(index_map, key)
			index_map[key] = index
			push!(labels, string(param_data))
			index += 1
		end
	end

	return (index_map, labels)
end
function _generate_tics_string(labels, indices = nothing)
	if indices === nothing
		indices = 0 : (length(labels) - 1)
	end
	
	return join(
		map(
			z -> string('"', z[2], '"', ' ', z[1]),
			zip(indices, labels)
		),
		", "
	)
end

function _generate_x_axis_with_primary_param(
	measurement::MeasurementContext
)::Tuple{Vector{Real}, String, String}
	index_map, labels = _generate_indices_and_labels(measurement.data, measurement.primary_param)
	
	indices = 0 : (length(labels) - 1)
	tics = _generate_tics_string(labels, indices)

	param_name::String = first(measurement.data).meta[:params][measurement.primary_param].name

	return (
		indices,
		tics,
		param_name
	)
end

function plot_dummy(
	plot::PlotContext
)
	figure_index = next_figure(plot)
	@gp plot.session :- figure_index title = "" :-
	setup_figure_common(plot)

	@gp plot.session :- "unset xtics" :-
	@gp plot.session :- "unset ytics" :-
	
	@gp plot.session :- "set key off" :-
	@gp plot.session :- "set xrange [-1:1]" :-
	@gp plot.session :- "set yrange [-1:1]" :-

	@gp plot.session :- "plot sqrt(-1)" :-

	save_plot(plot, "dummy")
end

struct BarsPart
	data::Vector{Measurement}
	title::String
	color::PlotColor
	fill_opacity::Real

	BarsPart(
		data, title::String;
		color::PlotColor = from_gnuplot(1),
		fill_opacity::Real = 1.0
	) = new(data, title, color, fill_opacity)
end
function _plot_bars_inner(
	plot::PlotContext, x_axis, y_parts::Vector{BarsPart};
	bar_type::String = "rowstacked"
)
	y_datas = []
	y_mins = nothing
	use_strings = []

	if bar_type == "rowstacked"
		@gp plot.session :- "set style histogram rowstacked" :-
		
		y_datas = [M.value.(part.data) for part in y_parts]
		use_strings = ["$(i + 1)" for i in range(1, length(y_datas))]
	elseif bar_type == "cluster"
		@gp plot.session :- "set style histogram cluster" :-

		y_datas = [M.value.(part.data) for part in y_parts]
		use_strings = ["$(i + 1)" for i in range(1, length(y_datas))]
	elseif bar_type == "errorbars"
		@gp plot.session :- "set style histogram errorbars lw 1 gap 0" :-

		y_mins = []
		for part in y_parts
			push!(y_datas, M.value.(part.data))
			push!(y_datas, M.uncertainty.(part.data))
			push!(y_mins, min(part.data...))
		end
		use_strings = ["$(i * 2):$(i * 2 + 1)" for i in range(1, length(y_datas))]
	else
		throw(DomainError(bar_type, "argument must be one of [rowstacked, cluster, errorbars]"))
	end

	@gp plot.session :- "set key outside above" :-
	@gp plot.session :- "set style data histogram" :-
	@gp plot.session :- "set style fill solid border -1" :-
	@gp plot.session :- "set boxwidth 0.75" :-
	# @gp plot.session :- "set xtics rotate by $(plot.x_ticks_angle)" :-

	axes = "x1y1"
	if plot.yrange === nothing
		@gp plot.session :- "set yrange [0:]" :-		
	end

	data_name = "\$data_$(plot.figure_index)_$(axes)"
	@gp plot.session :- data_name => (x_axis, y_datas...)

	@gp plot.session :- "plot newhistogram ''" :-
	for (i, part) in enumerate(y_parts)
		println("$(plot.figure_index): $(part.title) = $(M.value.(part.data))")
		@gp plot.session :- "plot $data_name using $(use_strings[i]) title '$(part.title)' axes $axes lc $(part.color) fs solid $(part.fill_opacity)" :-

		if y_mins !== nothing
			bottom = M.value(y_mins[i]) - M.uncertainty(y_mins[i])
			top = M.value(y_mins[i]) + M.uncertainty(y_mins[i])

			@gp plot.session :- [first(x_axis) - 1, last(x_axis) + 1] [top, top] "with lines lw 1 lc rgb \"#77000000\" notitle" :-
			@gp plot.session :- [first(x_axis) - 1, last(x_axis) + 1] [bottom, bottom] "with filledcurves y=$(top) fs transparent solid 0.3 lw 2 lc rgb \"#000000\" notitle" :-
		end
	end
end

function plot_time_bars(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	y_parts = []
	if plot.work_idle_split
		y_parts = [
			BarsPart([total_time_work(data) for data in measurement.data], "Time work"; color = from_gnuplot(3)),
			BarsPart([total_time_idle(data) for data in measurement.data], "Time idle"; color = from_gnuplot(3), fill_opacity = 0.6)
		]
	else
		y_parts = [
			BarsPart([total_time(data) for data in measurement.data], "Time"; color = from_gnuplot(3))
		]
	end

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'Total time'" :-

	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Time [s]'" :-
	_plot_bars_inner(plot, x_axis, y_parts)

	save_plot(plot, "$(measurement.title) - time_bars")
end

function plot_energy_bars(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	y_parts = []
	if plot.work_idle_split
		y_parts = [
			BarsPart([total_energy_work(data) for data in measurement.data], "Energy work"; color = from_gnuplot(5)),
			BarsPart([total_energy_idle(data) for data in measurement.data], "Energy idle"; color = from_gnuplot(5), fill_opacity = 0.6)
		]
	else
		y_parts = [
			BarsPart([total_energy(data) for data in measurement.data], "Energy"; color = from_gnuplot(5))
		]
	end

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'Total energy'" :-
	
	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Energy [J]'" :-
	_plot_bars_inner(plot, x_axis, y_parts)

	save_plot(plot, "$(measurement.title) - energy_bars")
end

function plot_temperature_bars(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	y_parts = [
		BarsPart([temperature_inf(temperature_data(data)) for data in measurement.data], "Temperature_{inf}"; color = from_gnuplot(6))
	]

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'T_{inf} above ambient'" :-
	
	@gp plot.session :- "set mytics 4" :-
	@gp plot.session :- "set grid ytics mytics" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Temperature [°C]'" :-
	_plot_bars_inner(plot, x_axis, y_parts; bar_type = "errorbars")

	save_plot(plot, "$(measurement.title) - temperature_bars")
end

struct LinesPart
	data::Vector{Measurement}
	title::String
	line_color::PlotColor
	dash_type::Int
	line_width::Int

	LinesPart(
		data, title::String;
		color::PlotColor = from_gnuplot(1), dash::Int = 1, width::Int = 2
	) = new(data, title, color, dash, width)
end
function _plot_lines_inner(
	plot::PlotContext,
	x_axis,
	y_parts::Vector{LinesPart}
)
	@gp plot.session :- "set xrange [$(first(x_axis) - 0.5):$(last(x_axis) + 0.5)]" :-

	for (i, part) in enumerate(y_parts)
		@gp plot.session :- x_axis M.value.(part.data) M.uncertainty.(part.data) "with yerrorlines title '$(part.title)' lt 2 lw $(part.line_width) ps 0 lc $(part.line_color) dt $(part.dash_type)" :-
		
		_, min_idx = findmin(part.data)
		@gp plot.session :- [x_axis[min_idx]] M.value(part.data[min_idx]) "with points notitle pt 6 ps 1 lc -1" :-
	end
end

function plot_time_per_kernel(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'Time/Kernel'" :-

	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set key left" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Time per kernel [s]'" :-

	y_parts = []
	if plot.work_idle_split
		y_parts = [
			BarsPart([total_time_work(data) / total_ops(data) for data in measurement.data], "Time work"; color = from_gnuplot(3))
		]
	else
		# TODO: / total_ops(data) wrong?
		y_parts = [
			BarsPart([total_time(data) / total_ops(data) for data in measurement.data], "Time"; color = from_gnuplot(3))
		]
	end

	_plot_bars_inner(plot, x_axis, y_parts; bar_type = "errorbars")

	save_plot(plot, "$(measurement.title) - energy_per_time")
end

function plot_energy_per_kernel(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'Energy/Kernel'" :-

	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set key left" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Energy per kernel [J]'" :-

	y_parts = []
	if plot.work_idle_split
		y_parts = [
			BarsPart([total_energy_work(data) / total_ops(data) for data in measurement.data], "Energy work"; color = from_gnuplot(5))
		]
	else
		# TODO: / total_ops(data) wrong?
		y_parts = [
			BarsPart([total_energy(data) / total_ops(data) for data in measurement.data], "Energy"; color = from_gnuplot(5))
		]
	end

	_plot_bars_inner(plot, x_axis, y_parts; bar_type = "errorbars")
	
	save_plot(plot, "$(measurement.title) - energy_per_kernel")
end

function plot_power_bars(
	plot::PlotContext,
	measurement::MeasurementContext
)
	x_axis, x_tics, param_name = _generate_x_axis_with_primary_param(measurement)

	# power = 

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index "set title" :-
	setup_figure_common(plot)
	@gp plot.session :- "set key title 'Power'" :-

	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set key left" :-
	@gp plot.session :- "set xtics nomirror ($x_tics)" :-
	
	@gp plot.session :- "set xlabel '$(param_name)'" :-
	@gp plot.session :- "set ylabel 'Power [W]'" :-

	y_parts = []
	if plot.work_idle_split
		y_parts = [
			BarsPart([total_power_watts2(data.df, true) for data in measurement.data], "Power work"; color = from_gnuplot(9))
		]
	else
		y_parts = [
			BarsPart([total_power_watts2(data) for data in measurement.data], "Power"; color = from_gnuplot(9))
		]
	end

	_plot_bars_inner(plot, x_axis, y_parts; bar_type = "errorbars")
	
	save_plot(plot, "$(measurement.title) - power_lines")
end

function _plot_temp_inner(
	plot::PlotContext,
	data::Thermobench.Data,
	column::Symbol,
	temperature_name::String,
	line_color::PlotColor
)
	temp_avg_time = 500.0

	temp_data = temperature_data(data, column)
	temp_inf = temperature_inf(temp_data)
	temp_avg = temperature_avg(temp_data, temp_avg_time)

	@gp plot.session :- temp_data.time (temp_data.temperature .- temp_data.ambient) "with lines lw 1 dt 1 lc $(lighten(line_color, 0.20)) title '$(temperature_name) temp. Δ'" :-
	# @gp plot.session :- temp_data.time temp_data.ambient "with lines lw 1 dt 1 lc $(lighten(line_color, 0.20)) title '$(temperature_name) ambient'" :-

	val = M.value(temp_inf)
	@gp plot.session :- [first(data.time), last(data.time)] [val, val] "with lines lw 2 dt 2 lc $(line_color) notitle" :-

	# @gp plot.session :- [max(first(data.time), last(data.time) - temp_avg_time), last(data.time)] [temp_avg, temp_avg] "with lines lw 2 dt 1 lc $(line_color) notitle" :-
end

function _plot_individual_progress_temps(
	plot::PlotContext,
	data::Thermobench.Data,
	primary_param::Symbol
)
	figure_index = next_figure(plot)
	data_param = data.meta[:params][primary_param]
	@gp plot.session :- figure_index title = "Temperature - $(data_param.name) = $(data_param)" :-
	setup_figure_common(plot)

	@gp plot.session :- "set ylabel 'Temperature [°C]'" :-
	@gp plot.session :- "set ytics" :-
	@gp plot.session :- "set key bottom right" :-
	
	cpu_little_cols = r"CPU[0-3]_load_"
	if size(data.df[!, cpu_little_cols])[2] > 0
		_plot_temp_inner(plot, data, :CPU_0_temp, "CPU LITTLE", from_gnuplot(2))
	end

	cpu_big_cols = r"CPU[4-5]_load_"
	if size(data.df[!, cpu_big_cols])[2] > 0
		_plot_temp_inner(plot, data, :CPU_1_temp, "CPU big", from_gnuplot(3))
	end

	_plot_temp_inner(plot, data, :GPU_0_temp, "GPU 0", from_gnuplot(4))
	_plot_temp_inner(plot, data, :GPU_1_temp, "GPU 1", from_gnuplot(5))
end

function _plot_individual_progress_cpuload(
	plot::PlotContext,
	data::Thermobench.Data,
	primary_param::Symbol
)
	figure_index = next_figure(plot)
	data_param = data.meta[:params][primary_param]
	@gp plot.session :- figure_index title = "CPU load - $(data_param.name) = $(data_param)" :-
	setup_figure_common(plot)

	@gp plot.session :- "set ylabel 'CPU load [%]'" :-
	@gp plot.session :- "set yrange [-10:110]" :-
	@gp plot.session :- "set grid ytics" :-
	@gp plot.session :- "set ytics" :-

	# cpu load columns are missing when the cpu wasn't on
	cpu_load_sum = (args...) -> sum(args) / length(args)
	
	cpu_little_cols = r"CPU[0-3]_load_"
	if size(data.df[!, cpu_little_cols])[2] > 0
		cpu_little = select(
			data.df, :time, cpu_little_cols => cpu_load_sum => :cpu_load
		) |> dropmissing

		@gp plot.session :- cpu_little.time cpu_little.cpu_load "with lines lw 1 lt 3 title 'CPU LITTLE load'" :-
	end
	
	cpu_big_cols = r"CPU[4-5]_load_"
	if size(data.df[!, cpu_big_cols])[2] > 0
		cpu_big = select(
			data.df, :time, cpu_big_cols => cpu_load_sum => :cpu_load
		) |> dropmissing

		@gp plot.session :- cpu_big.time cpu_big.cpu_load "with lines lw 1 lt 2 title 'CPU big load'" :-
	end
end

function _plot_individual_progress_power(
	plot::PlotContext,
	data::Thermobench.Data,
	primary_param::Symbol
)
	figure_index = next_figure(plot)
	data_param = data.meta[:params][primary_param]
	@gp plot.session :- figure_index title = "Power - $(data_param.name) = $(data_param)" :-
	setup_figure_common(plot)

	@gp plot.session :- "set ylabel 'Power [W]'" :-
	@gp plot.session :- "set key center right" :-

	data_power = power_watts(data)
	power_average = total_power_watts2(data)

	color = from_gnuplot(5)
	@gp plot.session :- data.time data_power "with lines lw 1 lt $(lighten(color, 0.20)) title 'Power'" :-
	@gp plot.session :- [first(data.time), last(data.time)] [POWER_IDLE, POWER_IDLE] "with lines lw 1 dt 2 lc $(color) title 'Power Idle'" :-
	@gp plot.session :- [first(data.time), last(data.time)] [M.value(power_average), M.value(power_average)] "with lines lw 2 dt 2 lc $(color) title 'Power Avg.'" :-
end

function plot_individual_progress(
	plot_session::Symbol,
	measurement::MeasurementContext
)
	layout_width = 3
	layout_height = length(measurement.data)
	plot = PlotContext(plot_session, (layout_height, layout_width))
	# plot.yrange = (0, 5000)

	for data in measurement.data
		_plot_individual_progress_temps(plot, data, measurement.primary_param)
		_plot_individual_progress_cpuload(plot, data, measurement.primary_param)
		_plot_individual_progress_power(plot, data, measurement.primary_param)
		# plot_dummy(plot)
	end

	save_plot(plot, "$(measurement.title) - individual_progress", true)
end
plot_individual_progress(measurement::MeasurementContext) = plot_individual_progress(Symbol(measurement.title * " individual"), measurement)

function _plot_isolated_runs_inner(
	plot::PlotContext,
	measurement::MeasurementContext,
	use_work_done::Bool,
	title::String,
	ylabel::String,
	column_name::Symbol
)
	figure_index = next_figure(plot)
	@gp plot.session :- figure_index title = title :-
	setup_figure_common(plot)

	@gp plot.session :- "set ylabel '$(ylabel)'" :-

	colors::Vector{PlotColor} = [
		PlotColor(colorant"#FF0000"),
		PlotColor(colorant"#00FF00"),
		PlotColor(colorant"#0000FF"),
		PlotColor(colorant"#FFFF00"),
		PlotColor(colorant"#FF00FF"),
		PlotColor(colorant"#00FFFF"),
		PlotColor(colorant"#3CD097"),
		PlotColor(colorant"#D0973C"),
		PlotColor(colorant"#973CD0"),
		PlotColor(colorant"#D04D3C"),
		PlotColor(colorant"#65AFDA"),
		PlotColor(colorant"#AFDA65"),
		PlotColor(colorant"#DA65AF"),
	]
	for (index, data) in enumerate(measurement.data)
		isolated = _isolate_work_ranges(data.df, use_work_done)

		data_x = collect(range(1, length(isolated)))
		data_y = [last(data[!, column_name]) for data in isolated]
		data_y_mean = Statistics.mean(data_y)
		data_y_std = Statistics.std(data_y)

		title = data.meta[:params][measurement.primary_param]
		# title = join(values(data.meta[:params]), " x ")
		color = colors[(index - 1) % length(colors) + 1]

		@gp plot.session :- data_x [y - data_y_mean for y in data_y] "with linespoints dt 3 lc $(color) title '$(title)'" :-
		
		# @gp plot.session :- [first(data_x), last(data_x)] [data_y_mean, data_y_mean] "with lines lw 2 lc $(color) notitle" :-

		@gp plot.session :- [first(data_x), last(data_x)] [-data_y_std, -data_y_std] "with filledcurves y=$(data_y_std) fs transparent solid 0.2 lw 2 lc $(color) notitle" :-
	end
end

function plot_isolated_runs(
	plot_session::Symbol,
	measurement::MeasurementContext,
	use_work_done::Bool
)
	layout_width = 2
	layout_height = 1
	plot = PlotContext(plot_session, (layout_height, layout_width))

	_plot_isolated_runs_inner(plot, measurement, use_work_done, "Time", "Time [s]", :time)
	_plot_isolated_runs_inner(plot, measurement, use_work_done, "Energy", "Energy [J]", :energy)

	save_plot(plot, "$(measurement.title) - isolated_runs", true)
end

function plot_energy_time_scatter(
	plot::PlotContext,
	measurements::Vector{MeasurementContext},
	plot_title = "Energy Time Scatter"
)
	# diag_max_x = missing
	# diag_avg_sum_x = 0
	# diag_avg_sum_y = 0
	# diag_avg_count = 0

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index title = plot_title
	setup_figure_common(plot)

	for meas in measurements
		# first we find the point where all params have the default value
		base_data = get_base_data(meas)
		
		x_axis = missing
		y_axis = missing
		if plot.work_idle_split
			x_axis = [total_time_work(data) for data in meas.data]
			y_axis = [total_energy_work(data) for data in meas.data]
		else
			x_axis = [total_time(data) / total_time(base_data) for data in meas.data]
			y_axis = [total_energy(data) / total_energy(base_data) for data in meas.data]
		end

		# if diag_max_x === missing
		# 	diag_max_x = max(x_axis...)
		# else
		# 	diag_max_x = max(diag_max_x, x_axis...)
		# end
		# diag_avg_sum_x += sum(x_axis)
		# diag_avg_sum_y += sum(y_axis)
		# diag_avg_count += length(x_axis)

		@gp plot.session :- "set xlabel 'Time [s]'" :-
		@gp plot.session :- "set ylabel 'Energy [J]'" :-
		@gp plot.session :- M.value.(x_axis) M.value.(y_axis) "title '$(meas.title)' with points ps 1" :-
	end

	# diag_avg_x = diag_avg_sum_x / diag_avg_count
	# diag_avg_y = diag_avg_sum_y / diag_avg_count
	# diag_scaler = diag_max_x / diag_avg_x * 1.1
	# x_diag = [0, diag_avg_x * diag_scaler]
	# y_diag = [0, diag_avg_y * diag_scaler]
	@gp plot.session :- [0, 1, 2] [0, 1, 2] "notitle with line lw 1 lt 0" :-
	@gp plot.session :- "set arrow from 1,0 to 1,2 nohead lw 1 lt 0" :-
	@gp plot.session :- "set arrow from 0,1 to 2,1 nohead lw 1 lt 0" :-

	save_plot(plot, "energy_time_scatter_multiple")
end

function plot_2d_matrix_plot(
	plot::PlotContext,
	measurement::MeasurementContext,
	x_param::Symbol,
	y_param::Symbol,
	z_value_type::Symbol = :energy
)
	x_index_map, x_labels = _generate_indices_and_labels(measurement.data, x_param)
	x_tics = _generate_tics_string(x_labels)
	x_param_name = first(measurement.data).meta[:params][x_param].name
	width = length(x_labels)

	y_index_map, y_labels = _generate_indices_and_labels(measurement.data, y_param)
	y_tics = _generate_tics_string(y_labels)
	y_param_name = first(measurement.data).meta[:params][y_param].name
	height = length(y_labels)

	title = "Time per kernel [s]"
	if z_value_type === :energy
		title = "Energy per kernel [J]"
	elseif z_value_type === :temperature
		title = "Temperature [°C]"
	elseif z_value_type === :power
		title = "Power [W]"
	else
		title = "Time per kernel [s]"
	end

	figure_index = next_figure(plot)
	@gp plot.session :- figure_index title = title
	setup_figure_common(plot)

	@gp plot.session :- "set xtics ($x_tics)" :-
	@gp plot.session :- "set ytics ($y_tics)" :-

	@gp plot.session :- "set xlabel '$(x_param_name)'" :-
	@gp plot.session :- "set ylabel '$(y_param_name)'" :-

	data_matrix = fill(NaN, (width, height))

	for data in measurement.data
		x_value = x_index_map[string(data.meta[:params][x_param])]
		y_value = y_index_map[string(data.meta[:params][y_param])]
		
		if z_value_type === :energy
			z_value = total_energy_work(data) / total_ops(data)
		elseif z_value_type === :temperature
			z_value = temperature_inf(temperature_data(data))
		elseif z_value_type === :power
			z_value = total_power_watts2(data.df, true)
		else
			z_value = total_time_work(data) / total_ops(data)
		end

		data_matrix[x_value + 1, y_value + 1] = M.value(z_value)
	end

	dataset_lines::Vector{String} = []
	
	# first line
	line = "0"
	for x in 1:width
		line *= string(' ', x - 1)
	end
	push!(dataset_lines, line)

	# data lines including first column
	for y in 1:height
		line = "$(y - 1)"
		for x in 1:width
			line *= string(' ', data_matrix[x, y])
		end
		push!(dataset_lines, line)
	end

	# special case
	if height == 1
		line = "1"
		for x in 1:width
			line *= string(' ', data_matrix[x, 1])
		end
		push!(dataset_lines, line)
	end

	@gp plot.session :- Gnuplot.DatasetText(dataset_lines) "matrix nonuniform with image notitle" :-

	save_plot(plot, "$(measurement.title) - $(x_param)x$(y_param) matrix")
end
