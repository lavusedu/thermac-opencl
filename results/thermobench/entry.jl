include("plotting.jl")

function _entry_common(
	meas::MeasurementContext;
	x_ticks_angle::Real = 0,
	x_ticks_size::Int = 19
)
	plot = PlotContext(Symbol(meas.title), (2, 3))
	plot.file_name = meas.title
	plot.x_ticks_angle = x_ticks_angle
	plot.x_ticks_size = x_ticks_size
	
	plot.work_idle_split = false
	plot.yrange = nothing #(0, 450)
	plot_time_bars(plot, meas)
	
	plot.yrange = nothing #(0, 3000)
	plot_energy_bars(plot, meas)
	
	plot.work_idle_split = true
	plot_temperature_bars(plot, meas)

	plot_time_per_kernel(plot, meas)
	
	plot.yrange = nothing #(100, 300)
	plot_energy_per_kernel(plot, meas)
	
	plot_power_bars(plot, meas)

	return meas
end
function _entry_common(
	path::String, primary_param::Union{Symbol, Nothing} = nothing;
	kwargs...
)
	meas = load_measurement(path, primary_param)
	_entry_common(meas; kwargs...)
end

function entry_mb2()
	a = _entry_common("control_mb2_cpu_profile"; x_ticks_angle = -25.0)
	plot_individual_progress(a)

	b = _entry_common("control_mb2_gpu_profile"; x_ticks_angle = -15.0)
	
	c = load_measurement("control_mb2_workgroup_size")
	c = filter_measurement_param(
		c,
		c.primary_param,
		v -> v.value != [1024, 1024],
		""
	)
	c1 = filter_measurement_param(
		c,
		c.primary_param,
		v -> v.value[1] == 1 || v.value[2] == 1,
		" select"
	)
	_entry_common(c1; x_ticks_angle = -50.0, x_ticks_size = 16)
	# c2 = load_measurement("control_mb2_workgroup_size_nocompute")
	# c3 = load_measurement("control_mb2_workgroup_size_pretty")

	plot = PlotContext(:control_mb2_workgroup_size_2d, (2, 2))
	plot.file_name = "control mb2 work group size 2d"
	plot.cbrange = (18.200, 18.202)
	plot_2d_matrix_plot(
		plot, c,
		Symbol("work_group_size[1]"), Symbol("work_group_size[2]"),
		:time
	)
	plot.cbrange = nothing
	plot_2d_matrix_plot(
		plot, c,
		Symbol("work_group_size[1]"), Symbol("work_group_size[2]"),
		:energy
	)
	plot_2d_matrix_plot(
		plot, c,
		Symbol("work_group_size[1]"), Symbol("work_group_size[2]"),
		:temperature
	)
	plot_2d_matrix_plot(
		plot, c,
		Symbol("work_group_size[1]"), Symbol("work_group_size[2]"),
		:power
	)
	
	d = load_measurement("test_mb2_gpu_affinity")
	d = combine_measurement_param(d, :graphics_api, :gpu_affinity)
	d = _entry_common(d; x_ticks_angle = -35.0)

	return [a, b, c]
end

function entry_cpmem()
	a = _entry_common("control_cpmem_cpu_profile"; x_ticks_angle = -25.0)
	plot_individual_progress(a)

	b = _entry_common("control_cpmem_gpu_profile"; x_ticks_angle = -15.0)
	c = _entry_common("control_cpmem_workgroup_size"; x_ticks_angle = -25.0)
	d = _entry_common("control_cpmem_offset_type")
	e = _entry_common("control_cpmem_vector_size")
	# e2 = _entry_common("control_cpmem_vector_size_lesswrite")
	f = _entry_common("test_cpmem_gpu_affinity")

	return [a, b, c, d, e, f]
end

function entry_intcomp()
	a = _entry_common("control_intcomp_cpu_profile"; x_ticks_angle = -25.0)
	b = _entry_common("control_intcomp_gpu_profile"; x_ticks_angle = -15.0)
	c = _entry_common("control_intcomp_workgroup_size"; x_ticks_angle = -25.0)
	d = _entry_common("control_intcomp_vector_size")
	e = _entry_common("test_intcomp_gpu_affinity")

	return [a, b, c, d, e]
end

function entry_profiles()
	a = _entry_common("do_mb2_profiles")
	b = _entry_common("do_cpmem_profiles")
	c = _entry_common("do_intcomp_profiles")

	return [a, b, c]
end

function entry_offload()
	a = load_measurement("test_mb2_cpu_offload", :work_share_factor)
	# plot_individual_progress(a1)
	_entry_common(a; x_ticks_angle = -35.0)
	
	b = load_measurement("test_mb2_cpu_offload_axis", :work_share_factor)
	b = combine_measurement_param(b, :work_share_factor, :work_share_axis)
	b = filter_measurement_param(
		b,
		b.primary_param,
		v -> v.value[1] == 10 || v.value[1] == 22 || v.value[1] == 24 || v.value[1] == 50,
		""
	)
	_entry_common(b; x_ticks_angle = -30.0)

	c = load_measurement("test_mb2_cpu_offload_cpu_profile", :work_share_factor)
	c = join_measurements(a, c; data_dir = c.data_dir, title = c.title, primary_param = c.primary_param)
	c1 = combine_measurement_param(c, Symbol("#cpu_profile"), :work_share_factor)
	_entry_common(c1; x_ticks_angle = -65.0, x_ticks_size = 16)
end

function entry_overhead()
	a = _entry_common("test_overhead_api")
	# b = _entry_common("test_overhead_vulkan_affinity")

	# plot_individual_progress(:test_overhead_api_individual, a)
	# plot_individual_progress(:test_overhead_vulkan_affinity_individual, b)

	# return [a, b]
end

function entry_throttling()
	# a = combine_measurement_param(
	# 	load_measurement("do_mb2_software_throttle_hardware", :gpu_affinity),
	# 	:graphics_api,
	# 	:gpu_affinity
	# )

	as = combine_measurement_param(
		load_measurement("do_mb2_software_throttle_hardware_short", :gpu_affinity),
		:graphics_api,
		:gpu_affinity
	)

	# b = combine_measurement_param(
	# 	load_measurement("do_mb2_software_throttle_software", :gpu_affinity),
	# 	:graphics_api,
	# 	:gpu_affinity
	# )

	bs = combine_measurement_param(
		load_measurement("do_mb2_software_throttle_software_short", :gpu_affinity),
		:graphics_api,
		:gpu_affinity
	)

	# _entry_common(a)
	_entry_common(as)
	# _entry_common(b)
	_entry_common(bs)

	# plot_individual_progress(:do_mb2_software_throttle_hardware_individual, a)
	plot_individual_progress(:do_mb2_software_throttle_hardware_short_individual, as)
	# plot_individual_progress(:do_mb2_software_throttle_software_individual, b)
	plot_individual_progress(:do_mb2_software_throttle_software_individual, bs)
end

function entry_pixeliter()
	# user nfs
	a = load_measurement("test_mb2_cpu_offload_pixel_iter", :work_share_factor)
	a = combine_measurement_param(a, :work_share_factor, :work_share_axis)
	a = filter_measurement_param(a, :work_share_axis, "x")

	# a1 = filter_measurement_param(a, :maximum_pixel_iterations, 128)
	a2 = filter_measurement_param(a, :maximum_pixel_iterations, 1024)
	# a3 = filter_measurement_param(a, :maximum_pixel_iterations, 3072)
	# a4 = filter_measurement_param(a, :maximum_pixel_iterations, 8192)
	# a5 = filter_measurement_param(a, :maximum_pixel_iterations, 65536)

	# _entry_common(a1)
	# _entry_common(a2)
	# _entry_common(a3)
	# _entry_common(a4)
	# _entry_common(a5)

	plot_isolated_runs(:tmp_a2, a2, false)
	plot_isolated_runs(:tmp_a2_work, a2, true)

	# root nfs
	b = load_measurement("test_mb2_pixeliter_nfs", :work_share_factor)
	# b = combine_measurement_param(b, :work_share_factor, :work_share_axis)
	b = filter_measurement_param(b, :work_share_axis, "x")

	# _entry_common(b)

	plot_isolated_runs(:tmp_b1, b, false)
	plot_isolated_runs(:tmp_b1_work, b, true)

	# tmp
	c = load_measurement("test_mb2_pixeliter_tmp", :work_share_factor)
	# c = combine_measurement_param(c, :work_share_factor, :work_share_axis)
	c = filter_measurement_param(c, :work_share_axis, "x")

	# _entry_common(c)

	plot_isolated_runs(:tmp_c1, c, false)
	plot_isolated_runs(:tmp_c1_work, c, true)
end

function entry_app()
	a = load_measurement("app_adasmark")
	a1 = filter_measurement_param(
		a,
		a.primary_param,
		v -> v.value == "base" || v.value == "cpu-debayer",
		" select"
	)
	# _entry_common(a)
	_entry_common(a1)
	# plot_isolated_runs(:app_adasmark_isolated, a, false)
	# plot_isolated_runs(:app_adasmark_isolated_work, a, true)
	# plot_individual_progress(:app_adasmark_select_individual, a1)
	# plot_isolated_runs(:app_adasmark_isolated, a, true)

	b = load_measurement("app_adasmark_cpu_profile")
	b = combine_measurement_param(b, :pipeline_prefix, Symbol("#cpu_profile"))
	b = filter_measurement_param(
		b,
		b.primary_param,
		v -> v.value[1] == "base",
		" base"
	)
	_entry_common(b; x_ticks_angle = -25.0)
	# plot_individual_progress(:app_adasmark_cpu_individual, b)
	# plot_isolated_runs(:app_adasmark_cpu_isolated, b, true)

	c = load_measurement("app_adasmark_gpu_profile")
	c = combine_measurement_param(c, :pipeline_prefix, Symbol("#gpu_profile"))
	c1 = filter_measurement_param(
		c,
		c.primary_param,
		v -> v.value == ["base", "overdrive"] || v.value == ["cpu-debayer", "nominal"],
		" select"
	)
	# _entry_common(c)
	_entry_common(c1; x_ticks_angle = -25.0)
	# plot_individual_progress(:app_adasmark_gpu_select_individual, c1)
	# plot_isolated_runs(:app_adasmark_gpu_isolated, c, true)

	return [a, b, c]
end

function entry_variance()
	a = load_measurement("test_mb2_cpu_offload_variance")
	a = combine_measurement_param(a, Symbol("#proc_scheduler"), :pocl_affinity, :pocl_max_pthread)
	
	a1 = filter_measurement_param(
		a,
		a.primary_param,
		v -> (v.value[1] != "fifo" && v.value[1] != "rr" && v.value[2] == true) || (v == "default x false x 6"),
		# v -> true,
		""
	)
	plot_isolated_runs(:variance_a, a1, true)

	_entry_common(a1)
end

all_mb = entry_mb2()
all_cpmem = entry_cpmem()
all_intcomp = entry_intcomp()
all_app = entry_app()
entry_offload()
entry_overhead()

# entry_variance()
# entry_pixeliter()
# entry_throttling()
# plot = PlotContext(:energy_time_scatter, (1, 1))
# plot.file_name = "energy time scatter"
# plot_energy_time_scatter(plot, [all_mb..., all_cpmem..., all_intcomp..., all_app...])


print("Done!")