Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	#measurement_name: data__vector_size=1,
	#measurement_directory: ./control_intcomp_vector_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff93b82050>
}
Command: /home/root/stage/thermobench --name data__vector_size=1 --output_dir ./control_intcomp_vector_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/int_compute --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 2,
	#measurement_name: data__vector_size=2,
	#measurement_directory: ./control_intcomp_vector_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff93b82050>
}
Command: /home/root/stage/thermobench --name data__vector_size=2 --output_dir ./control_intcomp_vector_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/int_compute --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 2
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 4,
	#measurement_name: data__vector_size=4,
	#measurement_directory: ./control_intcomp_vector_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff93b82050>
}
Command: /home/root/stage/thermobench --name data__vector_size=4 --output_dir ./control_intcomp_vector_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/int_compute --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 4
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 8,
	#measurement_name: data__vector_size=8,
	#measurement_directory: ./control_intcomp_vector_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff93b82050>
}
Command: /home/root/stage/thermobench --name data__vector_size=8 --output_dir ./control_intcomp_vector_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/int_compute --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 8
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 16,
	#measurement_name: data__vector_size=16,
	#measurement_directory: ./control_intcomp_vector_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff93b82050>
}
Command: /home/root/stage/thermobench --name data__vector_size=16 --output_dir ./control_intcomp_vector_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/int_compute --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 16
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Setting fan speed to 0.0
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
