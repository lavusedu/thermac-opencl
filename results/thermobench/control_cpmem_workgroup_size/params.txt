Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=32x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=32x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 32 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [64, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=64x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=64x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 64 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [128, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=128x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=128x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 128 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [256, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=256x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=256x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 256 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [512, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=512x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=512x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 512 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 20,
	run_min_time: 0.0,
	work_group_size: [1024, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	problem_size: 16777216,
	vector_size: 1,
	offset_type: local,
	#measurement_name: data__work_group_size=1024x1,
	#measurement_directory: ./control_cpmem_workgroup_size,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffffa58e1b40>
}
Command: /home/root/stage/thermobench --name data__work_group_size=1024x1 --output_dir ./control_cpmem_workgroup_size --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 -- ./sleepand.sh 5 /home/root/stage/experiments/clpeak_global_bandwidth --run-count 20 --run-min-time 0.0 --work-share-factor 0.0 --work-share-axis x --work-group-size-x 1024 --work-group-size-y 1 --width 16777216 --vector-size 1 --offset-type local
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Setting fan speed to 0.0
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
