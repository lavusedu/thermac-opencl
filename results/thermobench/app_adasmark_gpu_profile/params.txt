Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: base-single,
	#measurement_name: data__pipeline_prefix=base-single__#gpu_profile=overdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=base-single__#gpu_profile=overdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run base-single
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: nominal,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: base-single,
	#measurement_name: data__pipeline_prefix=base-single__#gpu_profile=nominal,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=base-single__#gpu_profile=nominal --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run base-single
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to nominal
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: underdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: base-single,
	#measurement_name: data__pipeline_prefix=base-single__#gpu_profile=underdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=base-single__#gpu_profile=underdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run base-single
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to underdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-split-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=overdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=overdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-split-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: nominal,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-split-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=nominal,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=nominal --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-split-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to nominal
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: underdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-split-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=underdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-split-debayer__#gpu_profile=underdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-split-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to underdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=overdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=overdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: nominal,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=nominal,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=nominal --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to nominal
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: underdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu-debayer,
	#measurement_name: data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=underdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu-debayer__#gpu_profile=underdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu-debayer
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to underdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: overdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu,
	#measurement_name: data__pipeline_prefix=pipeline-cpu__#gpu_profile=overdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu__#gpu_profile=overdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: nominal,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu,
	#measurement_name: data__pipeline_prefix=pipeline-cpu__#gpu_profile=nominal,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu__#gpu_profile=nominal --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to nominal
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Measurement execution:
Parameters: {
	thermobench_period: 1000,
	#fan_speen: 0.5,
	#cpu_profile: all,
	#gpu_profile: underdrive,
	#proc_scheduler: batch,
	#proc_niceness: -20,
	run_count: 10,
	run_min_time: 0.0,
	work_group_size: [32, 1],
	work_share_factor: 0.0,
	work_share_axis: x,
	pipeline_prefix: pipeline-cpu,
	#measurement_name: data__pipeline_prefix=pipeline-cpu__#gpu_profile=underdrive,
	#measurement_directory: ./app_adasmark_gpu_profile,
	#measurement_file_path: <function Measurement.__init__.<locals>.<lambda> at 0xffff92c50940>
}
Command: /home/root/stage/thermobench --name data__pipeline_prefix=pipeline-cpu__#gpu_profile=underdrive --output_dir ./app_adasmark_gpu_profile --sensors_file /home/root/stage/sensors.imx8 --column=work_begin --column=work_done --column=work_elapsed --cpu-usage --period 1000 --wait 0 --wait-timeout 5 -- /home/root/stage/experiments/adasmark --run-count 10 run pipeline-cpu
Env: {
	POCL_AFFINITY: 1
}

Setting fan speed to 0.5
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to underdrive
Setting process scheduler to 3 with priority 0
Setting process niceness to -20
Setting fan speed to 0.0
Setting cpu profile to [1, 1, 1, 1, 1, 1]
Setting gpu profile to overdrive
