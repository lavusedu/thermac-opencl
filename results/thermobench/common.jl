using Thermobench, DataFrames, Statistics

const POWER_IDLE = 4.96 # Watt

struct ParameterDefinition
	name::String
	default_value::Any

	parse_fn
	sort_key_fn

	ParameterDefinition(
		name::String,
		default_value::Any,
		parse_fn,
		sort_key_fn = identity
	) = new(
		name,
		default_value,
		parse_fn,
		sort_key_fn
	)
end

struct DataParameter{T}
	name::String
	value::T
	sort_key
end
const DATA_PARAM_JOIN_STR = " * "
function Base.show(io::IO, v::DataParameter{T}) where {T}
	if isa(v.value, Vector)
		join(io, v.value, DATA_PARAM_JOIN_STR)
	else
		print(io, string(v.value))
	end
end
function Base.string(v::DataParameter{T}) where {T}	
	if isa(v.value, Vector)
		return join(v.value, DATA_PARAM_JOIN_STR)
	else
		return string(v.value)
	end
end
function Base.isless(a::DataParameter{T}, b::DataParameter{T}) where {T}
	return isless(a.sort_key, b.sort_key)
end
function Base.:(==)(a::DataParameter{T}, b::T) where {T}
	return a.value == b
end
function Base.:(==)(a::DataParameter{T}, b::String) where {T}
	return string(a) == b
end

function _parse_int_param(v)
	return parse(Int, v)
end
function _parse_bool_param(v)
	return parse(Bool, v)
end
function _parse_percent_param(v)
	return convert(Int, round(parse(Float64, v) * 100, digits = 2))
end
function _parse_product_param(v)
	res::Vector{Int} = []
	for x in split(v, "x") # csv file names use x for product
		push!(res, _parse_int_param(x))
	end

	return res
end
function _parse_renamed_param(dict; allow_missing::Bool = false)
	if allow_missing
		return v -> get(dict, v, v)
	else
		return v -> dict[v]
	end
end

const KNOWN_PARAM_SORT_KEYS = Dict(
	# cpu profile
	"all" => 1,
	"1xA53" => 2,
	"4xA53" => 3,
	"1xA72" => 4,
	"2xA72" => 5,

	# gpu profile
	"overdrive" => 1,
	"nominal" => 2,
	"underdrive" => 3,

	# offset type
	"local" => 1,
	"global" => 2
)
function _param_sort_key(v)
	return get(KNOWN_PARAM_SORT_KEYS, v, v)
end

function _replace_name_characters(name::String)::String
	return replace(name, "_" => " ", "/" => "-")
end

const CPU_PROFILE_RENAME = Dict(
	"all" => "all",
	"little-one" => "1xA53",
	"little-all" => "4xA53",
	"big-one" => "1xA72",
	"big-all" => "2xA72",
)
const ADASMARK_RENAME = Dict(
	"base-single" => "base",
	"pipeline-cpu" => "cpu",
	"pipeline-cpu-debayer" => "cpu-debayer",
	"pipeline-cpu-dewarp" => "cpu-dewarp",
	"pipeline-split-bgrtogray" => "split-bgrtogray",
	"pipeline-split-debayer" => "split-debayer"
)

const PARAMETER_DEFINITIONS = Dict(
	:unknown => ParameterDefinition("Unknown", nothing, identity),

	:offload => ParameterDefinition("CPU offload [%]", 0, _parse_percent_param),
	:wgsx => ParameterDefinition("Workgroup X", 32,  _parse_int_param),
	:wgsy => ParameterDefinition("Workgroup Y", 1, _parse_int_param),
	:workgroups => ParameterDefinition("Workgroup size", 32, _parse_int_param),

	Symbol("#fan_speed") => ParameterDefinition("Fan speed [%]", 50, _parse_percent_param),
	Symbol("#cpu_profile") => ParameterDefinition("CPU profile", CPU_PROFILE_RENAME["all"], _parse_renamed_param(CPU_PROFILE_RENAME), _param_sort_key),
	Symbol("#gpu_profile") => ParameterDefinition("GPU profile", "overdrive", identity, _param_sort_key),
	Symbol("#proc_scheduler") => ParameterDefinition("Process scheduler", "default", identity),
	Symbol("#proc_niceness") => ParameterDefinition("Process niceness", 0, _parse_int_param),

	:pocl_affinity => ParameterDefinition("PoCL affinity pinning", false, _parse_bool_param),
	:pocl_max_pthread => ParameterDefinition("PoCL max threads", 6, _parse_int_param),
	
	:work_group_size => ParameterDefinition("Work group size", [32, 1], _parse_product_param),
	Symbol("work_group_size[1]") => ParameterDefinition("Work group size X", 32, identity),
	Symbol("work_group_size[2]") => ParameterDefinition("Work group size Y", 1, identity),

	:work_share_factor => ParameterDefinition("CPU offload [%]", 0, _parse_percent_param),
	:work_share_axis => ParameterDefinition("CPU offload axis", "x", identity),
	:maximum_pixel_iterations => ParameterDefinition("Pixel iterations", 3072, _parse_int_param),
	:vector_size => ParameterDefinition("Vector size", 1, _parse_int_param),
	:offset_type => ParameterDefinition("Memory offset type", "local", identity, _param_sort_key),

	:thermobench_period => ParameterDefinition("Sampling frequency", 1000, _parse_int_param),
	:gpu_affinity => ParameterDefinition("GPU core affinity", "0", identity),
	
	:graphics_api => ParameterDefinition("Graphics API", "opencl", identity),
	:pipeline_prefix => ParameterDefinition("Pipeline", ADASMARK_RENAME["base-single"], _parse_renamed_param(ADASMARK_RENAME, allow_missing = true)),

	:profile => ParameterDefinition("Config profile", "default", identity)
)

function _csv_create_parameter(key::Union{Symbol, AbstractString}, value, out)
	if !isa(key, Symbol)
		key = Symbol(key)
	end
	
	# get(KNOWN_PARAMS, key, KNOWN_PARAMS[:unknown])
	definition = PARAMETER_DEFINITIONS[key]

	parsed = definition.parse_fn(value)
	sort_key = definition.sort_key_fn(parsed)
	out[key] = DataParameter(definition.name, parsed, sort_key)

	if isa(parsed, Vector)
		for (index, subvalue) in enumerate(parsed)
			_csv_create_parameter(
				Symbol(string(key, '[', index, ']')),
				subvalue,
				out
			)
		end
	end

	return key
end
function _parse_csv_filename(file)
	if !endswith(file, ".csv")
		return nothing
	end

	name = SubString(file, 1, lastindex(file) - 4)
	first_param = nothing
	params = Dict()

	if startswith(name, "data__")
		name = SubString(name, 7)
		for param_pair in split(name, "__")
			key, value = split(param_pair, "=")

			param_key = _csv_create_parameter(key, value, params)
			if first_param === nothing
				first_param = param_key
			end
		end
		name = replace(name, "__" => ", ")
	elseif startswith(name, "mb2_offload_")
		# support specific older data
		value = SubString(name, 13)
		param_key = _csv_create_parameter(:work_share_factor, value, params)
	else
		param_regex = r"([a-zA-Z]+)_?([0-9\.]+)"
		for (key, value) in eachmatch(param_regex, name)
			param_key = _csv_create_parameter(key, value, params)
			if first_param === nothing
				first_param = param_key
			end
		end
	end

	return (file, name, params, first_param)
end

struct MeasurementContext
	data_dir::String
	title::String
	primary_param::Symbol
	data::Vector{Thermobench.Data}
end

"""
Linearly interpolate and then extrapolate column to fill all rows.
"""
function polate_column!(data::DataFrame, column::Symbol)
	view = data[!, [:time, column]]
	
	interpolate!(view)

	filled = filter(v -> !ismissing(v[column]), data[!, [:time, column]])
	first_time, first_col = first(filled)
	last_time, last_col = last(filled)

	for row in 1 : size(view)[1]
		if !ismissing(view[row, column])
			continue
		end

		view[row, column] = linear_extrapolation(first_time, first_col, last_time, last_col, view[row, :time])
	end
end

const COLUMN_POWER = Symbol("power_µW")
function _transform_power_to_energy!(data::DataFrame)
	polate_column!(data, COLUMN_POWER)

	time_diff = diff(data.time)
	power = map(Statistics.middle, data[2:end, COLUMN_POWER], data[1:end-1, COLUMN_POWER]) ./ 1e6
	energy_diff = time_diff .* power

	energy = [0.0]
	acc = 0
	for curr in energy_diff
		acc += curr
		push!(energy, acc)
	end
	data[!, :energy] = energy
end

function load_measurement(
	data_dir::String,
	primary_param::Union{Symbol, Nothing} = nothing
)::MeasurementContext
	parsed = filter(!isnothing, map(_parse_csv_filename, readdir(data_dir)))

	data = []
	for (file, name, params, first_param) in parsed
		dat = Thermobench.read(
			joinpath(data_dir, file),
			name = _replace_name_characters(name)
		)
		dat.meta[:params] = params

		# normalize energy
		if string(COLUMN_POWER) in names(dat.df)
			# support older measurements which had power measurement instead of energy
			_transform_power_to_energy!(dat.df)
		else
			polate_column!(dat.df, :energy)
		end

		push!(data, dat)
		if primary_param === nothing
			primary_param = first_param
		end
	end

	# sort based on primary param
	sort!(
		data,
		by = dat -> dat.meta[:params][primary_param].sort_key
	)

	return MeasurementContext(
		data_dir,
		_replace_name_characters(data_dir), # TODO: known titles?
		primary_param,
		data
	)
end

function filter_measurement_param(
	measurement::MeasurementContext,
	param_name,
	filter_func,
	title_suffix::String
)::MeasurementContext
	return MeasurementContext(
		measurement.data_dir,
		measurement.title * title_suffix,
		measurement.primary_param,
		filter(data -> filter_func(data.meta[:params][param_name]), measurement.data)
	)
end
function filter_measurement_param(
	measurement::MeasurementContext,
	param_name,
	param_value
)::MeasurementContext
	return filter_measurement_param(measurement, param_name, v -> v == param_value, " $(param_value)")
end

function combine_measurement_param(
	measurement::MeasurementContext, param_keys...
)::MeasurementContext
	key_str = join(["$(n)" for n in param_keys], DATA_PARAM_JOIN_STR)
	key = Symbol(key_str)

	new_data = []
	for data in measurement.data
		data = Thermobench.Data(
			data.df,
			data.name,
			deepcopy(data.meta)
		)

		params = [data.meta[:params][key] for key in param_keys]
		data.meta[:params][key] = DataParameter(
			join(getproperty.(params, :name), DATA_PARAM_JOIN_STR),
			getproperty.(params, :value),
			getproperty.(params, :sort_key)
		)

		push!(new_data, data)
	end

	sort!(
		new_data,
		by = tdata -> tdata.meta[:params][key].sort_key
	)

	return MeasurementContext(
		measurement.data_dir,
		measurement.title,
		key,
		new_data
	)
end

function join_measurements(
	m1::MeasurementContext, m2::MeasurementContext;
	data_dir::String, title::String, primary_param::Symbol
)::MeasurementContext
	new_data = [m1.data; m2.data]
	sort!(
		new_data,
		by = tdata -> tdata.meta[:params][primary_param].sort_key
	)

	return MeasurementContext(
		data_dir,
		title,
		primary_param,
		new_data
	)
end

"""
Return that data point of the measurement which has all parameters of default value
"""
function get_base_data(
	measurement::MeasurementContext
)::Union{Thermobench.Data, Nothing}
	for data in measurement.data
		is_different = false
		
		param_keys = keys(data.meta[:params])
		for param_key in param_keys
			if param_key in keys(PARAMETER_DEFINITIONS) # TODO
				if data.meta[:params][param_key].value != PARAMETER_DEFINITIONS[param_key].default_value
					is_different = true
					break
				end
			end
		end

		if !is_different
			return data
		end
	end

	return nothing
end

"""
Isolates rows into sub-dataframes based on work_begin and work_end columns.

The time and energy columns are offset to begin at zero in the subframes.

`use_work_done` controls whether work ranges are ended when `work_done` row is found or 
only after another `work_begin` row is found.
"""
function _isolate_work_ranges(data::DataFrame, use_work_done::Bool)::Vector{DataFrame}		
	begin_column = :work_begin
	if !("work_begin" in names(data))
		begin_column = :work_done
	end

	end_column = :work_done
	if !use_work_done
		end_column = begin_column
	end
	
	ranges = []
	
	current_range_index = 1
	current_work_index = nothing
	for i in 1:size(data)[1]
		if data[i, end_column] !== missing
			range = data[current_range_index : i, :]
			offset_column!(range, :time)
			offset_column!(range, :energy)
			push!(ranges, range)

			current_work_index = nothing
		end

		if data[i, begin_column] !== missing
			current_range_index = i
			current_work_index = data[i, begin_column]
		end
	end

	# don't forget the last range
	if current_work_index !== nothing
		range = data[current_range_index : end, :]
		offset_column!(range, :time)
		offset_column!(range, :energy)
		push!(ranges, range)

		current_work_index = nothing
	end

	if length(ranges) == 0
		range = data[1 : end, :]
		offset_column!(range, :time)
		offset_column!(range, :energy)
		return [range]
	else
		return ranges
	end
end
# isolate_work_ranges(data::Thermobench.Data, use_work_done::Bool)::Vector{DataFrame} = isolate_work_ranges(data.df, use_work_done)

function _drop_outliers(samples)
	# result = []
	# for sample in samples[2:]
	# 	if 
	# 	length(sample) > 1
	# 	&& last(sample).time > 0.0
	# 		push!(result, sample)
	# 	end
	# end

	# return result
	
	if length(samples) < 3
		return samples
	else
		return samples[2:end-1]
	end
end

function _total_sample_mean(data::DataFrame, use_work_done::Bool, column::Symbol)::Measurement
	isolated = _drop_outliers(_isolate_work_ranges(data, use_work_done))
	
	return sample_mean_est(
		last.(getproperty.(isolated, column))
	) * length(isolated)
end

"""
Return total consumed energy of this execution in Joules.
"""
function total_energy(data::DataFrame)::Measurement
	return _total_sample_mean(data, false, :energy)
	
	# rows = data[!, :energy]
	# delta = last(rows) - first(rows)
	# if delta < 0
	# 	@warn "calculation is a negative value - remeasure?"
	# end

	# return delta
end
total_energy(data::Thermobench.Data)::Measurement = total_energy(data.df)

"""
Return total consumed energy of this executions work (as reported by the experiment) in Joules.
"""
function total_energy_work(data::DataFrame)::Measurement
	return _total_sample_mean(data, true, :energy)
end
total_energy_work(data::Thermobench.Data)::Measurement = total_energy_work(data.df)

"""
Return total consumed energy of this executions idle time (as reported by the experiment) in Joules.
"""
function total_energy_idle(data::DataFrame)::Measurement
	return total_energy(data) - total_energy_work(data)
end
total_energy_idle(data::Thermobench.Data)::Measurement = total_energy_idle(data.df)

"""
Return total time of this execution in seconds.
"""
function total_time(data::DataFrame)::Measurement
	return _total_sample_mean(data, false, :time)
end
total_time(data::Thermobench.Data)::Measurement = total_time(data.df)

"""
Returns number of operations (kernel executions) performed.
"""
function total_ops(data::DataFrame)::Int64
	return length(_drop_outliers(_isolate_work_ranges(data, true)))
	# todo: might not be the same thing
	# return last(filter(!ismissing, data[!, :work_done]))
end
total_ops(data::Thermobench.Data)::Int64 = total_ops(data.df)

"""
Returns amount of time spent actually executing kernels (as reported by the experiment).
"""
function total_time_work(data::DataFrame)::Measurement
	# rows = _drop_outliers(filter(!ismissing, data[!, :work_elapsed]))
	# work_elapsed = sample_mean_est(rows) * length(rows)
	# return work_elapsed

	return _total_sample_mean(data, true, :time)
end
total_time_work(data::Thermobench.Data)::Measurement = total_time_work(data.df)

"""
Returns amount of time spent idle (as reported by the experiment).
"""
function total_time_idle(data::DataFrame)::Measurement
	return total_time(data) - total_time_work(data)
end
total_time_idle(data::Thermobench.Data)::Measurement = total_time_idle(data.df)

"""
Returns the power [W; J/s] at each point in time of the data.

The returned data has the same length as the time column.
"""
function power_watts(data::DataFrame)::Vector{Float64}
	res = diff(data.energy) ./ diff(data.time)
	for i in 1:size(res)[1]
		if isnan(res[i])
			if i == 1 
				res[i] = 0.0
			else
				res[i] = res[i - 1]
			end
		end
	end

	return [res[1]; res]
end
power_watts(data::Thermobench.Data)::Vector{Float64} = power_watts(data.df)

function total_power_watts(data::DataFrame)::Measurement
	return total_energy(data) / total_time(data)
end
total_power_watts(data::Thermobench.Data)::Measurement = total_power_watts(data.df)

function total_power_watts2(data::DataFrame, use_work_done::Bool)::Measurement
	isolated = _drop_outliers(_isolate_work_ranges(data, use_work_done))

	parts = []
	for range in isolated
		last_row = last(range)
		power = last_row.energy / last_row.time
		if !isnan(power)
			push!(parts, power)
		end
	end
	
	return sample_mean_est(parts)
end
total_power_watts2(data::Thermobench.Data)::Measurement = total_power_watts2(data.df, false)

function temperature_data(data::DataFrame, column::Symbol = :GPU_0_temp)::DataFrame
	if column === :cpu_auto
		column = :CPU_0_temp
		if size(data[!, r"CPU[0-3]_load_"])[2] == 0
			column = :CPU_1_temp
		end
	end
	
	temperature = select(
		data, :time, :ambient, column => :temperature
	)
	polate_column!(temperature, :temperature)
	polate_column!(temperature, :ambient)

	return temperature
end
temperature_data(data::Thermobench.Data, column::Symbol = :GPU_0_temp)::DataFrame = temperature_data(data.df, column)

function temperature_inf(data::DataFrame)::Measurement	
	mf = Thermobench.multi_fit(
		Thermobench.Data(data),
		:temperature,
		order = 1,
		use_cmpfit = true,
		use_measurements = true,
		tau_bounds = [(0.2*60, 2*60)],
		subtract = :ambient
	)

	# @gp :temp mf

	return mf.result.Tinf[1]
end
temperature_inf(data::Thermobench.Data)::Measurement = temperature_inf(data.df)

function temperature_avg(data::DataFrame, time_window = 200.0)::Float64
	end_time = max(data[!, :time]...)
	start_time = min(data[!, :time]...)
	start_time = max(end_time - time_window, start_time)
	data = filter(x -> x[:time] >= start_time && x[:time] <= end_time, data)

	average = sum(data[!, :temperature] - data[!, :ambient]) / size(data)[1]
	return average
end
temperature_avg(data::Thermobench.Data, time_window = 200.0)::Float64 = temperature_avg(data.df, time_window)

"""
Offsets column by given `offset` or by `-min(df[!, column])` in-place.
"""
function offset_column!(
	df::AbstractDataFrame,
	column::Symbol,
	offset = nothing
)
	if offset === nothing
		offset = -min(df[!, column]...)
	end

	for row in eachrow(df)
		row[column] += offset
	end
end

function linear_extrapolation(x0, y0, x1, y1, x2)
	xd = x1 - x0
	yd = y1 - y0

	y2 = y0 + (x2 - x0) * yd / xd

	return y2
end
