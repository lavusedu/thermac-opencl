using Cairo

const COLOR_TABLE = [
	(0.33, 0.33, 0.33), # grey
	(0.90, 0.00, 0.23), # red
	(0.90, 0.67, 0.00), # orange
	(0.23, 0.90, 0.00), # green
	(0.00, 0.90, 0.67), # cyan
	(0.00, 0.23, 0.90), # blue
	(0.67, 0.00, 0.90), # purple
]

function _aling_text(ctx, text, xpos, ypos, x_align = :center, y_align = :center)
	# typedef struct {
	#     double x_bearing;
	#     double y_bearing;
	#     double width;
	#     double height;
	#     double x_advance;
	#     double y_advance;
	# } cairo_text_extents_t;

	extents = text_extents(ctx, text)

	x_baseline = extents[1]
	y_baseline = extents[2]
	width = extents[3]
	height = extents[4]

	xpos_final = xpos
	if x_align == :left

	elseif x_align == :right
		xpos_final = xpos - width - x_baseline	
	elseif x_align == :center
		xpos_final = xpos - width / 2 - x_baseline
	end

	ypos_final = ypos
	if y_align == :top
		ypos_final = ypos - y_baseline
	elseif y_align == :bottom
		ypos_final = ypos + y_baseline
	elseif y_align == :center
		ypos_final = ypos - height / 2 - y_baseline
	end

	move_to(
		ctx,
		xpos_final,
		ypos_final
	)
	show_text(ctx, text)
end

function draw_clpeak_mem_access()
	surface_width = 1070.0
	surface_height = 60.0 * 4 + 15
	
	function draw_memory_vis(
		ctx, ypos_top,
		name,
		work_width::Int, vector_size::Int,
		colorization_fn
	)
		margin = 15.0
		height = 30.0
		width = surface_width - margin * 2.0

		save(ctx)

		element_count = work_width ÷ vector_size

		xpos_start = margin
		ypos_start = ypos_top + margin
		ypos_end = ypos_top + margin + height
		xpos_step = width / work_width * vector_size

		# heading
		set_source_rgb(ctx, 0, 0, 0)
		_aling_text(
			ctx, name,
			xpos_start, ypos_start,
			:left, :bottom
		)

		# colorizations
		for x_index in 0 : element_count - 1
			color_index = colorization_fn(x_index)
			if color_index === nothing
				continue
			end

			set_source_rgba(
				ctx,
				COLOR_TABLE[color_index][1],
				COLOR_TABLE[color_index][2],
				COLOR_TABLE[color_index][3],
				0.2
			)
			rectangle(
				ctx,
				xpos_start + xpos_step * x_index, ypos_start,
				xpos_step, height
			)
			fill(ctx)
		end

		# elements
		set_source_rgb(ctx, 0.2, 0.2, 0.2)
		set_line_width(ctx, 1.0)
		
		for x_index in 0 : element_count - 1
			xpos = xpos_start + xpos_step * x_index
			move_to(ctx, xpos, ypos_start)
			line_to(ctx, xpos, ypos_end)
			stroke(ctx)

			# _aling_text(
			# 	ctx, "v$(vector_size)",
			# 	xpos + xpos_step * vector_size / 2,
			# 	(ypos_start + ypos_end) / 2
			# )
		end

		# frame
		set_source_rgb(ctx, 0.0, 0.0, 0.0)
		set_line_width(ctx, 2.0)
		move_to(ctx, xpos_start + width - xpos_step, ypos_start)
		line_to(ctx, xpos_start, ypos_start)
		line_to(ctx, xpos_start, ypos_start + height)
		line_to(ctx, xpos_start + width - xpos_step, ypos_start + height)
		stroke(ctx)

		# frame trailing dots
		set_line_type(ctx, "dash")
		move_to(ctx, xpos_start + width - xpos_step, ypos_start)
		line_to(ctx, xpos_start + width, ypos_start)
		stroke(ctx)
		move_to(ctx, xpos_start + width - xpos_step, ypos_start + height)
		line_to(ctx, xpos_start + width, ypos_start + height)
		stroke(ctx)

		restore(ctx)

		return ypos_top + margin * 2 + height
	end

	surface = CairoSVGSurface("./clpeak_mem_access.svg", surface_width, surface_height)
	ctx = CairoContext(surface)

	# Select font globally
	select_font_face(ctx, "monospace", Cairo.FONT_SLANT_NORMAL, Cairo.FONT_WEIGHT_NORMAL)
	set_font_size(ctx, 10.0)

	# set_source_rgb(ctx, 0.96, 0.96, 0.86)
	# set_source_rgb(ctx, 1.0, 0.0, 0.0)
	# rectangle(ctx, 0.0, 0.0, surface_width, surface_height)
	# stroke(ctx)

	work_width = 256
	local_size = 4
	fetch_per_wi = 16
	function colorization_local(index)
		group_id = index ÷ (local_size * fetch_per_wi)
		local_id = index % local_size
		global_id = group_id * local_size + local_id
		
		# colorize only the first group
		if group_id != 0
			return nothing
		end

		# show accesses of the first work item specifically
		if local_id != 0
			return 3
		end

		return 2
	end

	function colorization_global(index)
		global_id = index % (work_width ÷ fetch_per_wi)
		group_id = global_id ÷ local_size
		local_id = global_id % local_size
		
		# colorize only the first group
		if group_id != 0
			return nothing
		end

		# show accesses of the first work item specifically
		if local_id != 0
			return 3
		end

		return 2
	end

	l = 15.0
	l = draw_memory_vis(ctx, l, "v1 - local", work_width, 1, colorization_local)
	l = draw_memory_vis(ctx, l, "v4 - local", work_width, 4, colorization_local)
	l = draw_memory_vis(ctx, l, "v1 - global", work_width, 1, colorization_global)
	l = draw_memory_vis(ctx, l, "v4 - global", work_width, 4, colorization_global)

	finish(surface)
end

draw_clpeak_mem_access()