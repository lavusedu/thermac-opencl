SUMMARY = "adasmark-auzone"
DESCRIPTION = ""
HOMEPAGE = "https://www.eembc.org/adasmark"

LICENSE = "CLOSED"

SRC_URI = "\
	file://adasmark-1.0.1.tar.gz \
"

S = "${WORKDIR}/adasmark-1.0.1"

# TODO: requires libOpenCL.so()(64bit)
INSANE_SKIP:${PN} += "file-rdeps"

SOLIBS = ".so*"
FILES_SOLIBSDEV = ""

FILES:${PN}-dev = "${includedir}/*"

do_install() {
	install -d ${D}${libdir}
	install -d ${D}${includedir}

	install ${S}/auzone_tsr/dist/${HOST_ARCH}/lib/* -t ${D}${libdir}
	install ${S}/auzone_tsr/dist/${HOST_ARCH}/include/* -t ${D}${includedir}
}
