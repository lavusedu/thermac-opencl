SUMMARY = "pocl - Portable Computing Language"
DESCRIPTION = ""
HOMEPAGE = "https://portablecl.org"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f1e5cf2520dacf7c8b2461f6084bcc57"

SRC_URI = "\
	git://github.com/pocl/pocl.git;protocol=https \
	file://0001-intptr-typedef.patch \
"
SRCREV = "de9b966bcfc2e335cfcca89e9e82271d517426bc"
S = "${WORKDIR}/git"

# for pocl itself
DEPENDS += "clang-native clang llvm hwloc"
# for opencl
DEPENDS += "opencl-headers opencl-icd-loader" 

inherit pkgconfig cmake

# pocl build configuration
EXTRA_OECMAKE += "-DCMAKE_BUILD_TYPE=Release -DENABLE_TESTSUITES=ALL -DENABLE_TESTS=ON -DENABLE_ICD=ON -DPOCL_ICD_ABSOLUTE_PATH=OFF"
# target system configuration
# TODO: How to get cpu name from available variables?
EXTRA_OECMAKE += "-DLLC_TRIPLE='${HOST_SYS}' -DLLVM_HOST_TARGET='${HOST_SYS}' -DLLC_HOST_CPU='cortex-a53'"

do_install:append() {
	# move the icd config to /etc
	install -d ${D}${sysconfdir}
	mv ${D}${exec_prefix}${sysconfdir}/OpenCL ${D}${sysconfdir}/OpenCL
	rm -r ${D}${exec_prefix}${sysconfdir}
}