# this package is broken
# it claims to provide an icd loader but it is in fact not a loader at all
# it also claims to provide a libVivanteOpenCL.so file, but that is gone from the recent versions

# here we remove the claim to provide an icd loader and pull another one from the base layer
# also depend on patchelf-native so we can patch the libraries as we are renaming them
PROVIDES:remove = "opencl-headers opencl-clhpp opencl-icd-loader virtual/opencl-icd"
DEPENDS += "opencl-icd-loader patchelf-native"

# tweak the files based on what we need
# TODO: need libOpenCL.so.3 in the newer packages, but opencl-icd-loader one doesn't seem to cut it
FILES:remove = "${libdir}/libOpenCL.so ${libdir}/libOpenCL.so.*"
FILES:libopencl-imx += "${libdir}/libVivanteOpenCL.so.3 ${libdir}/libVivanteOpenCL.so.3.0.0 ${libdir}/libVivanteOpenCL.so.1 ${libdir}/libVivanteOpenCL.so.1.2.0 ${libdir}/libOpenCL.so.3"

# and move the files to the correct locations
# plus patch their SONAME so that ldconfig doesn't create the wrong symlinks
do_install:append() {
	mv ${D}${libdir}/libOpenCL.so.1.2.0 ${D}${libdir}/libVivanteOpenCL.so.1.2.0
	patchelf --set-soname libVivanteOpenCL.so.1 ${D}${libdir}/libVivanteOpenCL.so.1.2.0

	cp ${D}${libdir}/libOpenCL.so.3.0.0 ${D}${libdir}/libVivanteOpenCL.so.3.0.0
	mv ${D}${libdir}/libOpenCL.so.3.0.0 ${D}${libdir}/libOpenCL.tmp.so
	patchelf --set-soname libVivanteOpenCL.so.3 ${D}${libdir}/libVivanteOpenCL.so.3.0.0

	# we want the icd to find the 3.0.0 version because that one supports being loaded as an icd
	# (even though the 1.2 version claims to be icd loadable and this one doesn't)
	# ln -sf libVivanteOpenCL.so.3 ${D}${libdir}/libVivanteOpenCL.so
	echo "libVivanteOpenCL.so.3" > ${D}${sysconfdir}/OpenCL/vendors/Vivante.icd
	
	rm ${D}${libdir}/libOpenCL.so
	rm ${D}${libdir}/libOpenCL.so.*

	mv ${D}${libdir}/libOpenCL.tmp.so ${D}${libdir}/libOpenCL.so.3

	# remove opencl headers - they are provided by opencl-headers package
	rm -r ${D}${includedir}/CL
}
