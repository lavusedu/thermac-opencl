SUMMARY = "adasmark"
DESCRIPTION = ""
HOMEPAGE = "https://www.eembc.org/adasmark"

# TODO: no license in the tarball?
# LICENSE = "Proprietary"
LICENSE = "CLOSED"

SRC_URI = "\
	file://adasmark-1.0.1.tar.gz \
	file://adasmark-vendoropt-1.0.1.tar.gz \
	file://0001-plugin-naming.patch \
"
# file://adasmark-1.0.1.tar.gz contains adasmark-1.0.1 folder with src, kernels, etc.
# file://adasmark-vendoropt-1.0.1.tar.gz contains adasmark-1.0.1 folder with vendoropt

S = "${WORKDIR}/adasmark-1.0.1"

PACKAGES =+ "${PN}-data"
FILES:${PN}-data = "${datadir}/*"

DEPENDS += "adasmark-auzone opencl-headers opencl-icd-loader gstreamer1.0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good libxml2 libpng"
RDEPENDS:${PN} = "adasmark-auzone adasmark-data"

inherit pkgconfig autotools

# TODO: could be autodetected from machine description variables
# TODO: runtime fails with nxp/iMX8 override
THERMAC_ADASMARK_VENDOR ??= "none"

do_vendoropt() {
	# if there is such a vendor in the vendoropt directory then apply it
	if [ -d ${S}/vendoropt/${THERMAC_ADASMARK_VENDOR} ]; then
		cp -r ${S}/vendoropt/${THERMAC_ADASMARK_VENDOR}/* ${S}
	fi
}
addtask vendoropt after do_unpack before do_patch

do_install:append() {
	mv ${D}${bindir}/launcher ${D}${bindir}/adasmark-launcher
	mv ${D}${bindir}/mat_xml2bin ${D}${bindir}/adasmark-mat_xml2bin
	mv ${D}${bindir}/pbuilder ${D}${bindir}/adasmark-pbuilder

	install -d ${D}${datadir}/adasmark

	cp -r ${S}/kernels ${D}${datadir}/adasmark/
	install -d ${D}${datadir}/adasmark/launcher/cfg
	cp ${S}/launcher/cfg/*.cfg ${D}${datadir}/adasmark/launcher/cfg
	install -d ${D}${datadir}/adasmark/launcher/data
	cp ${S}/launcher/data/*.gz ${D}${datadir}/adasmark/launcher/data
	cp  ${S}/launcher/run_pipe.sh ${D}${datadir}/adasmark/launcher/

	cp -r ${S}/media ${D}${datadir}/adasmark/
}
