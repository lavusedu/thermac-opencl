# imx8 system

![](bitbake.jpg)

## Yocto image

### Building

- The the `repo` tool (`curl https://storage.googleapis.com/git-repo-downloads/repo > repo && chmod +x repo` to a directory in `PATH`)
- Make project dir `mkdir imx-yocto-bsp && cd imx-yocto-bsp`
- Get the yocto image repo (this takes some time)
	- Hardknott: `repo init -u https://source.codeaurora.org/external/imx/imx-manifest -b imx-linux-hardknott -m imx-5.10.72-2.2.0.xml` 
	- Kirkstone: `repo init -u https://source.codeaurora.org/external/imx/imx-manifest -b imx-linux-kirkstone -m imx-5.15.32-2.0.0.xml`
	- `repo sync`
- Configure the build `DISTRO=fsl-imx-wayland MACHINE=imx8qmmek source imx-setup-release.sh -b build`
- Edit `build/conf/local.conf`:
	- (If building from within CIIRC which can't reach example.com) Append line `CONNECTIVITY_CHECK_URIS ?= "https://rtime.felk.cvut.cz/"`
	- Append line: `INHERIT += "rm_work"` to not keep work files around (up to 130GB)
	- TODO: Append line: `THERMAC_BSP = "nxp"` or `THERMAC_BSP = "mainline"`
		- Selecting `mainline` primarily changes from nxp library blobs to mesa OpenGL etc. libraries
		- TODO: Doesn't compile
	- TODO: `MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "0"`
- Edit `build/conf/bblayers.conf`:
	- Append line: `BBLAYERS += "${BSPDIR}/sources/meta-thermac"`
	- Symlink or copy the `system/meta-thermac` directory of this repository to `sources/meta-thermac`
- Start the build `bitbake imx-image-multimedia` (**this takes a lot of time**)
- The `adasmark` recipe requires the `adasmark-1.0.1.tar.gz` tarball
	- Bitbake will list all paths it searches when the file is missing and the recipe is invoked.
	- One of these places is `imx-yocto-bsp/downloads`

### Using

- The important files are in `build/tmp/deploy/images/imx8qmmek/` (with a different git tag)
	- Device tree: `imx8qm-mek--5.10.35+git0+ef3f2cfc60-r0-imx8qmmek-20210823112039.dtb`
	- Boot image: `Image--5.10.35+git0+ef3f2cfc60-r0-imx8qmmek-20210823112039.bin`
	- Kernel modules: `modules--5.10.35+git0+ef3f2cfc60-r0-imx8qmmek-20210823112039.tgz`
	- Rootfs: `imx-image-multimedia-imx8qmmek-20210823112039.rootfs.tar.bz2`
- Further packages can be built inside the shell sourced environment (when building):
	- Restore environment: `source setup-enviroment build`
	- Build package: `bitbake clinfo`, `bitbake pocl` etc.
	- Rebuild package index: `bitbake package-index`
- Serve debian packages: See `misc/serve_deb.sh`
	- Don't forget to `bitbake package-index` after a new package is built (and for the first time)
- On the board:
	- Create `/etc/apt/sources.list.d/ritchie.list`:
		- Hardknott:
			```
			deb [trusted=yes] http://10.35.95.3:33038/all /
			deb [trusted=yes] http://10.35.95.3:33038/imx8qmmek /
			deb [trusted=yes] http://10.35.95.3:33038/cortexa72-cortexa53-crypto /
			deb [trusted=yes] http://10.35.95.3:33038/cortexa72-cortexa53-crypto-mx8 /
			```
		- Kirkstone:
			```
			deb [trusted=yes] http://10.35.95.3:33038/all /
			deb [trusted=yes] http://10.35.95.3:33038/imx8qmmek /
			deb [trusted=yes] http://10.35.95.3:33038/armv8a /
			deb [trusted=yes] http://10.35.95.3:33038/armv8a-mx8 /
			```
	- `apt-get update`
	- Install packages: `apt-get install <package name>` (e.g. `clinfo`, `pocl`)

### Notes

- OpenEmbedded layer index `https://layers.openembedded.org/layerindex/branch/master/recipes/`
- Manifest comes from `https://source.codeaurora.org/external/imx/imx-manifest`
- Graphics recipes come from `https://source.codeaurora.org/external/imx/meta-imx/`
- Graphics blobs can be obtained from `https://www.nxp.com/lgfiles/NMG/MAD/YOCTO/<file-name>` (e.g. `imx-gpu-viv-6.4.3.p2.0-aarch64.bin`)
- Clean bitbake completely with `bitbake world -c cleansstate` (or `-c cleanall` - also deletes downloaded files)

## Booting

To boot the board using novaboot-shell (after having your ssh key registered):
```
Host thermac-boot
	HostName rtime.ciirc.cvut.cz
	User imx8
	IdentityFile ~/.ssh/id_something
```

To ssh into a booted board (assuming sshd is set up) using a proxy jump from ritchie:
```
Host thermac-root
	HostName imx8
	User root
	ProxyJump ritchie
```

Then, on one shell `ssh thermac-boot` and keep it open to hold the lock. In another shell `ssh thermac-root` to ssh into the board.

### Novaboot

See `misc/*.nova` scripts for examples.

### NFS

Userspace NFS server can be used to serve the image files over network. Compile the https://github.com/skoudmar/unfs3 project with the patch included in `misc/unfs3-chown.patch` to hack around chown/chmod permission issues.
