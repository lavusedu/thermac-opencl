# OpenCL Experiemnts

## Contents

- `libs` - target architecture libraries needed to cross-compile
- `opencl-ffi` - FFI bindings to OpenCL
- `opencl-lib` - Rust library on top of FFI bindings
- `opencl-experiements` - OpenCL experiments

Note: After cloning the repository, don't forget to `git submodule update --init` to pull in the OpenCL headers (or disable the `run_bindgen` feature in `opencl-ffi` crate).

## Building

### Local

Packages needed: `rustup`

For the `info` tool:

- `cargo run --release --bin info`

(analogically for other binaries)

### Cross

Packages needed (on host): `rustup` `cross-aarch64-linux-gnu`

For the `info` tool:

- `cargo build --release --bin info --target aarch64-unknown-linux-gnu`
- `scp target/aarch64-unknown-linux-gnu/release/info thermac-root:` (ssh config below)

(analogically for other binaries)

## Running

Packages needed (on target): `glibc` TODO OpenCL ICD I guess

Run the executable.