#!/bin/sh

set -e

cargo build --release --bin mb2 --target aarch64-unknown-linux-gnu
cargo build --release --bin clpeak_global_bandwidth --target aarch64-unknown-linux-gnu
cargo build --release --bin int_compute --target aarch64-unknown-linux-gnu
cargo build --release --bin overhead --target aarch64-unknown-linux-gnu
# cargo build --bin info --target aarch64-unknown-linux-gnu

	# target/aarch64-unknown-linux-gnu/debug/info \
scp \
	target/aarch64-unknown-linux-gnu/release/mb2 \
	target/aarch64-unknown-linux-gnu/release/clpeak_global_bandwidth \
	target/aarch64-unknown-linux-gnu/release/int_compute \
	target/aarch64-unknown-linux-gnu/release/overhead \
	adasmark/adasmark \
	thermac-root:stage/experiments/

scp \
	runner/runner.py \
	runner/experiments.json \
	runner/sensors.imx8 \
	runner/get_gpu_freq.py \
	runner/sleepand.sh \
	thermac-root:stage/

scp \
	adasmark/pipeline-*.cfg \
	thermac-root:adasmark/pipelines/
