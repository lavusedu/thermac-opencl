use std::{path::Path, borrow::Cow};

use crate::cli::{AxisSelector};

pub mod opencl;
pub mod vulkan;

#[derive(Clone, Copy, Debug)]
pub struct WorkRange {
	pub work_offset: [usize; 2],
	pub work_size: [usize; 2],
	pub group_size: [usize; 2]
}
impl WorkRange {
	fn compute_work_size(factor: f32, item_count: usize, group_size: usize) -> [usize; 2] {
		assert!(factor >= 0.0);
		assert!(factor <= 1.0);
		
		let first = (
			(1.0 - factor) * (item_count as f32) / (group_size as f32)
		).floor() as usize * group_size;

		[first, item_count - first]
	}

	pub fn new_split(
		axis: AxisSelector,
		group_size: [usize; 2],
		item_count: [usize; 2],
		factor: f32
	) -> (Self, Self) {
		match axis {
			AxisSelector::X => Self::new_split_x(group_size, item_count, factor),
			AxisSelector::Y => Self::new_split_y(group_size, item_count, factor)
		}
	}

	pub fn new_split_x(
		group_size: [usize; 2],
		item_count: [usize; 2],
		factor: f32
	) -> (Self, Self) {
		let work_size = Self::compute_work_size(factor, item_count[0], group_size[0]);
		
		(
			WorkRange {
				work_offset: [0, 0],
				work_size: [work_size[0], item_count[1]],
				group_size
			},
			WorkRange {
				work_offset: [work_size[0], 0],
				work_size: [work_size[1], item_count[1]],
				group_size
			}
		)
	}

	pub fn new_split_y(
		group_size: [usize; 2],
		item_count: [usize; 2],
		factor: f32
	) -> (Self, Self) {
		let work_size = Self::compute_work_size(factor, item_count[1], group_size[1]);
		
		(
			WorkRange {
				work_offset: [0, 0],
				work_size: [item_count[0], work_size[0]],
				group_size
			},
			WorkRange {
				work_offset: [0, work_size[0]],
				work_size: [item_count[0], work_size[1]],
				group_size
			}
		)
	}
	
	pub fn is_empty(&self) -> bool {
		self.work_size[0] * self.work_size[1] == 0
	}
}

#[derive(Debug)]
pub enum ProgramSource<'a> {
	String(Cow<'a, str>),
	Bytes(Cow<'a, [u8]>)
}

#[derive(Debug)]
pub struct InstanceCreateInfo<'a> {
	pub device_selector: &'a str,
	pub work_range: WorkRange,
	pub program_source: ProgramSource<'a>,
	pub program_entry: Cow<'a, str>,
	pub program_cache_dir: Option<&'a Path>
}

pub trait WorkInstance: Sized {
	type Fence: WorkFence;
	type OutputElement: Copy;

	fn run(&mut self) -> anyhow::Result<Self::Fence>;
	fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>>;
}

pub trait WorkFence {
	fn wait(&self) -> anyhow::Result<()>;
}
