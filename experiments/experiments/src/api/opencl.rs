use std::{rc::Rc, path::{Path, PathBuf}, borrow::Borrow};

use anyhow::Context as AnyhowContext;
use opencl_lib::prelude::*;

use crate::{
	api::{WorkFence, WorkRange, ProgramSource},
	util::DeviceSelectorPart
};

pub type OpenclFence = Event<Rc<Context>>;

pub struct OpenclInstanceBase {
	pub context: Rc<Context>,
	pub kernel: Kernel<Rc<Context>, Rc<BuiltKernelProgram<Rc<Context>>>>,
	pub queue: CommandQueue<Rc<Context>>,
	pub work_range: WorkRange
}
impl OpenclInstanceBase {
	pub fn new(info: super::InstanceCreateInfo<'_>) -> anyhow::Result<Self> {
        let (platform, device) = Self::select_platform_and_device(info.device_selector)?;

		let context = Rc::new(
			Context::new(vec![device])?
		);

		// -cl-uniform-work-group-size
		let program = {
			let cache_device_selector = format!(
				"{}:{}",
				platform.get_info()?.name,
				device.get_basic_info()?.name
			);

			let program = match info.program_cache_dir.as_deref().and_then(
				|cache_dir| Self::load_cached_program(cache_dir, &cache_device_selector)
			) {
				Some(bits) => KernelProgram::from_binaries(
					context.clone(),
					[bits.as_slice()]
				)?,
				None => match info.program_source {
					ProgramSource::String(string) => KernelProgram::from_sources(
						context.clone(),
						[string.borrow()]
					)?,
					ProgramSource::Bytes(bytes) => KernelProgram::from_binaries(
						context.clone(),
						[bytes.borrow()]
					)?
				}
			};

			let program = Rc::new(
				program.build()?
			);

			if let Some(path) = info.program_cache_dir.as_deref() {
				Self::store_cached_program(
					path,
					&cache_device_selector,
					program.get_program_binaries()?.into_iter().next().unwrap()
				)
			};

			program
		};

		let kernel = Kernel::new(
			program,
			&info.program_entry
		)?;

		let queue = CommandQueue::new(
			context.clone(),
			&context.devices().nth(0).unwrap(),
			CommandQueueProperties::NONE
		)?;
		
		Ok(
			Self {
				context,
				kernel,
				queue,
				work_range: info.work_range
			}
		)
    }
	
	pub fn select_platform_and_device(
		device_selector: &str
	) -> anyhow::Result<(PlatformId, DeviceId)> {
		let (platform_selector, device_selector) = match device_selector.split_once(":") {
			None => (DeviceSelectorPart::default(), DeviceSelectorPart::from_str(device_selector)),
			Some((platform, device)) => (DeviceSelectorPart::from_str(platform), DeviceSelectorPart::from_str(device))
		};
		
		let platform = {
			let platforms = PlatformId::query()?;
	
			match platform_selector {
				DeviceSelectorPart::Index(index) => platforms.into_iter().nth(index),
				DeviceSelectorPart::Name(needle) => platforms.into_iter().find(
					|p| match p.get_info() {
						Err(err) => {
							log::error!("Could not get platform info: {}", err);
							false
						}
						Ok(info) => &info.name == needle
					}
				)
			}.context("Could not find a platform fitting the requirements")?
		};
		log::debug!("Selected platform: {}", platform);
	
		let device = {
			let devices = DeviceId::query(platform, DeviceType::ALL)?;
	
			match device_selector {
				DeviceSelectorPart::Index(index) => devices.into_iter().nth(index),
				DeviceSelectorPart::Name(needle) => devices.into_iter().find(
					|d| match d.get_info() {
						Err(err) => {
							log::error!("Could not get device info: {}", err);
							false
						}
						Ok(info) => &info.basic.name == needle
					}
				)
			}.context("Could not find a platform fitting the requirements")?
		};
		log::debug!("Selected device: {}", device);
	
		Ok(
			(platform, device)
		)
	}

	fn cached_program_name(device_selector: &str) -> String {
		let exec_name_os = std::env::args_os().next();

		let exec_name = exec_name_os.as_ref().and_then(
			|name| Path::new(name).file_name() 
		).and_then(|n| n.to_str()).unwrap_or("<unknown>");

		format!("cl_{}_program-{}.bin", exec_name, device_selector)
	}

	pub fn load_cached_program(
		cache_dir: &Path,
		device_selector: &str
	) -> Option<Vec<u8>> {
		let file_path = PathBuf::from(cache_dir).join(
			Self::cached_program_name(device_selector)
		);
		
		match std::fs::read(&file_path) {
			Err(err) => {
				log::warn!("Could not load program binaries from {}: {}", file_path.display(), err);
				None
			}
			Ok(bits) => Some(bits)
		}
	}

	pub fn store_cached_program(
		cache_dir: &Path,
		device_selector: &str,
		bits: Vec<u8>
	) {
		let file_path = PathBuf::from(cache_dir).join(
			Self::cached_program_name(device_selector)
		);

		if file_path.exists() {
			return;
		}

		log::debug!("Writing program binaries to {}", file_path.display());
		match std::fs::write(&file_path, bits) {
			Err(err) => {
				log::warn!("Could not store program binaries to {}: {}", file_path.display(), err)
			}
			Ok(()) => ()
		}
	}

	pub fn run(&mut self) -> anyhow::Result<OpenclFence> {
        log::trace!("Running OpenCL work: {:?}", self.work_range);

		let finish_event = self.kernel.enqueue_nd_range_with_event(
			&self.queue,
			Some(self.work_range.work_offset.map(|x| x as _)),
			self.work_range.work_size.map(|x| x as _),
			Some(self.work_range.group_size.map(|x| x as _)),
			None
		)?;

		Ok(finish_event)
    }

	pub fn run_without_fence(&mut self) -> anyhow::Result<()> {
		log::trace!("Running OpenCL work: {:?}", self.work_range);

		self.kernel.enqueue_nd_range(
			&self.queue,
			Some(self.work_range.work_offset.map(|x| x as _)),
			self.work_range.work_size.map(|x| x as _),
			Some(self.work_range.group_size.map(|x| x as _)),
			None
		)?;

		Ok(())
	}

	pub fn wait_queue(&mut self) -> anyhow::Result<()> {
		self.queue.wait()?;

		Ok(())
	}
}
impl WorkFence for OpenclFence {
    fn wait(&self) -> anyhow::Result<()> {
        self.wait()?;

		Ok(())
    }
}
