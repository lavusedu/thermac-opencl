use std::{borrow::Borrow, ops::Deref, ffi::CStr, num::NonZeroU32};

use anyhow::Context as AnyhowContext;
use vulkayes_core::{
	self as vy,
	ash,
	prelude::*,
	util::WaitTimeout,
	memory::device::naive::NaiveDeviceMemoryAllocator
};
use ash::vk;

use crate::{
	api::{WorkFence, WorkRange, ProgramSource},
	util::DeviceSelectorPart
};

pub type VulkanFence = Vrc<Fence>;

pub struct VulkanInstanceBase {
	pub device: Vrc<Device>,
	pub queue: Vrc<Queue>,
	pub device_memory_allocator: NaiveDeviceMemoryAllocator,
	pub command_pool: Vrc<CommandPool>,
	pub fence: Vrc<Fence>,
	pub command_buffer: Vrc<CommandBuffer>,

	pub shader: Vrc<ShaderModule>,

	pub descriptor_sets: Vec<Vrc<DescriptorSet>>,
	pub pipeline_layout: Vrc<PipelineLayout>,
	pub pipeline: Vrc<ComputePipeline>,
	
	pub work_range: WorkRange
}
impl VulkanInstanceBase {
	pub fn new(
		info: super::InstanceCreateInfo<'_>,
		debug: bool,
		descriptor_layout_bindings: Vec<DescriptorSetLayoutBinding>,
		shader_specializations: impl SpecializationConstantsTrait
	) -> anyhow::Result<Self> {
		let instance = Instance::new(
			Entry::new()?,
			ApplicationInfo {
				application_name: "Experiment",
				engine_name: "Thermac experiments",
				api_version: VkVersion::new(1, 2, 0),
				..Default::default()
			},
			if debug { Some(CStr::from_bytes_with_nul(b"VK_LAYER_KHRONOS_validation\0").unwrap()) } else { None },
			if debug { Some(ash::extensions::ext::DebugUtils::name()) } else { None },
			Default::default(),
			if debug { vy::instance::debug::DebugCallback::Default() } else { vy::instance::debug::DebugCallback::None() }
		)?;
		let (device, queue) = Self::select_device(info.device_selector, instance)?;

		let device_memory_allocator = NaiveDeviceMemoryAllocator::new(device.clone());

		let command_pool = CommandPool::new(
			&queue,
			vk::CommandPoolCreateFlags::empty(),
			Default::default()
		)?;
		let [command_buffer] = CommandBuffer::new(command_pool.clone(), false)?;

		let fence = Fence::new(
			device.clone(),
			false,
			Default::default()
		)?;

		let shader = {
			// let cache_device_selector = format!(
			// 	"{:0>4x}:{:0>4x}",
			// 	device.physical_device().properties().vendor_id,
			// 	device.physical_device().properties().device_id, device.physical_device().properties().device_name
			// );
			
			// TODO: program cache
			match info.program_source {
				ProgramSource::String(_) => unimplemented!("Cannot create shader from string for Vulkan API"),
				ProgramSource::Bytes(bytes) =>  ShaderModule::new(
					device.clone(),
					ShaderModule::load_spirv_bytes(
						bytes.borrow()
					)?,
					Default::default()
				)?
			}
		};
		let shader_entry = match info.program_entry.borrow() {
			"main" => ShaderEntryPoint::Main,
			_ => todo!()
		};
		let descriptor_sets = Self::create_descriptor(device.clone(), descriptor_layout_bindings)?;
		let (pipeline_layout, pipeline) = Self::create_pipeline(
			device.clone(),
			descriptor_sets.as_slice(),
			shader.deref(),
			shader_entry,
			shader_specializations
		)?;

		Ok(
			Self {
				device,
				queue,
				device_memory_allocator,
				command_pool,
				fence,
				command_buffer,

				shader,

				descriptor_sets,
				pipeline_layout,
				pipeline,

				work_range: info.work_range
			}
		)
	}

	fn select_device(device_selector: &str, instance: Vrc<Instance>) -> anyhow::Result<(Vrc<Device>, Vrc<Queue>)> {
		let device_selector = DeviceSelectorPart::from_str(device_selector);
		
		let mut devices = instance.physical_devices()?;
		let physical_device = match device_selector {
			DeviceSelectorPart::Index(index) => devices.nth(index),
			DeviceSelectorPart::Name(needle) => devices.find(
				|device| device.properties().device_name.deref() == needle
			)
		}.context("Could not find a platform fitting the requirements")?;
		log::debug!(
			"Physical device: {:#?}\nExtensions: {:#?}\nMemory: {:#?}\nProperties: {:#?}\nFeatures: {:#?}",
			physical_device,
			physical_device.extensions_properties()?.collect::<Vec<_>>(),
			physical_device.memory_properties(),
			physical_device.properties(),
			physical_device.features()
		);

		let queue_family_index = physical_device.queue_family_properties().into_iter().enumerate().find(
			|(_, qf)| qf.queue_flags.contains(vk::QueueFlags::COMPUTE)
		).context("Selected device does not support compute operations")?.0;
		

		let vy::device::DeviceData { device, queues } = {
			let queues = [vy::device::QueueCreateInfo { queue_family_index: queue_family_index as u32, queue_priorities: [1.0] }];
	
			// create info pointers are valid because they are kept alive by queues argument
			let queue_create_infos: Vec<_> = queues
				.iter()
				.map(|q| {
					vk::DeviceQueueCreateInfo::builder()
						.queue_family_index(q.queue_family_index)
						.queue_priorities(q.queue_priorities.as_ref())
						.build()
				})
				.collect();

			let features = vk::PhysicalDeviceFeatures::default();
			let create_info = vk::DeviceCreateInfo::builder()
				.queue_create_infos(&queue_create_infos)
				.enabled_layer_names(&[])
				.enabled_extension_names(&[])
				.enabled_features(&features)
			;
	
			unsafe {
				Device::from_create_info(
					physical_device,
					create_info,
					Default::default()
				)?
			}
		};

		let queue = queues.into_iter().next().unwrap();

		Ok((device, queue))
	}

	fn create_descriptor(
		device: Vrc<Device>,
		layout_bindings: Vec<DescriptorSetLayoutBinding>
	) -> anyhow::Result<Vec<Vrc<DescriptorSet>>> {
		let mut descriptor_sets = Vec::with_capacity(layout_bindings.len());

		for layout_binding in layout_bindings {
			let descriptor_set_layout = DescriptorSetLayout::new(
				device.clone(),
				vk::DescriptorSetLayoutCreateFlags::empty(),
				std::iter::once(layout_binding),
				Default::default()
			)?;

			let layout_info: vk::DescriptorSetLayoutBindingBuilder = layout_binding.into();
			if layout_info.descriptor_count == 0 {
				unimplemented!()
			}

			let descriptor_set_pool = DescriptorPool::new(
				device.clone(),
				vk::DescriptorPoolCreateFlags::empty(),
				NonZeroU32::new(1).unwrap(),
				std::iter::once(DescriptorPoolSize {
					descriptor_type: layout_info.descriptor_type,
					count: NonZeroU32::new(layout_info.descriptor_count).unwrap()
				}),
				None,
				Default::default()
			)?;

			let descriptor_set = DescriptorSet::new(
				descriptor_set_pool,
				descriptor_set_layout
			)?;

			descriptor_sets.push(descriptor_set);
		}

		Ok(descriptor_sets)
	}

	fn create_pipeline<'a>(
		device: Vrc<Device>,
		descriptor_set_layout: &[Vrc<DescriptorSet>],
		shader: &ShaderModule,
		shader_entry: ShaderEntryPoint,
		shader_specializations: impl SpecializationConstantsTrait
	) -> anyhow::Result<(Vrc<PipelineLayout>, Vrc<ComputePipeline>)> {
		let descriptor_layouts: Vec<_> = descriptor_set_layout.iter().map(
			|ds| ds.layout().safe_handle()
		).collect();
		
		let pipeline_layout = PipelineLayout::new(
			device.clone(),
			descriptor_layouts,
			[],
			Default::default()
		)?;

		log::debug!("Shader specializations: {:#?}", shader_specializations);
		
		vy::describe_compute_pipeline! {
			let pipeline_create_info;

			Shaders {
				stage: shader, shader_entry, shader_specializations => vk::ShaderStageFlags::COMPUTE
			}
	
			Deps {
				layout: pipeline_layout
			}
		};

		let pipeline = unsafe {
				ComputePipeline::from_create_info(
				device,
				pipeline_create_info,
				Default::default()
			)?
		};

		Ok(
			(pipeline_layout, pipeline)
		)
	}

	fn run_record(&mut self) -> anyhow::Result<()> {
		log::trace!("Running Vulkan work: {:?}", self.work_range);

		// record the buffer
		self.command_pool.reset(false)?;
		{
			let cb = self.command_buffer.begin_recording(CommandBufferBeginInfo::OneTime)?;

			cb.bind_compute_pipeline(&self.pipeline);

			if self.descriptor_sets.len() > 0 {
				let handles: Vec<_> = self.descriptor_sets.iter().map(
					|ds| ds.safe_handle()
				).collect();

				cb.bind_descriptor_sets(
					vk::PipelineBindPoint::COMPUTE,
					&self.pipeline_layout,
					0,
					handles,
					[]
				);
			}

			let work_range = self.work_range;
			cb.dispatch_base(
				[work_range.work_offset[0] as u32, work_range.work_offset[1] as u32, 0],
				[
					(work_range.work_size[0] / work_range.group_size[0]) as u32,
					(work_range.work_size[1] / work_range.group_size[1]) as u32,
					1
				]
			);

			cb.end()?;
		}

		Ok(())
	}

	pub fn run(&mut self) -> anyhow::Result<VulkanFence> {
		self.run_record()?;

		// submit the buffer
		self.fence.reset()?;
		self.queue.submit(
			[],
			[],
			[&self.command_buffer],
			[],
			Some(&self.fence)
		)?;

		Ok(self.fence.clone())
	}

	pub fn run_without_fence(&mut self) -> anyhow::Result<()> {
		self.run_record()?;

		self.queue.submit(
			[],
			[],
			[&self.command_buffer],
			[],
			None
		)?;

		Ok(())
	}

	pub fn wait_queue(&mut self) -> anyhow::Result<()> {
		self.queue.wait()?;

		Ok(())
	}
}
impl WorkFence for VulkanFence {
	fn wait(&self) -> anyhow::Result<()> {
		Fence::wait(self.deref(), WaitTimeout::Forever)?;

		Ok(())
	}
}
