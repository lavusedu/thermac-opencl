use std::str::FromStr;

pub use clap;

#[derive(Clone, Copy, Debug)]
pub enum AxisSelector {
	X,
	Y
}
impl Default for AxisSelector {
	fn default() -> Self {
		Self::X
	}
}
impl FromStr for AxisSelector {
	type Err = anyhow::Error;

	fn from_str(value: &str) -> Result<Self, Self::Err> {
		let result = match value {
			"x" => Self::X,
			"y" => Self::Y,
			_ => anyhow::bail!("Invalid axis selector")
		};

		Ok(result)
	}
}

#[derive(Clone, Copy, Debug)]
pub enum ApiSelector {
	OpenCL,
	Vulkan
}
impl Default for ApiSelector {
	fn default() -> Self {
		Self::OpenCL
	}
}
impl FromStr for ApiSelector {
	type Err = anyhow::Error;

	fn from_str(value: &str) -> Result<Self, Self::Err> {
		let result = match value {
			"opencl" => Self::OpenCL,
			"vulkan" => Self::Vulkan,
			_ => anyhow::bail!("Invalid api selector")
		};

		Ok(result)
	}
}

#[derive(Clone, Copy, Debug)]
pub enum VerbositySelector {
	Error,
	Warn,
	Info,
	Debug,
	Trace
}
impl From<VerbositySelector> for log::Level {
	fn from(value: VerbositySelector) -> log::Level {
		match value {
			VerbositySelector::Error => log::Level::Error,
			VerbositySelector::Warn => log::Level::Warn,
			VerbositySelector::Info => log::Level::Info,
			VerbositySelector::Debug => log::Level::Debug,
			VerbositySelector::Trace => log::Level::Trace
		}
	}
}
impl Default for VerbositySelector {
	fn default() -> Self {
		Self::Info
	}
}
impl FromStr for VerbositySelector {
	type Err = anyhow::Error;

	fn from_str(value: &str) -> Result<Self, Self::Err> {
		let result = match value {
			"error" => Self::Error,
			"warn" => Self::Warn,
			"info" => Self::Info,
			"debug" => Self::Debug,
			"trace" => Self::Trace,
			_ => anyhow::bail!("Invalid verbosity selector")
		};

		Ok(result)
	}
}

#[macro_export]
macro_rules! cli {
	(
		@inherit(BaseCli)
		App $app_name: expr => $struct_vis: vis struct $struct_name: ident {
			$(
				$( #[$arg_attr: meta] )*
				$arg_about: literal
				$arg_name: ident: $arg_type: ty $( = $arg_default: expr )?
			),* $(,)?
		}
	) => {
		$crate::cli! {
			App $app_name => $struct_vis struct $struct_name {
				"graphics API to use"
				api: ApiSelector = "opencl",

				"first device index or name"
				device1: String = "0",
				"second device index or name"
				device2: String = "1:0",

				"path to a directory from which a previously compiled kernel program binary will be loaded instead of building the program from sources; program binaries will also be saved here if provided"
				program_binary_dir: Option<String>,

				"local work size in x axis; should divide width"
				work_group_size_x: usize = "32",
				"local work size in y axis; should divide height"
				work_group_size_y: usize = "1",

				"number of times to rerun the kernel"
				run_count: usize = "1",
				"minimum time for one kernel run; if a run takes less the thread sleeps"
				run_min_time: f32 = "0.0",

				"work share factor; 0.0 means all work on first device, 1.0 means all work on second device; granularity is work_group_size_x"
				work_share_factor: f32 = "0.0",
				"whether to share work across axis y instead of x"
				work_share_axis: AxisSelector = "x",

				"logging verbosity"
				log_verbosity: VerbositySelector = "info",
				"path where to dump output"
				dump_output_path: Option<String>,
				
				$(
					$( #[$arg_attr] )*
					$arg_about
					$arg_name: $arg_type $( = $arg_default )?
				),*
			}
		}
	};
	
	(
		App $app_name: expr => $struct_vis: vis struct $struct_name: ident {
			$(
				$( #[$arg_attr: meta] )*
				$arg_about: literal
				$arg_name: ident: $arg_type: ty $( = $arg_default: expr )?
			),* $(,)?
		}
	) => {
		use $crate::cli::clap::Parser;

		#[derive(Debug, Parser)]
		#[clap(version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"))]
		$struct_vis struct $struct_name {
			$(
				#[clap(
					long,
					help = $arg_about,
					forbid_empty_values = true,
					$( default_value = $arg_default, )?
				)]
				$( #[$arg_attr] )*
				pub $arg_name: $arg_type
			),*
		}
		impl $struct_name {
			pub fn parse() -> anyhow::Result<Self> {
				use $crate::cli::clap::{Parser, ErrorKind};

				let result = $struct_name::try_parse().map_err(
					|err| match err.kind {
						ErrorKind::DisplayHelp | ErrorKind::DisplayVersion => anyhow::anyhow!("{}", err),
						_ => anyhow::anyhow!("Failed to parse stdargs: {}", err)
					}
				);

				log::debug!("Cli: {:?}", result);

				result
			}
		}
	};
}


#[derive(Debug, Clone, Copy)]
pub enum VectorSizeSelector {
	Size1,
	Size2,
	Size4,
	Size8,
	Size16
}
impl Default for VectorSizeSelector {
	fn default() -> Self {
		Self::Size1
	}
}
impl FromStr for VectorSizeSelector {
	type Err = anyhow::Error;

	fn from_str(value: &str) -> Result<Self, Self::Err> {
		let result = match value {
			"1" => Self::Size1,
			"2" => Self::Size2,
			"4" => Self::Size4,
			"8" => Self::Size8,
			"16" => Self::Size16,
			_ => anyhow::bail!("Invalid vector size selector")
		};

		Ok(result)
	}
}
impl VectorSizeSelector {
	pub fn as_ident(&self) -> &'static str {
		match self {
			Self::Size1 => "v1",
			Self::Size2 => "v2",
			Self::Size4 => "v4",
			Self::Size8 => "v8",
			Self::Size16 => "v16"
		}
	}

	pub fn as_num(&self) -> usize {
		match self {
			Self::Size1 => 1,
			Self::Size2 => 2,
			Self::Size4 => 4,
			Self::Size8 => 8,
			Self::Size16 => 16
		}
	}
}

#[derive(Debug)]
pub enum OffsetTypeSelector {
	Local,
	Global
}
impl Default for OffsetTypeSelector {
	fn default() -> Self {
		Self::Local
	}
}
impl FromStr for OffsetTypeSelector {
	type Err = anyhow::Error;

	fn from_str(value: &str) -> Result<Self, Self::Err> {
		let result = match value {
			"local" => Self::Local,
			"global" => Self::Global,
			_ => anyhow::bail!("Invalid offset type selector")
		};

		Ok(result)
	}
}
impl OffsetTypeSelector {
	pub fn as_ident(&self) -> &'static str {
		match self {
			Self::Local => "local_offset",
			Self::Global => "global_offset"
		}
	}
}
