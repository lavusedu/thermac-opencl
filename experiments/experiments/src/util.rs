use rand::{self, distributions::Standard, prelude::Distribution, Rng, SeedableRng};

use crate::prelude::WorkRange;

#[derive(Debug)]
pub enum DeviceSelectorPart<'a> {
	Index(usize),
	Name(&'a str)
}
impl<'a> DeviceSelectorPart<'a> {
	pub fn from_str(value: &'a str) -> Self {
		match value.parse::<usize>() {
			Err(_) => Self::Name(value),
			Ok(index) => Self::Index(index)
		}
	}
}
impl Default for DeviceSelectorPart<'_> {
	fn default() -> Self {
		DeviceSelectorPart::Index(0)
	}
}
impl std::fmt::Display for DeviceSelectorPart<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			DeviceSelectorPart::Index(index) => write!(f, "{}", index),
			DeviceSelectorPart::Name(name) => write!(f, "{}", name)
		}
	}
}

struct RandIter<R: Rng, Rt> {
	rng: R,
	_boo: std::marker::PhantomData<Rt>
}
impl<R: Rng, Rt> Iterator for RandIter<R, Rt>
where
	Standard: Distribution<Rt>
{
	type Item = Rt;

	fn next(&mut self) -> Option<Self::Item> {
		Some(self.rng.gen())
	}
}

pub fn rand_values<T>(seed: u64) -> impl Iterator<Item = T>
where
	Standard: Distribution<T>
{	
	RandIter {
		rng: rand::rngs::SmallRng::seed_from_u64(seed),
		_boo: std::marker::PhantomData
	}
}

pub fn merge_work_buffers<T: Copy>(
	destination: &mut [T], destination_width: usize,
	source: &[T], source_range: WorkRange
) {
	for y in source_range.work_offset[1] .. source_range.work_offset[1] + source_range.work_size[1] {
		let x = (y * destination_width + source_range.work_offset[0]) .. (y * destination_width + source_range.work_offset[0] + source_range.work_size[0]);
		
		destination[x.clone()].copy_from_slice(&source[x]);
	}
}
