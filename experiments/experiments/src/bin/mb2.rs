use std::path::Path;

use anyhow::Context;

use thermac_experiments as common;

common::cli! {
	@inherit(BaseCli)
	App env!("CARGO_BIN_NAME") => pub struct CliArgs {
		"total width of the image"
		width: usize = "3072",
		"total height of the image"
		height: usize = "3072",

		"maximum number of iterations per pixel"
		max_pixel_iter: usize = "8192",
		"escape radius of the function; radius of the cutoff circle"
		escape_radius: f32 = "Inf",
		
		"kernel to run - pretty, black or nocompute"
		kernel: String = "pretty",
		"skip writing the result into a buffer and instead just use volatile instruction to prevent optimizing out the computation"
		no_write: u8 = "0"
	}
}

common::main! {
	let cli: CliArgs;
	let item_count = [cli.width, cli.height];

	match api {
		OpenCL => {
			RUN_COUNT = 1,
			create = fn create_opencl -> Result<OpenclInstance>
		}
		Vulkan => {
			RUN_COUNT = 1,
			create = fn create_vulkan -> Result<VulkanInstance>
		}
	}

	fn dump_output;
}

fn dump_output(cli: &CliArgs, path: &Path, buffer: Vec<u8>) -> anyhow::Result<()> {
	#[cfg(feature = "output_check")]
	{
		use image::{GrayImage, ImageFormat};

		let image = GrayImage::from_vec(
			cli.width as u32,
			cli.height as u32,
			buffer
		).context("could not create output image")?;

		log::trace!("Saving output image");
		image.save_with_format(path, ImageFormat::Png)?;
	}

	#[cfg(not(feature = "output_check"))]
	{
		unimplemented!("this binary was not compiled with support for output dump")
	}

	Ok(())
}

use ocl::{create_opencl, OpenclInstance};
mod ocl {
	use std::rc::Rc;

	use anyhow::Context as AnyhowContext;
	use opencl_lib::prelude::*;
	
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				opencl::*
			}
		}
	};

	pub struct OpenclInstance {
		inner: OpenclInstanceBase,
		buffer: Buffer<Rc<Context>>
	}
	impl OpenclInstance {
		fn new(
			info: InstanceCreateInfo,
			image_size: [usize; 2]
		) -> anyhow::Result<Self> {
			let mut inner = OpenclInstanceBase::new(info)?;

			let buffer_size = image_size[0].checked_mul(image_size[1]).context("width * height overflow")?;
			let buffer = Buffer::new(
				inner.context.clone(),
				BufferMemoryFlags::KERNEL_WO,
				buffer_size as _
			)?;

			inner.kernel.set_arg(0, &buffer.handle())?;
			
			Ok(
				Self {
					inner,
					buffer
				}
			)
		}
	}
	impl WorkInstance for OpenclInstance {
		type Fence = OpenclFence;
		type OutputElement = u8;

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {
			let mut output_buffer: Vec<u8> = Vec::new();
			output_buffer.resize(self.buffer.len_as::<u8>() as usize, 0xAA);

			self.buffer.enqueue_read(
				&self.inner.queue,
				0,
				output_buffer.as_mut_slice(),
				None,
				true
			)?;

			Ok(output_buffer)
		}
	}
	pub fn create_opencl(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<OpenclInstance> {
		let program_source = {
			let want_no_write: bool = cli.no_write != 0;
			let escape_radius = cli.escape_radius * cli.escape_radius;
			let string = format!(
				r##"
				#define HEIGHT ({})
				#define WIDTH ({})
				#define MAX_ITER ({})
				#define ESCAPED(z) ({})
				#define NO_RESULT_WRITE {}

				{}
				"##,
				cli.height,
				cli.width,
				cli.max_pixel_iter,
				if escape_radius.is_finite() {
					format!("(z) >= {}", escape_radius)
				} else {
					format!("(z) >= INFINITY")
				},
				if want_no_write { 1 } else { 0 },
				include_str!("../../assets/kernels/mandelbrot2.cl")
			);

			ProgramSource::String(string.into())
		};

		let program_entry = match cli.kernel.as_str() {
			"pretty" => "mandelbrot_pretty",
			"black" => "mandelbrot_black",
			"nocompute" => "mandelbrot_nocompute",
			name => anyhow::bail!("Invalid kernel name \"{}\" (allowed: pretty, black, nocompute)", name)
		};

		OpenclInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			},
			[cli.width, cli.height]
		)
	}
}

/*
use vk_texbuf::{create_vulkan, VulkanInstance};
mod vk_texbuf {
	use std::{num::{NonZeroU32, NonZeroU64}, ops::Deref};

	use vulkayes_core::{
		prelude::*,
		ash::vk
	};
	
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				vulkan::*
			}
		}
	};

	vulkayes_core::shader_specialization_constants! {
		pub struct SpecializationConstants {
			layout(local_size_x_id = 0, local_size_y_id = 1) in;
			layout(constant_id = 2) const uint width;
			layout(constant_id = 3) const uint height;
			layout(constant_id = 4) const uint max_iter;
			layout(constant_id = 5) const float escape_radius;
			layout(constant_id = 6) const bool no_write;
			layout(constant_id = 7) const uint kernel;
		}
	}

	pub struct VulkanInstance {
		inner: VulkanInstanceBase,
		image_size: [usize; 2],
		buffer: Vrc<Buffer>,
		// non-trivial Drop code yo
		#[allow(dead_code)]
		buffer_view: Vrc<BufferView>
	}
	impl VulkanInstance {
		fn new(
			info: InstanceCreateInfo,
			image_size: [usize; 2],
			max_iter: usize,
			escape_radius: f32,
			no_write: u32,
			kernel: &str
		) -> anyhow::Result<Self> {
			let spec = SpecializationConstants {
				local_size_x: info.work_range.group_size[0] as u32,
				local_size_y: info.work_range.group_size[1] as u32,
				width: image_size[0] as u32,
				height: image_size[1] as u32,
				max_iter: max_iter as u32,
				escape_radius: escape_radius,
				no_write: no_write,
				kernel: match kernel {
					"pretty" | "pretty_opt" => 0,
					"black" | "black_opt" => 1,
					"nocompute" | "nocompute_opt" => 2,
					name => anyhow::bail!("Invalid kernel name \"{}\" (allowed: pretty, black, nocompute, +_opt)", name)
				}
			};
			let inner = VulkanInstanceBase::new(
				info,
				log::log_enabled!(log::Level::Debug),
				DescriptorSetLayoutBinding::Generic(
					DescriptorSetLayoutBindingGenericType::STORAGE_TEXEL_BUFFER,
					NonZeroU32::new(1).unwrap(),
					vk::ShaderStageFlags::COMPUTE
				),
				spec
			)?;

			let buffer = Buffer::new(
				inner.device.clone(),
				NonZeroU64::new((image_size[0] * image_size[1]) as u64).unwrap(),
				// wanna write to it and transfer the result out to read it
				vk::BufferUsageFlags::TRANSFER_SRC | vk::BufferUsageFlags::STORAGE_TEXEL_BUFFER,
				SharingMode::from(inner.queue.deref()),
				BufferAllocatorParams::Some {
					allocator: &inner.device_memory_allocator,
					requirements: vk::MemoryPropertyFlags::DEVICE_LOCAL
				},
				Default::default()
			)?;
			let buffer_view = BufferView::new(
				buffer.clone(),
				vk::Format::R8_UINT,
				0,
				buffer.size(),
				Default::default()
			)?;

			{
				let descriptor_set_lock = inner.descriptor_set.lock_safe_handle();
				DescriptorSet::update(
					&inner.device,
					&[
						DescriptorSetWrite::new(
							descriptor_set_lock.borrow_safe(),
							0,
							0,
							DescriptorSetWriteData::TexelBuffer(
								DescriptorTypeTexelBuffer::STORAGE_TEXEL_BUFFER,
								&[buffer_view.safe_handle()]
							)
						)?
					],
					&[]
				);
			}
			
			Ok(
				Self {
					inner,
					image_size,
					buffer,
					buffer_view
				}
			)
		}
	}
	impl WorkInstance for VulkanInstance {
		type Fence = VulkanFence;
		type OutputElement = u8;

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {			
			let size = self.image_size[0] * self.image_size[1];

			let output_buffer = Buffer::new(
				self.inner.device.clone(),
				NonZeroU64::new(size as u64).unwrap(),
				vk::BufferUsageFlags::TRANSFER_DST,
				SharingMode::from(self.inner.queue.deref()),
				BufferAllocatorParams::Some {
					allocator: &self.inner.device_memory_allocator,
					requirements: vk::MemoryPropertyFlags::HOST_VISIBLE
				},
				Default::default()
			)?;
	
			self.inner.command_pool.reset(false)?;
			{
				let cb = self.inner.command_buffer.begin_recording(CommandBufferBeginInfo::OneTime)?;
				cb.copy_buffer_to_buffer(
					&self.buffer,
					&output_buffer,
					[BufferBufferCopy::new(0, 0, self.buffer.size())]
				);
	
				cb.end()?;
			}
	
			self.inner.fence.reset()?;
			self.inner.queue.submit(
				[],
				[],
				[&self.inner.command_buffer],
				[],
				Some(&self.inner.fence)
			)?;
			self.inner.fence.wait()?;
	
			let mut output = vec![0u8; size];
			output_buffer.memory().unwrap().map_memory_with(
				|mut access| {
					output.as_mut_slice().copy_from_slice(
						access.bytes_mut()
					);
	
					MappingAccessResult::Unmap
				}
			)?;

			Ok(output)
		}
	}

	pub fn create_vulkan(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<VulkanInstance> {
		let program_source = if cli.kernel.ends_with("_opt") {
			ProgramSource::Bytes(
				include_bytes!("../../assets/glsl/mandelbrot2_texbuf_opt.spv").as_slice().into()
			)
		} else {
			ProgramSource::Bytes(
				include_bytes!("../../assets/glsl/mandelbrot2_texbuf.spv").as_slice().into()
			)
		};
		let program_entry = "main";

		VulkanInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			},
			[cli.width, cli.height],
			cli.max_pixel_iter,
			cli.escape_radius,
			cli.no_write as u32,
			&cli.kernel
		)
	}
}
*/
use vk_image::{create_vulkan, VulkanInstance};
mod vk_image {
	use std::{num::{NonZeroU32, NonZeroU64}, ops::Deref};

	use vulkayes_core::{
		prelude::*,
		ash::vk
	};
	
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				vulkan::*
			}
		}
	};

	vulkayes_core::shader_specialization_constants! {
		pub struct SpecializationConstants {
			layout(local_size_x_id = 0, local_size_y_id = 1) in;
			layout(constant_id = 2) const uint width;
			layout(constant_id = 3) const uint height;
			layout(constant_id = 4) const uint max_iter;
			layout(constant_id = 5) const float escape_radius;
			layout(constant_id = 6) const bool no_write;
			layout(constant_id = 7) const uint kernel;
		}
	}

	pub struct VulkanInstance {
		inner: VulkanInstanceBase,
		image_size: [usize; 2],
		image: Vrc<Image>,
		// non-trivial Drop code yo
		#[allow(dead_code)]
		image_view: Vrc<ImageView>
	}
	impl VulkanInstance {
		const IMAGE_LAYOUT: ImageLayoutFinal = ImageLayoutFinal::GENERAL;

		fn new(
			info: InstanceCreateInfo,
			image_size: [usize; 2],
			max_iter: usize,
			escape_radius: f32,
			no_write: u32,
			kernel: &str
		) -> anyhow::Result<Self> {
			let spec = SpecializationConstants {
				local_size_x: info.work_range.group_size[0] as u32,
				local_size_y: info.work_range.group_size[1] as u32,
				width: image_size[0] as u32,
				height: image_size[1] as u32,
				max_iter: max_iter as u32,
				escape_radius: escape_radius,
				no_write: no_write,
				kernel: match kernel {
					"pretty" | "pretty_opt" => 0,
					"black" | "black_opt" => 1,
					"nocompute" | "nocompute_opt" => 2,
					name => anyhow::bail!("Invalid kernel name \"{}\" (allowed: pretty, black, nocompute, +_opt)", name)
				}
			};
			let inner = VulkanInstanceBase::new(
				info,
				log::log_enabled!(log::Level::Debug),
				vec![
					DescriptorSetLayoutBinding::Generic(
						DescriptorSetLayoutBindingGenericType::STORAGE_IMAGE,
						NonZeroU32::new(1).unwrap(),
						vk::ShaderStageFlags::COMPUTE
					)
				],
				spec
			)?;

			let image = Image::new(
				inner.device.clone(),
				vk::Format::R8_UINT,
				ImageSize::new_2d(
					NonZeroU32::new(image_size[0] as u32).unwrap(),
					NonZeroU32::new(image_size[1] as u32).unwrap(),
					NonZeroU32::new(1).unwrap(),
					MipmapLevels::One()
				).transmute().into(),
				ImageTilingAndLayout::OptimalUndefined(),
				vk::ImageUsageFlags::TRANSFER_SRC | vk::ImageUsageFlags::STORAGE,
				SharingMode::from(inner.queue.deref()),
				ImageAllocatorParams::Some {
					allocator: &inner.device_memory_allocator,
					requirements: vk::MemoryPropertyFlags::DEVICE_LOCAL
				},
				Default::default()
			)?;
			let image_view = ImageView::new(
				image.clone().into(),
				ImageViewRange::Type2D(0, NonZeroU32::new(1).unwrap(), 0),
				None,
				vk::ComponentMapping::default(),
				vk::ImageAspectFlags::COLOR,
				Default::default()
			)?;

			inner.command_pool.reset(false)?;
			{
				let cb = inner.command_buffer.begin_recording(CommandBufferBeginInfo::OneTime)?;

				cb.pipeline_barrier(
					vk::PipelineStageFlags::ALL_COMMANDS,
					vk::PipelineStageFlags::COMPUTE_SHADER,
					[],
					[],
					[
						ImageMemoryBarrier::new(
							&image,
							image_view.subresource_range(),
							ImageLayoutSource::UNDEFINED,
							Self::IMAGE_LAYOUT,
							vk::AccessFlags::MEMORY_WRITE,
							vk::AccessFlags::MEMORY_WRITE
						)
					]
				);
	
				cb.end()?;
			}
			inner.fence.reset()?;
			inner.queue.submit(
				[],
				[],
				[&inner.command_buffer],
				[],
				Some(&inner.fence)
			)?;
			inner.fence.wait()?;

			{
				let descriptor_set = inner.descriptor_sets[0].safe_handle();
				DescriptorSet::update(
					&inner.device,
					&[
						DescriptorSetWrite::new(
							descriptor_set,
							0,
							0,
							DescriptorSetWriteData::Image(
								DescriptorTypeImage::STORAGE_IMAGE,
								&[DescriptorImageInfo::without_sampler(&image_view, Self::IMAGE_LAYOUT.into())]
							)
						)?
					],
					&[]
				);
			}
			
			Ok(
				Self {
					inner,
					image_size,
					image,
					image_view
				}
			)
		}
	}
	impl WorkInstance for VulkanInstance {
		type Fence = VulkanFence;
		type OutputElement = u8;

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {
			let size = self.image_size[0] * self.image_size[1];

			let output_buffer = Buffer::new(
				self.inner.device.clone(),
				NonZeroU64::new(size as u64).unwrap(),
				vk::BufferUsageFlags::TRANSFER_DST,
				SharingMode::from(self.inner.queue.deref()),
				BufferAllocatorParams::Some {
					allocator: &self.inner.device_memory_allocator,
					requirements: vk::MemoryPropertyFlags::HOST_VISIBLE
				},
				Default::default()
			)?;
	
			self.inner.command_pool.reset(false)?;
			{
				let cb = self.inner.command_buffer.begin_recording(CommandBufferBeginInfo::OneTime)?;

				let sub_range = self.image_view.subresource_range();
				let sub_size = self.image_view.subresource_image_size();
				cb.copy_image_to_buffer(
					&self.image,
					Self::IMAGE_LAYOUT.into(),
					&output_buffer,
					[
						BufferImageCopy::new(
							0,
							None,
							ImageSubresourceLayers::new(sub_range.aspect_mask, sub_range.mipmap_levels_base, sub_range.array_layers_base, sub_range.array_layers),
							vk::Offset3D { x: 0, y: 0, z: 0 },
							vk::Extent3D { width: sub_size.width().get(), height: sub_size.height().get(), depth: sub_size.depth().get() }
						)
					]
				);
	
				cb.end()?;
			}
			self.inner.fence.reset()?;
			self.inner.queue.submit(
				[],
				[],
				[&self.inner.command_buffer],
				[],
				Some(&self.inner.fence)
			)?;
			self.inner.fence.wait()?;
	
			let mut output = vec![0u8; size];
			output_buffer.memory().unwrap().map_memory_with(
				|mut access| {
					output.as_mut_slice().copy_from_slice(
						access.bytes_mut()
					);
	
					MappingAccessResult::Unmap
				}
			)?;

			Ok(output)
		}
	}

	pub fn create_vulkan(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<VulkanInstance> {
		let program_source = if cli.kernel.ends_with("_opt") {
			ProgramSource::Bytes(
				include_bytes!("../../assets/glsl/mandelbrot2_image_opt.spv").as_slice().into()
			)
		} else {
			ProgramSource::Bytes(
				include_bytes!("../../assets/glsl/mandelbrot2_image.spv").as_slice().into()
			)
		};
		let program_entry = "main";

		VulkanInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			},
			[cli.width, cli.height],
			cli.max_pixel_iter,
			cli.escape_radius,
			cli.no_write as u32,
			&cli.kernel
		)
	}
}
