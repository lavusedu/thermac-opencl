use std::{fs, path::Path, io::Write};

use thermac_experiments as common;
use common::cli::{VectorSizeSelector, OffsetTypeSelector};

// One work item will access this many input elements
// This also means that input size will be this many times bigger than the output size
const ONE_WORK_ITEM_ACCESS_COUNT: usize = 16;

common::cli! {
	@inherit(BaseCli)
	App env!("CARGO_BIN_NAME") => pub struct CliArgs {
		"represent the input as a vector of this size"
		vector_size: VectorSizeSelector = "1",

		"use local or global item index as an offset into the buffer"
		offset_type: OffsetTypeSelector = "local",

		"total width of the input buffer (in f32 elements)"
		width: usize = "16777216",
		
		"seed used in PRNG to generate input data"
		rand_seed: u64 = "0"
	}
}

common::main! {
	let cli: CliArgs;
	let item_count = [cli.width / ONE_WORK_ITEM_ACCESS_COUNT / cli.vector_size.as_num(), 1];

	match api {
		OpenCL => {
			RUN_COUNT = 2000,
			create = fn create_opencl -> Result<OpenclInstance>
		}
	}

	fn dump_output;
}

fn dump_output(_cli: &CliArgs, path: &Path, buffer: Vec<f32>) -> anyhow::Result<()> {
	let mut file = fs::OpenOptions::new().write(true).create(true).open(path)?;
	let mut sum: f32 = 0.0;
	for value in buffer {
		writeln!(&mut file, "{}", value)?;
		sum += value;
	}
	writeln!(&mut file, "sum = {}", sum)?;

	Ok(())
}

use ocl::{create_opencl, OpenclInstance};
mod ocl {
	use std::rc::Rc;

	use opencl_lib::prelude::*;
	
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				opencl::*
			},
			util::rand_values
		}
	};

	pub struct OpenclInstance {
		inner: OpenclInstanceBase,
		#[allow(dead_code)]
		input_buffer: Buffer<Rc<Context>>,
		output_buffer: Buffer<Rc<Context>>
	}
	impl OpenclInstance {
		fn new(
			info: InstanceCreateInfo,
			width: usize,
			rand_seed: u64
		) -> anyhow::Result<Self> {
			let mut inner = OpenclInstanceBase::new(info)?;

			let input_buffer = Buffer::new(
				inner.context.clone(),
				BufferMemoryFlags::KERNEL_RO,
				(width * std::mem::size_of::<f32>()) as u64
			)?;
	
			let host_memory: Vec<f32> = rand_values(rand_seed).take(width).collect();
			input_buffer.enqueue_write(
				&inner.queue,
				0,
				host_memory.as_slice(),
				None,
				true
			)?;
			std::mem::drop(host_memory);
	
			let output_buffer = Buffer::new(
				inner.context.clone(),
				BufferMemoryFlags::KERNEL_WO,
				(width * std::mem::size_of::<f32>() / super::ONE_WORK_ITEM_ACCESS_COUNT) as u64
			)?;
	
			inner.kernel.set_arg(0, &input_buffer.handle())?;
			inner.kernel.set_arg(1, &output_buffer.handle())?;
			
			Ok(
				Self {
					inner,
					input_buffer,
					output_buffer
				}
			)
		}
	}
	impl WorkInstance for OpenclInstance {
		type Fence = OpenclFence;
		type OutputElement = f32;

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {
			let mut output_buffer: Vec<f32> = Vec::new();
			output_buffer.resize(self.output_buffer.len_as::<f32>() as usize, f32::NAN);

			self.output_buffer.enqueue_read(
				&self.inner.queue,
				0,
				output_buffer.as_mut_slice(),
				None,
				true
			)?;

			Ok(output_buffer)
		}
	}
	pub fn create_opencl(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<OpenclInstance> {
		let program_source = ProgramSource::String(include_str!("../../assets/kernels/clpeak_global_bandwidth.cl").into());
		let program_entry = format!(
			"global_bandwidth_{}_{}",
			cli.vector_size.as_ident(),
			cli.offset_type.as_ident()
		);

		OpenclInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			},
			cli.width,
			cli.rand_seed
		)
	}
}