use std::{fs, path::Path, io::Write};

use thermac_experiments as common;
use common::cli::VectorSizeSelector;

common::cli! {
	@inherit(BaseCli)
	App env!("CARGO_BIN_NAME") => pub struct CliArgs {
		"represent the input as a vector of this size"
		vector_size: VectorSizeSelector = "1",

		"total width of the output buffer (in i32 elements)"
		width: usize = "16777216",
		
		"seed used in PRNG to generate input data"
		rand_seed: u64 = "0",
	}
}

common::main! {
	let cli: CliArgs;
	let item_count = [cli.width / cli.vector_size.as_num(), 1];

	match api {
		OpenCL => {
			RUN_COUNT = 10,
			create = fn create_opencl -> Result<OpenclInstance>
		}
	}

	fn dump_output;
}

fn dump_output(_cli: &CliArgs, path: &Path, buffer: Vec<u8>) -> anyhow::Result<()> {
	let mut file = fs::OpenOptions::new().write(true).create(true).open(path)?;
	let mut sum: i64 = 0;
	for value in buffer {
		writeln!(&mut file, "{}", value)?;
		sum += value as i64;
	}
	writeln!(&mut file, "sum = {}", sum)?;

	Ok(())
}

use ocl::{create_opencl, OpenclInstance};
mod ocl {
	use std::rc::Rc;

	use opencl_lib::prelude::*;
	
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				opencl::*
			},
			util::rand_values
		}
	};

	pub struct OpenclInstance {
		inner: OpenclInstanceBase,
		buffer: Buffer<Rc<Context>>
	}
	impl OpenclInstance {
		fn new(
			info: InstanceCreateInfo,
			width: usize,
			rand_seed: u64,
			vector_size: VectorSizeSelector
		) -> anyhow::Result<Self> {
			let mut inner = OpenclInstanceBase::new(info)?;

			let input_array: Vec<i32> = rand_values(rand_seed).take(vector_size.as_num()).collect();

			let buffer = Buffer::new(
				inner.context.clone(),
				BufferMemoryFlags::KERNEL_WO,
				(width * std::mem::size_of::<i32>()) as u64
			)?;

			inner.kernel.set_arg(0, &buffer.handle())?;
			inner.kernel.set_arg(1, input_array.as_slice())?;
			
			Ok(
				Self {
					inner,
					buffer
				}
			)
		}
	}
	impl WorkInstance for OpenclInstance {
		type Fence = OpenclFence;
		type OutputElement = u8;

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {
			let mut output_buffer: Vec<u8> = Vec::new();
			output_buffer.resize(self.buffer.len_as::<u8>() as usize, 0xAA);

			self.buffer.enqueue_read(
				&self.inner.queue,
				0,
				output_buffer.as_mut_slice(),
				None,
				true
			)?;

			Ok(output_buffer)
		}
	}
	pub fn create_opencl(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<OpenclInstance> {
		let program_source = ProgramSource::String(include_str!("../../assets/kernels/int_compute.cl").into());
		let program_entry = format!(
			"compute_integer_{}",
			cli.vector_size.as_ident()
		);

		OpenclInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			},
			cli.width,
			cli.rand_seed,
			cli.vector_size
		)
	}
}