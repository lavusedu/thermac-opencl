use std::path::Path;

use thermac_experiments as common;

common::cli! {
	@inherit(BaseCli)
	App env!("CARGO_BIN_NAME") => pub struct CliArgs {
	}
}

common::main! {
	let cli: CliArgs;
	let item_count = [32, 32];

	match api {
		OpenCL => {
			RUN_COUNT = 50_000,
			create = fn create_opencl -> Result<OpenclInstance>
		}
		Vulkan => {
			RUN_COUNT = 50_000,
			create = fn create_vulkan -> Result<VulkanInstance>
		}
	}

	fn dump_output;
}

fn dump_output(_cli: &CliArgs, _path: &Path, _buffer: Vec<()>) -> anyhow::Result<()> {
	Ok(())
}

use ocl::{create_opencl, OpenclInstance};
mod ocl {
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				opencl::*
			}
		}
	};

	pub struct OpenclInstance {
		inner: OpenclInstanceBase
	}
	impl OpenclInstance {
		fn new(
			info: InstanceCreateInfo
		) -> anyhow::Result<Self> {			
			let mut inner = OpenclInstanceBase::new(info)?;

			inner.kernel.set_arg(0, &std::ptr::null::<()>())?;
			
			Ok(
				Self {
					inner
				}
			)
		}
	}
	impl WorkInstance for OpenclInstance {
		type Fence = OpenclFence;
		type OutputElement = ();

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {
			Ok(Vec::new())
		}
	}
	pub fn create_opencl(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<OpenclInstance> {
		let program_source = ProgramSource::String(
			"__kernel void overhead(__global char* out) { volatile int a = get_global_id(0); return; }".into()
		);

		let program_entry = "overhead";

		OpenclInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			}
		)
	}
}

use vk::{create_vulkan, VulkanInstance};
mod vk {
	use super::{
		CliArgs,
		common::{
			prelude::*,
			api::{
				ProgramSource,
				vulkan::*
			}
		}
	};

	pub struct VulkanInstance {
		inner: VulkanInstanceBase,
	}
	impl VulkanInstance {
		fn new(
			info: InstanceCreateInfo,
		) -> anyhow::Result<Self> {
			let inner = VulkanInstanceBase::new(
				info,
				log::log_enabled!(log::Level::Debug),
				vec![],
				()
			)?;
			
			Ok(
				Self {
					inner
				}
			)
		}
	}
	impl WorkInstance for VulkanInstance {
		type Fence = VulkanFence;
		type OutputElement = ();

		fn run(&mut self) -> anyhow::Result<Self::Fence> {
			self.inner.run()
		}

		fn read_output(&mut self) -> anyhow::Result<Vec<Self::OutputElement>> {			
			Ok(Vec::new())
		}
	}

	pub fn create_vulkan(cli: &CliArgs, device_selector: &str, work_range: WorkRange) -> anyhow::Result<VulkanInstance> {
		let program_source = ProgramSource::Bytes(
			include_bytes!("../../assets/glsl/overhead.spv").as_slice().into()
		);
		let program_entry = "main";

		VulkanInstance::new(
			InstanceCreateInfo {
				device_selector,
				work_range,
				program_source,
				program_entry: program_entry.into(),
				program_cache_dir: cli.program_binary_dir.as_deref().map(std::path::Path::new)
			}
		)
	}
}
