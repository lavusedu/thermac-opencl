use thermac_experiments as common;

fn opencl_info() -> anyhow::Result<()> {
	use opencl_lib::prelude::*;

	fn device_info(device: DeviceId)  {
		match device.get_info() {
			Err(err) => log::error!("{:?}: {}", device, err),
			Ok(info) => log::info!("{:?}: {:#?}", device, info)
		}
	}

	fn platform_info(platform: PlatformId) {
		match platform.get_info() {
			Err(err) => log::error!("{:?}: {}", platform, err),
			Ok(info) => log::info!("{:?}: {:#?}", platform, info)
		}

		match DeviceId::query(platform, DeviceType::ALL) {
			Err(err) => log::error!("{}", err),
			Ok(devices) => devices.into_iter().for_each(device_info)
		}
	}

	for platform in PlatformId::query()? {
		platform_info(platform);
	}

	Ok(())
}

fn vulkan_info() -> anyhow::Result<()> {
	use std::ffi::CStr;
	use vulkayes_core::{prelude::*, self as vy, ash};
	use ash::vk;

	let instance = Instance::new(
		Entry::new()?,
		ApplicationInfo {
			application_name: "Info",
			engine_name: "Thermac experiments",
			api_version: VkVersion::new(1, 2, 0),
			..Default::default()
		},
		Some(CStr::from_bytes_with_nul(b"VK_LAYER_KHRONOS_validation\0").unwrap()),
		Some(ash::extensions::ext::DebugUtils::name()),
		Default::default(),
		vy::instance::debug::DebugCallback::Default()
	)?;

	unsafe {
		let len = instance.enumerate_physical_device_groups_len()?;
		
		let mut groups = vec![vk::PhysicalDeviceGroupProperties::default(); len];
		instance.enumerate_physical_device_groups(
			groups.as_mut_slice()
		)?;

		for (index, group) in groups.into_iter().enumerate() {
			log::info!("Group {}:", index);
			log::info!("subset_allocation = {}", group.subset_allocation != 0);
			log::info!("device_count = {}", group.physical_device_count);
			for i in 0 .. group.physical_device_count as usize {
				let physical_device = PhysicalDevice::from_existing(instance.clone(), group.physical_devices[i]);
				log::debug!(
					"Physical device: {:?}\nExtensions: {:#?}\nMemory: {:#?}\nProperties: {:#?}\nFeatures: {:#?}",
					physical_device,
					physical_device.extensions_properties()?.collect::<Vec<_>>(),
					physical_device.memory_properties(),
					physical_device.properties(),
					physical_device.features()
				);
			}
		}
	};

	Ok(())
}

fn main() -> anyhow::Result<()> {
	common::initialize_logger(log::Level::Trace);

	opencl_info()?;
	vulkan_info()?;

	Ok(())
}
