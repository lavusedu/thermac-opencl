use std::{
	io::Write,
	time::{Duration, Instant}
};

pub mod prelude;

pub mod cli;
pub mod util;
pub mod api;

pub fn initialize_logger(log_level: log::Level) {
	use std::borrow::Cow;

	use edwardium_logger::{
		targets::stderr::StderrTarget,
		Logger
	};

	const IGNORE_LIST: Cow<'static, [Cow<'static, str>]> = Cow::Borrowed(&[]);

	let logger = Logger::new(StderrTarget::new(log_level, IGNORE_LIST), Instant::now());
	logger.init_boxed().expect("could not initialize logger");
}

use api::{WorkInstance, WorkFence};

pub struct InstanceRunner<I: WorkInstance, const N: usize> {
	pub instances: [I; N],
	run_count: [usize; 2],
	run_min_time: Duration
}
impl<I: WorkInstance, const N: usize> InstanceRunner<I, N> {
	pub fn new(
		instances: [I; N],
		run_count: [usize; 2],
		run_min_time: f32
	) -> Self {
		Self {
			instances,
			run_count,
			run_min_time: Duration::from_secs_f32(run_min_time)
		}
	}
}
macro_rules! impl_run {
	(
		$instances: literal [$( $instance_index: literal ),+]
	) => {
		impl<I: WorkInstance> InstanceRunner<I, $instances> {
			pub fn run(&mut self) -> anyhow::Result<()> {
				log::info!(
					concat!("Running {} instance{} {}x{} times"),
					$instances, if $instances != 1 { "s" } else { "" },
					self.run_count[0], self.run_count[1]
				);
		
				let stdout = std::io::stdout();
				let mut stdout = stdout.lock();

				for iter_index in 0 .. self.run_count[0] {
					let time_before = Instant::now();
					writeln!(&mut stdout, "work_begin={}", iter_index + 1)?;
					stdout.flush()?;
			
					for _ in 0 .. self.run_count[1] {
						
						let fences = [
							$(
								self.instances[$instance_index].run()?
							),+
						];
						
						$(
							fences[$instance_index].wait()?;
						)+

						/*
						$(
							self.instances[$instance_index].run_without_fence()?;
						)+

						$(
							self.instances[$instance_index].wait_queue()?;
						)+
						*/
					}
			
					let elapsed = time_before.elapsed();
					writeln!(&mut stdout, "work_done={}", iter_index + 1)?;
					writeln!(&mut stdout, "work_elapsed={}", elapsed.as_secs_f64())?;
					stdout.flush()?;
			
					// sleep for the remaining time
					let remaining_idle = self.run_min_time.saturating_sub(elapsed);
					if !remaining_idle.is_zero() {
						std::thread::sleep(remaining_idle);
					}
				}

				Ok(())
			}
		}
	}
}
impl_run!(1 [0]);
impl_run!(2 [0, 1]);

#[macro_export]
macro_rules! main {
	(
		let $cli: ident: $cli_type: ty;
		let item_count = $item_count: expr;

		match api {
			$(
				$api_selector: ident => {
					RUN_COUNT = $inner_run_count: expr,
					create = fn $create_instance_fn: ident -> Result<$instance_type: ty>
					$(,)?
				}
			)+
		}

		fn $dump_output: ident;
	) => {
		use $crate::prelude::*;

		fn main() -> anyhow::Result<()> {
			let $cli = <$cli_type>::parse()?;
			common::initialize_logger($cli.log_verbosity.into());

			let work_ranges = WorkRange::new_split(
				$cli.work_share_axis,
				[$cli.work_group_size_x, $cli.work_group_size_y],
				$item_count,
				$cli.work_share_factor
			);

			if !work_ranges.0.is_empty() && !work_ranges.1.is_empty() {
				log::debug!("Work 1: {:?}", work_ranges.0);
				log::debug!("Work 2: {:?}", work_ranges.1);
				
				match $cli.api {
					$(
						ApiSelector::$api_selector => {	
							let mut runner = $crate::InstanceRunner::<$instance_type, 2>::new(
								[
									$create_instance_fn(&$cli, &$cli.device1, work_ranges.0)?,
									$create_instance_fn(&$cli, &$cli.device2, work_ranges.1)?
								],
								[$cli.run_count, $inner_run_count],
								$cli.run_min_time
							);
							runner.run()?;

							if let Some(ref path) = $cli.dump_output_path {
								let mut out1 = runner.instances[0].read_output()?;
								let out2 = runner.instances[1].read_output()?;

								common::util::merge_work_buffers(
									out1.as_mut_slice(), $item_count[0],
									out2.as_slice(), work_ranges.1
								);
								
								$dump_output(
									&$cli,
									std::path::Path::new(path),
									out1
								)?;
							}
						}
					)+
					_ => unimplemented!("Api not supported for this experiment")
				}
			} else {
				let (device, work_range) = if work_ranges.0.is_empty() {
					(&$cli.device2, work_ranges.1)
				} else {
					(&$cli.device1, work_ranges.0)
				};

				match $cli.api {
					$(
						ApiSelector::$api_selector => {
							let mut runner = $crate::InstanceRunner::<$instance_type, 1>::new(
								[
									$create_instance_fn(&$cli, device, work_range)?
								],
								[$cli.run_count, $inner_run_count],
								$cli.run_min_time
							);
							runner.run()?;

							if let Some(ref path) = $cli.dump_output_path {
								$dump_output(
									&$cli,
									std::path::Path::new(path),
									runner.instances[0].read_output()?
								)?;
							}
						}
					)+
					_ => unimplemented!("Api not supported for this experiment")
				}
			}

			Ok(())
		}
	};
}
