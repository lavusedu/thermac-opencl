__kernel void vecsum(
	__global float* a,
	__global float* b,
	__global float* c,
	const unsigned long n
) {
	int id = get_global_id(0);

	if (id >= 0 && id < n) {
		c[id] = a[id] + b[id];
	}
}
