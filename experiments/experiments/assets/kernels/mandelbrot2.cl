// This kernel is incomplete - these macro definitions are missing
// They are to be prepended to the string when the kernel is being compiled
// This is a legacy thing, the right way is to use specialization constants,
// but those are available only from OpenCL 2.2
// #define HEIGHT ({})
// #define WIDTH ({})
// #define MAX_ITER ({})
// #define ESCAPED(z) ({})

#ifndef WIDTH
	#define WIDTH 3072
#endif
#ifndef HEIGHT
	#define HEIGHT 3072
#endif
#ifndef MAX_ITER
	#define MAX_ITER 3072
#endif
#ifndef ESCAPED
	#define ESCAPED(z) (z) >= INFINITY
#endif

float2 compute_coords(
	const float2 view_min,
	const float2 view_max,
	const int2 screen_pos,
	const float2 screen_size
) {
	const float2 pixel_size = (view_max - view_min) / screen_size;
	float2 coords = view_min + (float2)(screen_pos.x, screen_pos.y) * pixel_size;

	if (fabs(coords.y) < pixel_size.y / 2) {
		coords.y = 0.0;
	}

	return coords;
}

int mandelbrot_compute(const float2 coords) {
	float Zr = 0.0;
	float Zi = 0.0;
	float Zr2 = 0.0;
	float Zi2 = 0.0;

	int iter = 0;
	for (iter = 0; iter < MAX_ITER; iter++) {
		if (ESCAPED(Zr2 + Zi2)) {
			break;
		}
		
		Zi = 2 * Zr * Zi + coords.y;
		Zr = Zr2 - Zi2 + coords.x;
		Zr2 = Zr * Zr;
		Zi2 = Zi * Zi;
	}

	return iter;
}

__kernel void mandelbrot_pretty(__global unsigned char* buffer) {
	const int2 screen_pos = (int2)(get_global_id(0), get_global_id(1));
	
	const float2 coords = compute_coords(
		(float2)(-2.0, -1.0),
		(float2)(0.5, 1.0),
		screen_pos,
		(float2)(WIDTH, HEIGHT)
	);

	const int iter = mandelbrot_compute(coords);

	#if NO_RESULT_WRITE
		volatile int result = iter;
	#else
		buffer[WIDTH * screen_pos.y + screen_pos.x] = (-iter) & 0xFF;
	#endif
}

__kernel void mandelbrot_black(__global unsigned char* buffer) {
	const int2 screen_pos = (int2)(get_global_id(0), get_global_id(1));
	
	const float2 coords = compute_coords(
		(float2)(-0.3, -0.3),
		(float2)(0.2, 0.3),
		screen_pos,
		(float2)(WIDTH, HEIGHT)
	);

	const int iter = mandelbrot_compute(coords);

	#if NO_RESULT_WRITE
		volatile int result = iter;
	#else
		buffer[WIDTH * screen_pos.y + screen_pos.x] = (-iter) & 0xFF;
	#endif
}

__kernel void mandelbrot_nocompute(__global unsigned char* buffer) {
	const int2 screen_pos = (int2)(get_global_id(0), get_global_id(1));
	
	volatile int iter = 0;
	while (iter < MAX_ITER) {
		iter += 1;
	}

	#if NO_RESULT_WRITE
		volatile int result = iter;
	#else
		buffer[WIDTH * screen_pos.y + screen_pos.x] = (-iter) & 0xFF;
	#endif
}
