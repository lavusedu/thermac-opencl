// Avoiding auto-vectorize by using vector-width locked dependent code

#undef MAD_4
#undef MAD_16
#undef MAD_64

#define MAD_4(x, y)     y = (x*y) + x;      x = (y*x) + y;      y = (x*y) + x;       x = (y*x) + y;
#define MAD_16(x, y)    MAD_4(x, y);        MAD_4(x, y);        MAD_4(x, y);        MAD_4(x, y);
#define MAD_64(x, y)    MAD_16(x, y);       MAD_16(x, y);       MAD_16(x, y);       MAD_16(x, y);

__kernel void compute_integer_v1(__global int* ptr, int A) {
    int x = A;
    int y = (int)get_local_id(0);

    for (int i = 0; i < 64; i++) {
        MAD_16(x, y);
    }

    ptr[get_global_id(0)] = y;
}

__kernel void compute_integer_v2(__global int2* ptr, int2 A) {
    int2 x = A;
    int2 y = (int2)get_local_id(0);

    for (int i = 0; i < 64; i++) {
        MAD_16(x, y);
    }

    ptr[get_global_id(0)] = y;
}

__kernel void compute_integer_v4(__global int4* ptr, int4 A) {
    int4 x = A;
    int4 y = (int4)get_local_id(0);

    for(int i = 0; i < 64; i++) {
        MAD_16(x, y);
    }

    ptr[get_global_id(0)] = y;
}


__kernel void compute_integer_v8(__global int8* ptr, int8 A) {
    int8 x = A;
    int8 y = (int8)get_local_id(0);

    for (int i = 0; i < 64; i++) {
        MAD_16(x, y);
    }

    ptr[get_global_id(0)] = y;
}

__kernel void compute_integer_v16(__global int16* ptr, int16 A) {
    int16 x = A;
    int16 y = (int16)get_local_id(0);

    for (int i = 0; i < 64; i++) {
        MAD_16(x, y);
    }

    ptr[get_global_id(0)] = y;
}
