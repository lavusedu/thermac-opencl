#undef FETCH_2
#undef FETCH_8

#define FETCH_2(sum, id, A, jumpBy)      sum += A[id];   id += jumpBy;   sum += A[id];   id += jumpBy;
#define FETCH_4(sum, id, A, jumpBy)      FETCH_2(sum, id, A, jumpBy);   FETCH_2(sum, id, A, jumpBy);
#define FETCH_8(sum, id, A, jumpBy)      FETCH_4(sum, id, A, jumpBy);   FETCH_4(sum, id, A, jumpBy);

#define FETCH_PER_WI  16

// Kernels fetching by local_size offset
__kernel void global_bandwidth_v1_local_offset(__global float* A, __global float* B) {
	int id = (get_group_id(0) * get_local_size(0) * FETCH_PER_WI) + get_local_id(0);
	float sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_local_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v2_local_offset(__global float2* A, __global float2* B) {
	int id = (get_group_id(0) * get_local_size(0) * FETCH_PER_WI) + get_local_id(0);
	float2 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_local_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v4_local_offset(__global float4* A, __global float4* B) {
	int id = (get_group_id(0) * get_local_size(0) * FETCH_PER_WI) + get_local_id(0);
	float4 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_local_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v8_local_offset(__global float8* A, __global float8* B) {
	int id = (get_group_id(0) * get_local_size(0) * FETCH_PER_WI) + get_local_id(0);
	float8 sum = 0;

	for(int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_local_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v16_local_offset(__global float16* A, __global float16* B) {
	int id = (get_group_id(0) * get_local_size(0) * FETCH_PER_WI) + get_local_id(0);
	float16 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_local_size(0));
	}

	B[get_global_id(0)] = sum;
}

// Kernels fetching by global_size offset
__kernel void global_bandwidth_v1_global_offset(__global float* A, __global float* B) {
	int id = get_global_id(0);
	float sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_global_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v2_global_offset(__global float2* A, __global float2* B) {
	int id = get_global_id(0);
	float2 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_global_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v4_global_offset(__global float4* A, __global float4* B) {
	int id = get_global_id(0);
	float4 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_global_size(0));
	}

	B[get_global_id(0)] = sum;
}


__kernel void global_bandwidth_v8_global_offset(__global float8* A, __global float8* B) {
	int id = get_global_id(0);
	float8 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_global_size(0));
	}

	B[get_global_id(0)] = sum;
}

__kernel void global_bandwidth_v16_global_offset(__global float16* A, __global float16* B) {
	int id = get_global_id(0);
	float16 sum = 0;

	for (int i = 0; i < 4; i += 1) {
		FETCH_4(sum, id, A, get_global_size(0));
	}

	B[get_global_id(0)] = sum;
}
