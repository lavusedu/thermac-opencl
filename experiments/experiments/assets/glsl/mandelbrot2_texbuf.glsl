#version 430

layout(local_size_x_id = 0, local_size_y_id = 1) in;

layout(constant_id = 2) const uint WIDTH = 3072;
layout(constant_id = 3) const uint HEIGHT = 3072;
layout(constant_id = 4) const uint MAX_ITER = 8192;
layout(constant_id = 5) const float ESCAPE_RADIUS = 1. / 0.;
layout(constant_id = 6) const bool NO_WRITE = false;
layout(constant_id = 7) const uint KERNEL = 0;

layout(set = 0, binding = 0, r8ui) uniform restrict writeonly uimageBuffer output_buffer;

vec2 compute_coords(
	const vec2 view_min,
	const vec2 view_max,
	const ivec2 screen_pos,
	const vec2 screen_size
) {
	const vec2 pixel_size = (view_max - view_min) / screen_size;
	vec2 coords = view_min + vec2(screen_pos.x, screen_pos.y) * pixel_size;

	if (abs(coords.y) < pixel_size.y / 2) {
		coords.y = 0.0;
	}

	return coords;
}

int mandelbrot_compute(const vec2 coords) {
	float Zr = 0.0;
	float Zi = 0.0;
	float Zr2 = 0.0;
	float Zi2 = 0.0;

	int iter = 0;
	for (iter = 0; iter < MAX_ITER; iter++) {
		if (Zr2 + Zi2 >= ESCAPE_RADIUS) {
			break;
		}
		
		Zi = 2 * Zr * Zi + coords.y;
		Zr = Zr2 - Zi2 + coords.x;
		Zr2 = Zr * Zr;
		Zi2 = Zi * Zi;
	}

	return iter;
}

void mandelbrot_pretty() {
	int result = 0;
	
	const ivec2 screen_pos = ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y);

	const vec2 coords = compute_coords(
		vec2(-2.0, -1.0),
		vec2(0.5, 1.0),
		screen_pos,
		vec2(WIDTH, HEIGHT)
	);

	const int iter = (-mandelbrot_compute(coords)) & 0xFF;

	if (NO_WRITE) {
		volatile int result = iter;
	} else {
		const int index = int(gl_GlobalInvocationID.y * WIDTH + gl_GlobalInvocationID.x);
		const ivec4 texel = ivec4(iter, 0, 0, 0);
		imageStore(output_buffer, index, texel);
	}
}

void mandelbrot_black() {
	int result = 0;
	
	const ivec2 screen_pos = ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y);

	const vec2 coords = compute_coords(
		vec2(-0.3, -0.3),
		vec2(0.2, 0.3),
		screen_pos,
		vec2(WIDTH, HEIGHT)
	);

	const int iter = (-mandelbrot_compute(coords)) & 0xFF;

	if (NO_WRITE) {
		volatile int result = iter;
	} else {
		const int index = int(gl_GlobalInvocationID.y * WIDTH + gl_GlobalInvocationID.x);
		const ivec4 texel = ivec4(iter, 0, 0, 0);
		imageStore(output_buffer, index, texel);
	}
}

void mandelbrot_nocompute() {
	int result = 0;
	
	const ivec2 screen_pos = ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y);

	volatile int iter = 0;
	while (iter < MAX_ITER) {
		iter += 1;
	}

	if (NO_WRITE) {
		volatile int result = iter;
	} else {
		const int index = int(gl_GlobalInvocationID.y * WIDTH + gl_GlobalInvocationID.x);
		const ivec4 texel = ivec4(iter, 0, 0, 0);
		imageStore(output_buffer, index, texel);
	}
}

void main() {
	if (KERNEL == 0) {
		mandelbrot_pretty();
	} else if (KERNEL == 1) {
		mandelbrot_black();
	} else if (KERNEL == 2) {
		mandelbrot_nocompute();
	}
}
