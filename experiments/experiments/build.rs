use std::{
	path::{Path, PathBuf},
	fs,
	process::Command
};

const GLSL_DIR: &'static str = "./assets/glsl";

fn has_glslc() -> bool {
	match Command::new("glslc").arg("--version").status() {
		Ok(exit) if exit.success() => true,
		_ => false
	}
}

fn run_glslc(path: &Path, stem: &str) -> PathBuf {	
	let mut output_path = path.to_path_buf();
	output_path.set_file_name(stem);
	output_path.set_extension("spv");
	
	let status = Command::new("glslc")
		.arg("-o").arg(&output_path)
		.arg("-fshader-stage=compute")
		.arg(path)
		.status()
		.unwrap()
	;

	if !status.success() {
		panic!("Shader compilation failed");
	}

	output_path
}

fn run_spirv_opt(path: &Path, stem: &str) -> PathBuf {
	let file_name = format!("{}_opt.spv", stem);
	
	let mut output_path = path.to_path_buf();
	output_path.set_file_name(file_name);

	let status = Command::new("spirv-opt")
		.arg("-o").arg(&output_path)
		.arg(path)
		.status()
		.unwrap()
	;

	if !status.success() {
		eprintln!("Warning: Shader optimization failed");
	}

	output_path
}

fn main_glsl() {
	for entry in fs::read_dir(GLSL_DIR).unwrap() {
		let entry = entry.unwrap();
		let path = entry.path();

		// only care about .glsl files
		if !path.extension().map(|ext| ext.eq_ignore_ascii_case("glsl")).unwrap_or(false) {
			continue
		}

		let stem = path.file_stem().unwrap().to_str().unwrap();
		
		eprintln!("Info: Processing {}", path.display());

		let spirv_path = run_glslc(&path, stem);
		run_spirv_opt(&spirv_path, stem);

		println!("cargo:rerun-if-changed={}", path.display());
	}

}

fn main() {
	if has_glslc() {
		main_glsl();
	}
}
