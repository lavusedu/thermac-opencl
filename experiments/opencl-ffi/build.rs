fn main() {
	#[cfg(not(target_os = "macos"))]
	println!("cargo:rustc-link-lib=OpenCL");
	#[cfg(target_os = "macos")]
	println!("cargo:rustc-link-lib=framework=OpenCL");

	#[cfg(feature = "run_bindgen")]
	bindgen_scope::run()
}

#[cfg(feature = "run_bindgen")]
mod bindgen_scope {
	// I am become meta
	macro_rules! make_builder_macro {
		(
			$meta_dollar_hack: tt
			$meta_macro_name: ident;
			$(
				$meta_block_name: ident => $( $meta_fn_name: ident ).+
			),+ $(,)?
		) => {
			macro_rules! $meta_macro_name {
				$(
					(
						$builder: expr;
						$meta_block_name {
							$meta_dollar_hack( $name: ident ),+ $meta_dollar_hack(,)?
						}
						$meta_dollar_hack( $much: tt )*
					) => {
						$builder = $builder
							$meta_dollar_hack(
								$(
									.$meta_fn_name(stringify!($name))
								)+
							)+
						;

						$meta_macro_name!($builder; $meta_dollar_hack($much)*);
					};
				)+

				(
					$builder: expr;
				) => {};
			}
		}
	}
	make_builder_macro!(
		$
		add_to_builder;
		typedef => allowlist_type,
		opaque => allowlist_type.opaque_type,
		var => allowlist_var,
		fun => allowlist_function
	);

	use std::{env, path::PathBuf};
	use bindgen::{Builder, callbacks::{IntKind, ParseCallbacks}};

	const INCLUDE_HEADERS: &'static [&'static str] = &[
		"OpenCL-Headers/CL/cl.h"
	];

	#[derive(Debug)]
	struct MyParseCallbacks;
	impl ParseCallbacks for MyParseCallbacks {
		fn int_macro(&self, name: &str, _value: i64) -> Option<IntKind> {
			match name {
				"CL_SUCCESS" => Some(IntKind::I32),
				_ => None
			}
		}
	}

	const fn cl_version_define() -> &'static str {
		if cfg!(feature = "opencl_3_0") {
			"-DCL_TARGET_OPENCL_VERSION=300"
		} else if cfg!(feature = "opencl_2_2") {
			"-DCL_TARGET_OPENCL_VERSION=220"
		} else if cfg!(feature = "opencl_2_1") {
			"-DCL_TARGET_OPENCL_VERSION=210"
		} else if cfg!(feature = "opencl_2_0") {
			"-DCL_TARGET_OPENCL_VERSION=200"
		} else if cfg!(feature = "opencl_1_2") {
			"-DCL_TARGET_OPENCL_VERSION=120"
		} else if cfg!(feature = "opencl_1_1") {
			"-DCL_TARGET_OPENCL_VERSION=110"
		} else {
			"-DCL_TARGET_OPENCL_VERSION=100"
		}
	}

	fn base_builder() -> Builder {
		let mut builder = Builder::default()
			.clang_arg("-IOpenCL-Headers/")
			.clang_arg(cl_version_define())
			// .dynamic_library_name("OpenCL")
			.allowlist_recursively(false)
			.layout_tests(false)
			.parse_callbacks(Box::new(MyParseCallbacks))
		;

		for &header in INCLUDE_HEADERS.iter() {
			println!("cargo:rerun-if-changed={}", header);
	
			builder = builder.header(header);
		}

		builder
	}

	fn output(builder: Builder, filename: &'static str) {
		let bindings = builder.generate().expect("Could not generate bindings");

		let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
		bindings
			.write_to_file(out_path.join(filename))
			.expect("Could not write generated bindings to a file")
		;

		let out_path = PathBuf::from("src/pregenerated");
		bindings
			.write_to_file(out_path.join(filename))
			.expect("Could not write generated bindings to a file")
		;
	}

	pub fn run() {
		opencl_1_0();
		#[cfg(feature = "opencl_1_1")]
		opencl_1_1();
		#[cfg(feature = "opencl_1_2")]
		opencl_1_2();
		#[cfg(feature = "opencl_2_0")]
		opencl_2_0();
		#[cfg(feature = "opencl_2_1")]
		opencl_2_1();
		#[cfg(feature = "opencl_2_2")]
		opencl_2_2();
		#[cfg(feature = "opencl_3_0")]
		opencl_3_0();
	}

	fn opencl_1_0() {
		let mut builder = base_builder();

		// base
		add_to_builder! {
			builder;
			typedef {
				size_t,
				cl_int,
				cl_uint,
				cl_ulong,
				cl_bool,
				cl_bitfield,
				cl_properties
			}
			var {
				CL_TRUE,
				CL_FALSE,
				CL_NONE
			}
			// error codes
			var {
				CL_SUCCESS,
				CL_BUILD_PROGRAM_FAILURE,
				CL_COMPILER_NOT_AVAILABLE,
				CL_DEVICE_NOT_FOUND,
				CL_DEVICE_NOT_AVAILABLE,
				CL_IMAGE_FORMAT_MISMATCH,
				CL_IMAGE_FORMAT_NOT_SUPPORTED,
				CL_INVALID_ARG_INDEX,
				CL_INVALID_ARG_SIZE,
				CL_INVALID_ARG_VALUE,
				CL_INVALID_BINARY,
				CL_INVALID_BUFFER_SIZE,
				CL_INVALID_BUILD_OPTIONS,
				CL_INVALID_COMMAND_QUEUE,
				CL_INVALID_CONTEXT,
				CL_INVALID_DEVICE,
				CL_INVALID_DEVICE_TYPE,
				CL_INVALID_EVENT,
				CL_INVALID_EVENT_WAIT_LIST,
				CL_INVALID_GLOBAL_OFFSET,
				CL_INVALID_GLOBAL_WORK_SIZE,
				CL_INVALID_HOST_PTR,
				CL_INVALID_IMAGE_FORMAT_DESCRIPTOR,
				CL_INVALID_IMAGE_SIZE,
				CL_INVALID_KERNEL,
				CL_INVALID_KERNEL_ARGS,
				CL_INVALID_KERNEL_DEFINITION,
				CL_INVALID_KERNEL_NAME,
				CL_INVALID_MEM_OBJECT,
				CL_INVALID_OPERATION,
				CL_INVALID_PLATFORM,
				CL_INVALID_PROGRAM,
				CL_INVALID_PROGRAM_EXECUTABLE,
				CL_INVALID_QUEUE_PROPERTIES,
				CL_INVALID_SAMPLER,
				CL_INVALID_VALUE,
				CL_INVALID_WORK_DIMENSION,
				CL_INVALID_WORK_GROUP_SIZE,
				CL_INVALID_WORK_ITEM_SIZE,
				CL_MAP_FAILURE,
				CL_MEM_COPY_OVERLAP,
				CL_MEM_OBJECT_ALLOCATION_FAILURE,
				CL_OUT_OF_HOST_MEMORY,
				CL_OUT_OF_RESOURCES,
				CL_PROFILING_INFO_NOT_AVAILABLE
			}
			// platform
			var {
				CL_PLATFORM_PROFILE,
				CL_PLATFORM_VERSION,
				CL_PLATFORM_NAME,
				CL_PLATFORM_VENDOR,
				CL_PLATFORM_EXTENSIONS
			}
			opaque {
				_cl_platform_id
			}
			typedef {
				cl_platform_id,
				cl_platform_info
			}
			fun {
				clGetPlatformIDs,
				clGetPlatformInfo
			}
			// device 
			var {
				CL_DEVICE_TYPE,
				CL_DEVICE_VENDOR_ID,
				CL_DEVICE_MAX_COMPUTE_UNITS,
				CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
				CL_DEVICE_MAX_WORK_ITEM_SIZES,
				CL_DEVICE_MAX_WORK_GROUP_SIZE,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
				CL_DEVICE_MAX_CLOCK_FREQUENCY,
				CL_DEVICE_ADDRESS_BITS,
				CL_DEVICE_MAX_MEM_ALLOC_SIZE,
				CL_DEVICE_IMAGE_SUPPORT,
				CL_DEVICE_MAX_READ_IMAGE_ARGS,
				CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
				CL_DEVICE_IMAGE2D_MAX_WIDTH,
				CL_DEVICE_IMAGE2D_MAX_HEIGHT,
				CL_DEVICE_IMAGE3D_MAX_WIDTH,
				CL_DEVICE_IMAGE3D_MAX_HEIGHT,
				CL_DEVICE_IMAGE3D_MAX_DEPTH,
				CL_DEVICE_MAX_SAMPLERS,
				CL_DEVICE_MAX_PARAMETER_SIZE,
				CL_DEVICE_MEM_BASE_ADDR_ALIGN,
				CL_DEVICE_SINGLE_FP_CONFIG,
				CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
				CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
				CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
				CL_DEVICE_GLOBAL_MEM_SIZE,
				CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
				CL_DEVICE_MAX_CONSTANT_ARGS,
				CL_DEVICE_LOCAL_MEM_TYPE,
				CL_DEVICE_LOCAL_MEM_SIZE,
				CL_DEVICE_ERROR_CORRECTION_SUPPORT,
				CL_DEVICE_PROFILING_TIMER_RESOLUTION,
				CL_DEVICE_ENDIAN_LITTLE,
				CL_DEVICE_AVAILABLE,
				CL_DEVICE_COMPILER_AVAILABLE,
				CL_DEVICE_EXECUTION_CAPABILITIES,
				CL_DEVICE_PLATFORM,
				CL_DEVICE_NAME,
				CL_DEVICE_VENDOR,
				CL_DRIVER_VERSION,
				CL_DEVICE_PROFILE,
				CL_DEVICE_VERSION,
				CL_DEVICE_EXTENSIONS,
				CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
				CL_DEVICE_QUEUE_PROPERTIES,
				CL_FP_DENORM,
				CL_FP_INF_NAN,
				CL_FP_ROUND_TO_NEAREST,
				CL_FP_ROUND_TO_ZERO,
				CL_FP_ROUND_TO_INF,
				CL_FP_FMA,
				CL_LOCAL,
				CL_GLOBAL,
				CL_EXEC_KERNEL,
				CL_EXEC_NATIVE_KERNEL,
				CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE,
				CL_QUEUE_PROFILING_ENABLE,
				CL_READ_ONLY_CACHE,
				CL_READ_WRITE_CACHE
			}
			var {
				CL_DEVICE_TYPE_CPU,
				CL_DEVICE_TYPE_GPU,
				CL_DEVICE_TYPE_ACCELERATOR,
				CL_DEVICE_TYPE_DEFAULT,
				CL_DEVICE_TYPE_ALL
			}
			opaque {
				_cl_device_id
			}
			typedef {
				cl_device_id,
				cl_device_type,
				cl_device_info,
				cl_device_fp_config,
				cl_device_exec_capabilities,
				cl_device_local_mem_type,
				cl_device_mem_cache_type
			}
			fun {
				clGetDeviceIDs,
				clGetDeviceInfo
			}
			// context
			opaque {
				_cl_context
			}
			typedef {
				cl_context,
				cl_context_properties
			}
			fun {
				clCreateContext,
				clReleaseContext
			}
			// queue
			opaque {
				_cl_command_queue
			}
			typedef {
				cl_command_queue,
				cl_command_queue_properties
			}
			fun {
				clCreateCommandQueue,
				clReleaseCommandQueue,
				clFlush,
				clFinish
			}
			// program
			opaque {
				_cl_program
			}
			var {
				CL_PROGRAM_REFERENCE_COUNT,
				CL_PROGRAM_CONTEXT,
				CL_PROGRAM_NUM_DEVICES,
				CL_PROGRAM_DEVICES,
				CL_PROGRAM_SOURCE,
				CL_PROGRAM_BINARY_SIZES,
				CL_PROGRAM_BINARIES,
				CL_PROGRAM_BUILD_STATUS,
				CL_PROGRAM_BUILD_OPTIONS,
				CL_PROGRAM_BUILD_LOG
			}
			typedef {
				cl_program,
				cl_program_info,
				cl_program_build_info
			}
			fun {
				clCreateProgramWithSource,
				clCreateProgramWithBinary,
				clBuildProgram,
				clReleaseProgram,
				clGetProgramInfo,
				clGetProgramBuildInfo
			}
			// kernel
			opaque {
				_cl_kernel
			}
			typedef {
				cl_kernel
			}
			fun {
				clCreateKernel,
				clReleaseKernel,
				clSetKernelArg,
				clEnqueueNDRangeKernel
			}
			// buffer
			opaque {
				_cl_mem
			}
			var {
				CL_MEM_READ_WRITE,
				CL_MEM_WRITE_ONLY,
				CL_MEM_READ_ONLY
			}
			typedef {
				cl_mem,
				cl_mem_flags
			}
			fun {
				clCreateBuffer,
				clReleaseMemObject,
				clEnqueueReadBuffer,
				clEnqueueWriteBuffer
			}
			// event
			opaque {
				_cl_event
			}
			var {
				CL_EVENT_COMMAND_QUEUE,
				CL_EVENT_COMMAND_TYPE,
				CL_EVENT_COMMAND_EXECUTION_STATUS,
				CL_QUEUED,
				CL_SUBMITTED,
				CL_RUNNING,
				CL_COMPLETE,
				CL_EVENT_REFERENCE_COUNT,
				CL_COMMAND_NDRANGE_KERNEL,
				CL_COMMAND_TASK,
				CL_COMMAND_NATIVE_KERNEL,
				CL_COMMAND_READ_BUFFER,
				CL_COMMAND_WRITE_BUFFER,
				CL_COMMAND_COPY_BUFFER,
				CL_COMMAND_READ_IMAGE,
				CL_COMMAND_WRITE_IMAGE,
				CL_COMMAND_COPY_IMAGE,
				CL_COMMAND_COPY_BUFFER_TO_IMAGE,
				CL_COMMAND_COPY_IMAGE_TO_BUFFER,
				CL_COMMAND_MAP_BUFFER,
				CL_COMMAND_MAP_IMAGE,
				CL_COMMAND_UNMAP_MEM_OBJECT,
				CL_COMMAND_MARKER
			}
			typedef {
				cl_event,
				cl_event_info
			}
			fun {
				clWaitForEvents,
				clGetEventInfo,
				clRetainEvent,
				clReleaseEvent
			}
			// barrier
			fun {
				clEnqueueMarker
			}
		};

		output(builder, "bindings_1_0.rs");
	}

	#[cfg(feature = "opencl_1_1")]
	fn opencl_1_1() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_1_0::*;");

		add_to_builder!(
			builder;
			// error codes
			var {
				CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST,
				CL_INVALID_PROPERTY,
				CL_MISALIGNED_SUB_BUFFER_OFFSET
			}
			// device
			var {
				CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
				CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
				CL_DEVICE_OPENCL_C_VERSION,
				CL_DEVICE_HOST_UNIFIED_MEMORY,
				CL_FP_SOFT_FLOAT
			}
			// event
			var {
				CL_EVENT_CONTEXT,
				CL_COMMAND_READ_BUFFER_RECT,
				CL_COMMAND_WRITE_BUFFER_RECT,
				CL_COMMAND_COPY_BUFFER_RECT,
				CL_COMMAND_USER
			}
			fun {
				clCreateUserEvent,
				clSetUserEventStatus,
				clSetEventCallback
			}
		);

		output(builder, "bindings_1_1.rs");
	}

	#[cfg(feature = "opencl_1_2")]
	fn opencl_1_2() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_1_1::*;");

		add_to_builder!(
			builder;
			// common
			var {
				CL_BLOCKING,
				CL_NON_BLOCKING
			}
			// error codes
			var {
				CL_COMPILE_PROGRAM_FAILURE,
				CL_DEVICE_PARTITION_FAILED,
				CL_INVALID_COMPILER_OPTIONS,
				CL_INVALID_DEVICE_PARTITION_COUNT,
				CL_INVALID_IMAGE_DESCRIPTOR,
				CL_INVALID_LINKER_OPTIONS,
				CL_KERNEL_ARG_INFO_NOT_AVAILABLE,
				CL_LINK_PROGRAM_FAILURE,
				CL_LINKER_NOT_AVAILABLE
			}
			// device
			var {
				CL_DEVICE_IMAGE_MAX_BUFFER_SIZE,
				CL_DEVICE_IMAGE_MAX_ARRAY_SIZE,
				CL_DEVICE_DOUBLE_FP_CONFIG,
				CL_DEVICE_LINKER_AVAILABLE,
				CL_DEVICE_PRINTF_BUFFER_SIZE,
				CL_DEVICE_PREFERRED_INTEROP_USER_SYNC,
				CL_DEVICE_PARENT_DEVICE,
				CL_DEVICE_PARTITION_MAX_SUB_DEVICES,
				CL_DEVICE_PARTITION_PROPERTIES,
				CL_DEVICE_PARTITION_AFFINITY_DOMAIN,
				CL_DEVICE_PARTITION_TYPE,
				CL_DEVICE_REFERENCE_COUNT,
				CL_DEVICE_BUILT_IN_KERNELS,
				CL_FP_CORRECTLY_ROUNDED_DIVIDE_SQRT,
				CL_DEVICE_PARTITION_EQUALLY,
				CL_DEVICE_PARTITION_BY_COUNTS,
				CL_DEVICE_PARTITION_BY_AFFINITY_DOMAIN,
				CL_DEVICE_AFFINITY_DOMAIN_NUMA,
				CL_DEVICE_AFFINITY_DOMAIN_L4_CACHE,
				CL_DEVICE_AFFINITY_DOMAIN_L3_CACHE,
				CL_DEVICE_AFFINITY_DOMAIN_L2_CACHE,
				CL_DEVICE_AFFINITY_DOMAIN_L1_CACHE,
				CL_DEVICE_AFFINITY_DOMAIN_NEXT_PARTITIONABLE
			}
			var {
				CL_DEVICE_TYPE_CUSTOM
			}
			typedef {
				cl_device_partition_property,
				cl_device_affinity_domain
			}
			// program
			var {
				CL_PROGRAM_BINARY_TYPE
			}
			fun {
				clCreateProgramWithBuiltInKernels
			}
			// buffer
			var {
				CL_MEM_HOST_WRITE_ONLY,
				CL_MEM_HOST_READ_ONLY,
				CL_MEM_HOST_NO_ACCESS
			}
			// event
			var {
				CL_COMMAND_BARRIER,
				CL_COMMAND_MIGRATE_MEM_OBJECTS,
				CL_COMMAND_FILL_BUFFER,
				CL_COMMAND_FILL_IMAGE
			}
			// barrier
			fun {
				clEnqueueMarkerWithWaitList,
				clEnqueueWaitForEvents,
				clEnqueueBarrierWithWaitList,
				clEnqueueBarrier
			}
		);

		output(builder, "bindings_1_2.rs");
	}

	#[cfg(feature = "opencl_2_0")]
	fn opencl_2_0() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_1_2::*;");

		add_to_builder!(
			builder;
			// error codes
			var {
				CL_INVALID_DEVICE_QUEUE,
				CL_INVALID_PIPE_SIZE
			}
			// device
			var {
				CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS,
				CL_DEVICE_IMAGE_PITCH_ALIGNMENT,
				CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT,
				CL_DEVICE_MAX_PIPE_ARGS,
				CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS,
				CL_DEVICE_PIPE_MAX_PACKET_SIZE,
				CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE,
				CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE,
				CL_DEVICE_QUEUE_ON_HOST_PROPERTIES,
				CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES,
				CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE,
				CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE,
				CL_DEVICE_MAX_ON_DEVICE_QUEUES,
				CL_DEVICE_MAX_ON_DEVICE_EVENTS,
				CL_DEVICE_SVM_CAPABILITIES,
				CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT,
				CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT,
				CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT,
				CL_QUEUE_ON_DEVICE,
				CL_QUEUE_ON_DEVICE_DEFAULT,
				CL_DEVICE_SVM_COARSE_GRAIN_BUFFER,
				CL_DEVICE_SVM_FINE_GRAIN_BUFFER,
				CL_DEVICE_SVM_FINE_GRAIN_SYSTEM,
				CL_DEVICE_SVM_ATOMICS
			}
			typedef {
				cl_command_queue_properties,
				cl_device_svm_capabilities
			}
			// queue
			typedef {
				cl_queue_properties
			}
			fun {
				clCreateCommandQueueWithProperties
			}
			// program
			var {
				CL_PROGRAM_BUILD_GLOBAL_VARIABLE_TOTAL_SIZE
			}
			// event
			var {
				CL_COMMAND_SVM_FREE,
				CL_COMMAND_SVM_MEMCPY,
				CL_COMMAND_SVM_MEMFILL,
				CL_COMMAND_SVM_MAP,
				CL_COMMAND_SVM_UNMAP
			}
		);

		output(builder, "bindings_2_0.rs");
	}

	#[cfg(feature = "opencl_2_1")]
	fn opencl_2_1() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_2_0::*;");

		add_to_builder!(
			builder;
			// platform
			var {
				CL_PLATFORM_HOST_TIMER_RESOLUTION
			}
			// device
			var {
				CL_DEVICE_MAX_NUM_SUB_GROUPS,
				CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS,
				CL_DEVICE_IL_VERSION
			}
			// program
			fun {
				clCreateProgramWithIL
			}
		);

		output(builder, "bindings_2_1.rs");
	}

	#[cfg(feature = "opencl_2_2")]
	fn opencl_2_2() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_2_1::*;");
		
		add_to_builder!(
			builder;
			// error codes
			var {
				CL_INVALID_SPEC_ID,
				CL_MAX_SIZE_RESTRICTION_EXCEEDED
			}
		);

		output(builder, "bindings_2_2.rs");
	}

	#[cfg(feature = "opencl_3_0")]
	fn opencl_3_0() {
		let mut builder = base_builder().raw_line("pub(crate) use crate::cl_2_2::*;");

		// platform
		add_to_builder!(
			builder;
			// common
			var {
				CL_VERSION_MAJOR_BITS,
				CL_VERSION_MINOR_BITS,
				CL_VERSION_PATCH_BITS
			}
			// platform
			var {
				CL_PLATFORM_NUMERIC_VERSION,
				CL_PLATFORM_EXTENSIONS_WITH_VERSION
			}
			typedef {
				cl_version,
				_cl_name_version,
				cl_name_version
			}
			// device
			var {
				CL_DEVICE_ILS_WITH_VERSION,
				CL_DEVICE_BUILT_IN_KERNELS_WITH_VERSION,
				CL_DEVICE_NUMERIC_VERSION,
				CL_DEVICE_OPENCL_C_ALL_VERSIONS,
				CL_DEVICE_OPENCL_C_FEATURES,
				CL_DEVICE_EXTENSIONS_WITH_VERSION,
				CL_DEVICE_ATOMIC_MEMORY_CAPABILITIES,
				CL_DEVICE_ATOMIC_FENCE_CAPABILITIES,
				CL_DEVICE_NON_UNIFORM_WORK_GROUP_SUPPORT,
				CL_DEVICE_WORK_GROUP_COLLECTIVE_FUNCTIONS_SUPPORT,
				CL_DEVICE_GENERIC_ADDRESS_SPACE_SUPPORT,
				CL_DEVICE_DEVICE_ENQUEUE_CAPABILITIES,
				CL_DEVICE_PIPE_SUPPORT,
				CL_DEVICE_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
				CL_DEVICE_LATEST_CONFORMANCE_VERSION_PASSED,
				CL_DEVICE_ATOMIC_ORDER_RELAXED,
				CL_DEVICE_ATOMIC_ORDER_ACQ_REL,
				CL_DEVICE_ATOMIC_ORDER_SEQ_CST,
				CL_DEVICE_ATOMIC_SCOPE_WORK_ITEM,
				CL_DEVICE_ATOMIC_SCOPE_WORK_GROUP,
				CL_DEVICE_ATOMIC_SCOPE_DEVICE,
				CL_DEVICE_ATOMIC_SCOPE_ALL_DEVICES,
				CL_DEVICE_QUEUE_SUPPORTED,
				CL_DEVICE_QUEUE_REPLACEABLE_DEFAULT
			}
			typedef {
				cl_device_atomic_capabilities,
				cl_device_device_enqueue_capabilities
			}
			// event
			var {
				CL_COMMAND_SVM_MIGRATE_MEM
			}
		);

		output(builder, "bindings_3_0.rs");
	}
}
