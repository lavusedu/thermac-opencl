#[path = "bindings_1_0.rs"]
pub mod cl_1_0;

#[cfg(feature = "opencl_1_1")]
#[path = "bindings_1_1.rs"]
pub mod cl_1_1;

#[cfg(feature = "opencl_1_2")]
#[path = "bindings_1_2.rs"]
pub mod cl_1_2;

#[cfg(feature = "opencl_2_0")]
#[path = "bindings_2_0.rs"]
pub mod cl_2_0;

#[cfg(feature = "opencl_2_1")]
#[path = "bindings_2_1.rs"]
pub mod cl_2_1;

#[cfg(feature = "opencl_2_2")]
#[path = "bindings_2_2.rs"]
pub mod cl_2_2;

#[cfg(feature = "opencl_3_0")]
#[path = "bindings_3_0.rs"]
pub mod cl_3_0;
