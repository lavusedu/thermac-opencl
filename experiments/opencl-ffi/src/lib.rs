#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_imports)]

pub mod prelude;

#[cfg(feature = "run_bindgen")]
mod bindings {
	pub mod cl_1_0 {
		include!(concat!(env!("OUT_DIR"), "/bindings_1_0.rs"));
	}

	#[cfg(feature = "opencl_1_1")]
	pub mod cl_1_1 {
		include!(concat!(env!("OUT_DIR"), "/bindings_1_1.rs"));
	}

	#[cfg(feature = "opencl_1_2")]
	pub mod cl_1_2 {
		include!(concat!(env!("OUT_DIR"), "/bindings_1_2.rs"));
	}

	#[cfg(feature = "opencl_2_0")]
	pub mod cl_2_0 {
		include!(concat!(env!("OUT_DIR"), "/bindings_2_0.rs"));
	}

	#[cfg(feature = "opencl_2_1")]
	pub mod cl_2_1 {
		include!(concat!(env!("OUT_DIR"), "/bindings_2_1.rs"));
	}

	#[cfg(feature = "opencl_2_2")]
	pub mod cl_2_2 {
		include!(concat!(env!("OUT_DIR"), "/bindings_2_2.rs"));
	}

	#[cfg(feature = "opencl_3_0")]
	pub mod cl_3_0 {
		include!(concat!(env!("OUT_DIR"), "/bindings_3_0.rs"));
	}
}

#[cfg(not(feature = "run_bindgen"))]
#[path = "pregenerated/mod.rs"]
mod bindings;

pub use bindings::*;
