use opencl_ffi::cl_1_0::{cl_uint, cl_event, clReleaseEvent, clWaitForEvents};

use crate::prelude::{ClObject, Context, SharedAccess};

cl_error! {
	"release event"
	pub enum EventDropError {
		InvalidEvent = CL_INVALID_EVENT,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"wait for events"
	pub enum EventWaitError {
		InvalidValue = CL_INVALID_VALUE,
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidEvent = CL_INVALID_EVENT,
		#[cfg(feature = "opencl_1_1")]
		ExecStatusErrorForEventsInWaitList = CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

pub struct Event<C: SharedAccess<Context>> {
	context: C,
	event: cl_event
}
impl<C: SharedAccess<Context>> Event<C> {
	/// ### Safety
	/// * `event` must be a valid event belonging to `context`
	pub unsafe fn from_raw(context: C, event: cl_event) -> Self {
		Event {
			context,
			event
		}
	}

	pub fn wait(&self) -> Result<(), EventWaitError> {
		Event::wait_for_events([self])
	}
	
	pub fn context(&self) -> &C {
		&self.context
	}

	pub fn resolve_wait_list<'e>(wait_list: impl IntoIterator<Item = &'e Event<C>>) -> (cl_uint, Option<Vec<cl_event>>) where C: 'e {
		let handles = wait_list.into_iter().map(|event| event.handle()).collect::<Vec<_>>();

		if handles.len() == 0 {
			(0, None)
		} else {
			(handles.len() as cl_uint, Some(handles))
		}
	}

	pub fn wait_for_events<const N: usize>(events: [&Event<C>; N]) -> Result<(), EventWaitError> {
		let handles = events.map(|e| e.handle());

		unsafe {
			cl_try!(
				EventWaitError: clWaitForEvents(
					N as cl_uint,
					handles.as_ptr()
				)
			)?;
		}

		Ok(())
	}
}
impl<C: SharedAccess<Context>> ClObject for Event<C> {
    type HandleType = cl_event;

    fn handle(&self) -> Self::HandleType {
        self.event
    }
}
impl_fallible_drop! {
	impl Event<C> where [C: SharedAccess<Context>] {
		pub fn release(self) -> Result<(), EventDropError> {
			unsafe {
				cl_try!(
					EventDropError: clReleaseEvent(self.event)
				)?;
			}
			
			Ok(())
		}
	}
}