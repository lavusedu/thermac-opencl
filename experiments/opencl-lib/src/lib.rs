#![allow(unused_unsafe)]

pub use opencl_ffi;

pub mod prelude;

#[macro_use]
pub mod macros;
pub mod common;

pub mod platform;
pub mod device;
pub mod context;
pub mod queue;
pub mod program;
pub mod kernel;
pub mod buffer;
pub mod event;