use opencl_ffi::cl_1_0::{size_t, CL_TRUE, CL_FALSE, cl_mem, clCreateBuffer, clReleaseMemObject, clEnqueueReadBuffer, clEnqueueWriteBuffer};

use crate::prelude::{Bitfield, ClObject, CommandQueue, SharedAccess, Event};

use super::context::Context;

cl_error! {
	"create buffer"
	pub enum BufferInitError {
		InvalidContext = CL_INVALID_CONTEXT,
		#[cfg(feature = "opencl_1_1")]
		InvalidProperty = CL_INVALID_PROPERTY,
		InvalidValue = CL_INVALID_VALUE,
		InvalidBufferSize = CL_INVALID_BUFFER_SIZE,
		InvalidHostPtr = CL_INVALID_HOST_PTR,
		MemObjectAllocationFailure = CL_MEM_OBJECT_ALLOCATION_FAILURE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"release buffer"
	pub enum BufferDropError {
		InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_mem_flags) BufferMemoryFlags {
		KERNEL_RW = CL_MEM_READ_WRITE,
		KERNEL_WO = CL_MEM_WRITE_ONLY,
		KERNEL_RO = CL_MEM_READ_ONLY,
		#[cfg(feature = "opencl_1_2")]
		HOST_WO = CL_MEM_HOST_WRITE_ONLY,
		#[cfg(feature = "opencl_1_2")]
		HOST_RO = CL_MEM_HOST_READ_ONLY,
		#[cfg(feature = "opencl_1_2")]
		HOST_NO_ACCESS = CL_MEM_HOST_NO_ACCESS
	}
}

cl_error! {
	"enqueue buffer read/write operation"
	pub enum BufferEnqeueuReadWriteError {
		InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE,
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidMemObject = CL_INVALID_MEM_OBJECT,
		InvalidValue = CL_INVALID_VALUE,
		InvalidEventWaitList = CL_INVALID_EVENT_WAIT_LIST,
		#[cfg(feature = "opencl_1_1")]
		MisalignedSubBufferOffset = CL_MISALIGNED_SUB_BUFFER_OFFSET,
		#[cfg(feature = "opencl_1_1")]
		ExecStatusErrorForEventsInWaitList = CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST,
		AllocationFailure = CL_MEM_OBJECT_ALLOCATION_FAILURE,
		InvalidOperation = CL_INVALID_OPERATION,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

#[derive(Debug)]
pub struct Buffer<C: SharedAccess<Context>> {
	context: C,
	buffer: cl_mem,
	size: size_t
}
impl<C: SharedAccess<Context>> Buffer<C> {
	pub fn new(
		context: C,
		memory_flags: BufferMemoryFlags,
		size: size_t
	) -> Result<Self, BufferInitError> {
		let buffer = unsafe {
			cl_try!(
				noret BufferInitError: clCreateBuffer(
					context.handle(),
					memory_flags.into_raw(),
					size,
					std::ptr::null_mut()
				)
			)?
		};

		Ok(
			Buffer {
				context,
				buffer,
				size
			}
		)
	}

	pub fn len(&self) -> size_t {
		self.size
	}

	pub fn len_as<T: Sized>(&self) -> size_t {
		self.len() / (std::mem::size_of::<T>() as size_t)
	}

	pub fn context(&self) -> &C {
		&self.context
	}
}
impl<'e, C: SharedAccess<Context> + 'e> Buffer<C> {
	impl_enqueue_with_event! {
		pub fn {
			name = enqueue_read,
			name_with_event = enqueue_read_with_event,
			blocking = blocking
		} [D: Copy] (
			..,
			offset: size_t,
			data: &mut [D]
		) -> Result<.., BufferEnqeueuReadWriteError> {
			clEnqueueReadBuffer(
				..,
				offset,
				std::mem::size_of_val(data) as size_t,
				unsafe {
					std::ptr::slice_from_raw_parts_mut(
						data as *mut _ as *mut u8,
						std::mem::size_of_val(data)
					) as *mut _
				}
			)
		}
	}

	impl_enqueue_with_event! {
		pub fn {
			name = enqueue_write,
			name_with_event = enqueue_write_with_event,
			blocking = blocking
		} [D: Copy] (
			..,
			offset: size_t,
			data: &[D]
		) -> Result<.., BufferEnqeueuReadWriteError> {
			clEnqueueWriteBuffer(
				..,
				offset,
				std::mem::size_of_val(data) as size_t,
				unsafe {
					std::ptr::slice_from_raw_parts(
						data as *const _ as *const u8,
						std::mem::size_of_val(data)
					) as *const _
				}
			)
		}
	}
}
impl<C: SharedAccess<Context>> ClObject for Buffer<C> {
    type HandleType = cl_mem;

    fn handle(&self) -> Self::HandleType {
        self.buffer
    }
}
impl_fallible_drop! {
	impl Buffer<C> where [C: SharedAccess<Context>] {
		pub fn release(self) -> Result<(), BufferDropError> {
			unsafe {
				cl_try!(
					BufferDropError: clReleaseMemObject(self.buffer)
				)?;
			}
			
			Ok(())
		}
	}
}
