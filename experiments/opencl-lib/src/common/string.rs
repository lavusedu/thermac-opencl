use std::ffi::{CString, IntoStringError};

pub fn char_array_to_string(mut bytes: Vec<u8>) -> Result<String, IntoStringError> {
	assert_eq!(bytes[bytes.len() - 1], 0);
	bytes.pop();

	CString::new(bytes).unwrap().into_string()
}
