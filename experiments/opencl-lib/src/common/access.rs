use std::ops::{Deref, DerefMut};

#[derive(Debug)]
pub struct OwnedAccess<T>(pub T);
impl<T> Deref for OwnedAccess<T> {
	type Target = T;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl<T> DerefMut for OwnedAccess<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

/// Marker trait for wrappers which give unique access to a specific object.
///
/// This can include mutable references and owned values.
pub trait UniqueAccess<T>: DerefMut<Target = T> + std::fmt::Debug {}

/// Marker trait for wrappers which give shared access to a specific object.
///
/// This can include shared references, `Rc`s, `Arc`s and such.
///
/// ### Safety
/// Cloning of the implementor must share the access to the value.
pub unsafe trait SharedAccess<T>: Deref<Target = T> + Clone + std::fmt::Debug {}

impl<T: std::fmt::Debug> UniqueAccess<T> for &'_ mut T {}
impl<T: std::fmt::Debug> UniqueAccess<T> for OwnedAccess<T> {}
impl<T: std::fmt::Debug> UniqueAccess<T> for Box<T> {}

unsafe impl<T: std::fmt::Debug> SharedAccess<T> for &'_ T {}
unsafe impl<T: std::fmt::Debug> SharedAccess<T> for std::rc::Rc<T> {}
unsafe impl<T: std::fmt::Debug> SharedAccess<T> for std::sync::Arc<T> {}
