pub mod access;
pub mod string;
pub mod bitfield;
pub mod object;

#[cfg(feature = "opencl_3_0")]
pub mod version;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum OpenclFeatureVersion {
	V10,
	#[cfg(feature = "opencl_1_1")]
	V11,
	#[cfg(feature = "opencl_1_2")]
	V12,
	#[cfg(feature = "opencl_2_0")]
	V20,
	#[cfg(feature = "opencl_2_1")]
	V21,
	#[cfg(feature = "opencl_2_2")]
	V22,
	#[cfg(feature = "opencl_3_0")]
	V30
}
impl Default for OpenclFeatureVersion {
	fn default() -> Self {
		OpenclFeatureVersion::V10
	}
}