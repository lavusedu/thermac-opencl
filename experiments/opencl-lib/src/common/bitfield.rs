use std::ops::{BitAnd, BitOr, BitXor, Not};

pub trait Bitfield: Copy + Eq + BitOr + BitAnd + BitXor + Not {
	/// Underlying type.
	type BitType: Copy + Eq + BitOr + BitAnd + BitXor + Not + Into<opencl_ffi::cl_1_0::cl_bitfield>;

	/// Constant containing no flags.
	const NONE: Self;
	/// Constant containing all possible flags.
	const ALL: Self;

	/// Attempts to create an instance from raw representation.
	///
	/// If the raw representation contains bits which are not know returns `Err(<the remaining bits>)`.
	fn from_raw(raw: Self::BitType) -> Result<Self, Self::BitType>;

	/// Creates an instance from raw representation.
	///
	/// ### Safety
	/// * Raw must not contain bits which are not in `Self::ALL`.
	unsafe fn from_raw_unchecked(raw: Self::BitType) -> Self;

	/// Returns the raw representation.
	fn into_raw(self) -> Self::BitType;
}
