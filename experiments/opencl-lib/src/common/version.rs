pub struct UnpackedVersion(u16, u16, u16);
impl UnpackedVersion {
	pub fn unpack(version: opencl_ffi::cl_3_0::cl_version) -> Self {
		use opencl_ffi::cl_3_0::{
			CL_VERSION_MAJOR_BITS,
			CL_VERSION_MINOR_BITS,
			CL_VERSION_PATCH_BITS
		};
	
		let major = (version >> (CL_VERSION_MINOR_BITS + CL_VERSION_PATCH_BITS)) & (1 << CL_VERSION_MAJOR_BITS - 1);
		let minor = (version >> CL_VERSION_PATCH_BITS) & (1 << CL_VERSION_MINOR_BITS - 1);
		let patch = version & (1 << CL_VERSION_PATCH_BITS - 1);
	
		UnpackedVersion(major as u16, minor as u16, patch as u16)
	}
}
impl std::fmt::Debug for UnpackedVersion {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}.{}.{}", self.0, self.1, self.2)
	}
}

#[repr(transparent)]
pub struct NameVersionPacked(opencl_ffi::cl_3_0::cl_name_version);
impl NameVersionPacked {
	pub const fn empty() -> Self {
		NameVersionPacked(
			opencl_ffi::cl_3_0::cl_name_version {
				version: 0,
				name: [0; 64]
			}
		)
	}

	pub fn unpack(self) -> Result<NameVersion, std::ffi::IntoStringError> {
		let version = UnpackedVersion::unpack(self.0.version);

		let name = std::ffi::CString::from(
			unsafe {
				std::ffi::CStr::from_ptr(self.0.name.as_ptr())
			}
		).into_string()?;

		Ok(
			NameVersion {
				name,
				version
			}
		)
	}
}
impl Default for NameVersionPacked {
	fn default() -> Self {
		Self::empty()
	}
}
impl Clone for NameVersionPacked {
	fn clone(&self) -> Self {
		NameVersionPacked(opencl_ffi::cl_3_0::cl_name_version { version: self.0.version, name: self.0.name })
	}
}


#[derive(Debug)]
pub struct NameVersion {
	pub name: String,
	pub version: UnpackedVersion
}