pub trait ClObject {
	type HandleType;

	fn handle(&self) -> Self::HandleType;
}