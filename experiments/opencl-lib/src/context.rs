use opencl_ffi::cl_1_0::{clCreateContext, clReleaseContext, cl_context};

use crate::prelude::ClObject;

use super::device::DeviceId;

cl_error! {
	"create context"
	pub enum ContextInitError {
		InvalidPlatform = CL_INVALID_PLATFORM,
		#[cfg(feature = "opencl_1_1")]
		InvalidProperty = CL_INVALID_PROPERTY,
		InvalidValue = CL_INVALID_VALUE,
		InvalidDevice = CL_INVALID_DEVICE,
		DeviceNotAvailable = CL_DEVICE_NOT_AVAILABLE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"release context"
	pub enum ContextDropError {
		InvalidContext = CL_INVALID_CONTEXT,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

#[derive(Debug)]
pub struct Context {
	devices: Vec<DeviceId>,
	context: cl_context
}
impl Context {
	pub fn new(
		devices: Vec<DeviceId>
	) -> Result<Self, ContextInitError> {
		let device_handles: Vec<_> = devices.iter().map(
			|d| d.handle()
		).collect();

		let context = unsafe {
			cl_try!(
				noret ContextInitError: clCreateContext(
					std::ptr::null(),
					device_handles.len() as _,
					device_handles.as_ptr(),
					None,
					std::ptr::null_mut(),
				)
			)?
		};

		Ok(
			Context {
				devices,
				context
			}
		)
	}

	pub fn devices(&self) -> impl Iterator<Item = DeviceId> + '_ {
		self.devices.iter().copied()
	}
}
impl ClObject for Context {
	type HandleType = cl_context;
	
	fn handle(&self) -> Self::HandleType {
		self.context
	}
}
impl_fallible_drop! {
	impl Context {
		pub fn release(self) -> Result<(), ContextDropError> {
			unsafe {
				cl_try!(
					ContextDropError: clReleaseContext(self.context)
				)?;
			}

			Ok(())
		}
	}
}
