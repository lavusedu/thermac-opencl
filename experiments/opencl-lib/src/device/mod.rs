use opencl_ffi::cl_1_0::{cl_device_id, clGetDeviceIDs};

use crate::prelude::{
	Bitfield,
	PlatformId,
	ClObject
};

mod info;
pub use info::*;

cl_error! {
	"query device ids"
	pub enum DeviceIdQueryError {
		InvalidPlatform = CL_INVALID_PLATFORM,
		InvalidDeviceType = CL_INVALID_DEVICE_TYPE,
		InvalidValue = CL_INVALID_VALUE,
		DeviceNotFound = CL_DEVICE_NOT_FOUND,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

#[derive(Debug, Copy, Clone)]
pub struct DeviceId {
	platform: PlatformId,
	device_id: cl_device_id
}
impl DeviceId {
	pub fn query(
		platform: PlatformId,
		device_type: DeviceType
	) -> Result<Vec<DeviceId>, DeviceIdQueryError> {
		let len = unsafe {
			let mut len = 0;
			cl_try!(
				DeviceIdQueryError: clGetDeviceIDs(
					platform.handle(),
					device_type.into_raw(),
					0,
					std::ptr::null_mut(),
					&mut len
				)
			)?;

			len
		};

		let mut devices: Vec<cl_device_id> = vec![std::ptr::null_mut(); len as usize];
		unsafe {
			cl_try!(
				DeviceIdQueryError: clGetDeviceIDs(
					platform.handle(),
					device_type.into_raw(),
					len,
					devices.as_mut_ptr(),
					std::ptr::null_mut()
				)
			)?;
		}
		
		let devices = devices.into_iter().map(
			|id| unsafe { DeviceId::new(platform, id) }
		).collect::<Vec<DeviceId>>();

		Ok(
			devices
		)
	}

	/// ### Safety
	/// `device_id` must be a valid device id belonging to `platform`
	pub unsafe fn new(platform: PlatformId, device_id: cl_device_id) -> Self {
		DeviceId {
			platform,
			device_id
		}
	}

	pub fn get_basic_info(&self) -> Result<DeviceBasicInfo, GetDeviceInfoError> {
		DeviceBasicInfo::get(*self)
	}

	pub fn get_info(&self) -> Result<DeviceInfo, GetDeviceInfoError> {
		DeviceInfo::get(*self)
	}
}
impl ClObject for DeviceId {
	type HandleType = cl_device_id;
	
	fn handle(&self) -> Self::HandleType {
		self.device_id
	}
}
impl std::fmt::Display for DeviceId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		writeln!(f, "DeviceId({:?}, {:?}):", self.platform, self.device_id)?;

		match self.get_basic_info() {
			Ok(info) => {
				writeln!(f, "info = {:#?}", info)?;
			}
			Err(err) => {
				writeln!(f, "info_error = {}", err)?;
			}
		}

		Ok(())
	}
}