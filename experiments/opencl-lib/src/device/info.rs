use std::{any::Any, convert::TryFrom, ffi::IntoStringError};

use thiserror::Error;

use opencl_ffi::cl_1_0::{CL_TRUE, clGetDeviceInfo, cl_bool, cl_device_fp_config, cl_device_type, cl_platform_id, cl_uint, cl_ulong, size_t, cl_device_mem_cache_type, cl_device_local_mem_type, cl_bitfield, CL_DEVICE_PLATFORM};

use crate::{
	common::string::char_array_to_string,
	prelude::{Bitfield, PlatformId, ClObject}
};

use super::DeviceId;

cl_error! {
	"get device info"
	pub enum DeviceInfoError {
		InvalidDevice = CL_INVALID_DEVICE,
		InvalidValue = CL_INVALID_VALUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}
#[derive(Debug, Error)]
pub enum GetDeviceInfoError {
	#[error("Failed to convert C string to utf8: {0}")]
	IntoStringError(#[from] IntoStringError),
	#[error("Received invalid bitfield \"{bitfield_name}\" value: {actual:0b} (allowed: {allowed:0b})")]
	InvalidBitfieldValue {
		actual: cl_bitfield,
		allowed: cl_bitfield,
		bitfield_name: &'static str
	},
	#[error("Received invalid enum \"{enum_name}\" value: {actual}")]
	InvalidEnumValue {
		actual: cl_uint,
		enum_name: &'static str
	},
	#[error(transparent)]
	DeviceInfoError(#[from] DeviceInfoError)
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_device_type) DeviceType {
		ALL_TYPES = CL_DEVICE_TYPE_ALL,
		DEFAULT = CL_DEVICE_TYPE_DEFAULT,
		CPU = CL_DEVICE_TYPE_CPU,
		GPU = CL_DEVICE_TYPE_GPU,
		ACCELERATOR = CL_DEVICE_TYPE_ACCELERATOR,
		#[cfg(feature = "opencl_1_2")]
		CUSTOM = CL_DEVICE_TYPE_CUSTOM
	}
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_device_fp_config) DeviceFpConfig {
		DENORM = CL_FP_DENORM,
		INF_NAN = CL_FP_INF_NAN,
		ROUND_TO_NEAREST = CL_FP_ROUND_TO_NEAREST,
		ROUND_TO_ZERO = CL_FP_ROUND_TO_ZERO,
		ROUND_TO_INF = CL_FP_ROUND_TO_INF,
		FMA = CL_FP_FMA,
		#[cfg(feature = "opencl_1_1")]
		SOFT_FLOAT = CL_FP_SOFT_FLOAT,
		#[cfg(feature = "opencl_1_2")]
		CORRECTLY_ROUNDED_DIVIDE_SQRT = CL_FP_CORRECTLY_ROUNDED_DIVIDE_SQRT
	}
}

cl_enum! {
	pub enum DeviceMemCacheType {
		None = CL_NONE,
		ReadOnly = CL_READ_ONLY_CACHE,
		ReadWrite = CL_READ_WRITE_CACHE
	}
}

cl_enum! {
	pub enum DeviceLocalMemType {
		Local = CL_LOCAL,
		Global = CL_GLOBAL
	}
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_device_exec_capabilities) DeviceExecCapabilities {
		KERNEL = CL_EXEC_KERNEL,
		NATIVE_KERNEL = CL_EXEC_NATIVE_KERNEL
	}
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_command_queue_properties) CommandQueueProperties {
		OUT_OF_ORDER_EXECUTION = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE,
		PROFILING = CL_QUEUE_PROFILING_ENABLE,
		#[cfg(feature = "opencl_2_0")]
		ON_DEVICE = CL_QUEUE_ON_DEVICE,
		#[cfg(feature = "opencl_2_0")]
		ON_DEVICE_DEFAULT = CL_QUEUE_ON_DEVICE_DEFAULT
	}
}

#[cfg(feature = "opencl_1_2")]
cl_enum! {
	pub enum DevicePartitionProperty {
		Equally = CL_DEVICE_PARTITION_EQUALLY,
		ByCounts = CL_DEVICE_PARTITION_BY_COUNTS,
		ByAffinityDomain = CL_DEVICE_PARTITION_BY_AFFINITY_DOMAIN
	}
}

#[cfg(feature = "opencl_1_2")]
cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_2::cl_device_affinity_domain) DeviceAffinityDomain {
		NUMA = CL_DEVICE_AFFINITY_DOMAIN_NUMA,
		L4_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L4_CACHE,
		L3_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L3_CACHE,
		L2_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L2_CACHE,
		L1_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L1_CACHE,
		NEXT_PARTITIONABLE = CL_DEVICE_AFFINITY_DOMAIN_NEXT_PARTITIONABLE
	}
}

#[cfg(feature = "opencl_2_0")]
cl_bitfield! {
	pub bitfield(opencl_ffi::cl_2_0::cl_device_svm_capabilities) DeviceSvmCapabilities {
		COARSE_GRAIN_BUFFER = CL_DEVICE_SVM_COARSE_GRAIN_BUFFER,
		FINE_GRAIN_BUFFER = CL_DEVICE_SVM_FINE_GRAIN_BUFFER,
		FINE_GRAIN_SYSTEM = CL_DEVICE_SVM_FINE_GRAIN_SYSTEM,
		ATOMICS = CL_DEVICE_SVM_ATOMICS
	}
}

#[cfg(feature = "opencl_3_0")]
cl_bitfield! {
	pub bitfield(opencl_ffi::cl_3_0::cl_device_atomic_capabilities) DeviceAtomicCapabilities {
		ORDER_RELAXED = CL_DEVICE_ATOMIC_ORDER_RELAXED,
		ORDER_ACQ_REL = CL_DEVICE_ATOMIC_ORDER_ACQ_REL,
		ORDER_SEQ_CST = CL_DEVICE_ATOMIC_ORDER_SEQ_CST,
		SCOPE_WORK_ITEM = CL_DEVICE_ATOMIC_SCOPE_WORK_ITEM,
		SCOPE_WORK_GROUP = CL_DEVICE_ATOMIC_SCOPE_WORK_GROUP,
		SCOPE_DEVICE = CL_DEVICE_ATOMIC_SCOPE_DEVICE,
		SCOPE_ALL_DEVICES = CL_DEVICE_ATOMIC_SCOPE_ALL_DEVICES
	}
}

#[cfg(feature = "opencl_3_0")]
cl_bitfield! {
	pub bitfield(opencl_ffi::cl_3_0::cl_device_atomic_capabilities) DeviceEnqueueCapabilities {
		QUEUE_SUPPORTED = CL_DEVICE_QUEUE_SUPPORTED,
		QUEUE_REPLACEABLE_DEFAULT = CL_DEVICE_QUEUE_REPLACEABLE_DEFAULT
	}
}

macro_rules! get_device_info {
	(
		$id: expr;
		$(
			let $var_name: ident = $property_name: ident ::{$( $property_type: tt )+} $( => $and_then_closure: expr )?
		);+ $(;)?
	) => {
		$(
			let $var_name = unsafe {
				cl_get_info!(
					clGetDeviceInfo(
						$id.handle(),
						opencl_ffi::prelude::$property_name
					) -> Result<$( $property_type )+, DeviceInfoError>
				)
			}.map_err(GetDeviceInfoError::from)
			$(
				.and_then($and_then_closure)
			)?
			?;
		)+
	};
}

fn get_info_map_string(bytes: Vec<u8>) -> Result<String, GetDeviceInfoError> {
	char_array_to_string(bytes).map_err(GetDeviceInfoError::from)
}
fn get_info_map_bool(value: cl_bool) -> Result<bool, GetDeviceInfoError> {
	Ok(value == CL_TRUE)
}
fn get_info_map_bitfield<B: Bitfield + Any>(value: B::BitType) -> Result<B, GetDeviceInfoError> {
	B::from_raw(value).map_err(
		|v| {
			GetDeviceInfoError::InvalidBitfieldValue {
				actual: v.into(),
				allowed: B::ALL.into_raw().into(),
				bitfield_name: std::any::type_name::<B>()
			}
		}
	)
}
fn get_info_map_enum<E: TryFrom<cl_uint, Error = cl_uint> + Any>(value: cl_uint) -> Result<E, GetDeviceInfoError> {
	E::try_from(value).map_err(
		|v| {
			GetDeviceInfoError::InvalidEnumValue {
				actual: v,
				enum_name: std::any::type_name::<E>()
			}
		}
	)
}

#[derive(Debug)]
pub struct DeviceBasicInfo {
	pub device_type: DeviceType,
	pub vendor_id: cl_uint,
	pub available: bool,
	pub compiler_available: bool,
	pub platform_id: PlatformId,
	pub name: String,
	pub vendor: String,
	pub driver_version: String,
	pub profile: String,
	pub version: String,
	pub extensions: Vec<String>
}
impl DeviceBasicInfo {
	pub fn get(id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		get_device_info! {
			id;
			let device_type = CL_DEVICE_TYPE::{cl_device_type} => get_info_map_bitfield::<DeviceType>;
			let vendor_id = CL_DEVICE_VENDOR_ID::{cl_uint};
			let available = CL_DEVICE_AVAILABLE::{cl_bool} => get_info_map_bool;
			let compiler_available = CL_DEVICE_COMPILER_AVAILABLE::{cl_bool} => get_info_map_bool;
			let name = CL_DEVICE_NAME::{Vec<u8>} => get_info_map_string;
			let vendor = CL_DEVICE_VENDOR::{Vec<u8>} => get_info_map_string;
			let driver_version = CL_DRIVER_VERSION::{Vec<u8>} => get_info_map_string;
			let profile = CL_DEVICE_PROFILE::{Vec<u8>} => get_info_map_string;
			let version = CL_DEVICE_VERSION::{Vec<u8>} => get_info_map_string;
			let extensions = CL_DEVICE_EXTENSIONS::{Vec<u8>} => |bytes| get_info_map_string(bytes).map(
				|string| string.split(' ').map(String::from).collect::<Vec<String>>()
			)
		};

		// special because it needs a non-copy initializer
		let platform_id = unsafe {
			cl_get_info!(
				clGetDeviceInfo(
					id.handle(),
					CL_DEVICE_PLATFORM
				) -> Result<cl_platform_id = std::ptr::null_mut(), DeviceInfoError>
			)
		}.map(
			|id| unsafe { PlatformId::new(id) }
		)?;
		
		Ok(
			DeviceBasicInfo {
				device_type,
				vendor_id,
				available,
				compiler_available,
				platform_id,
				name,
				vendor,
				driver_version,
				profile,
				version,
				extensions
			}
		)
	}
}

#[derive(Debug)]
pub struct DeviceInfo {
	pub basic: DeviceBasicInfo,
	pub max_compute_units: cl_uint,
	pub max_work_item_dimensions: cl_uint,
	pub max_work_item_sizes: Vec<size_t>,
	pub max_work_group_size: size_t,
	pub preferred_vector_width_char: cl_uint,
	pub preferred_vector_width_short: cl_uint,
	pub preferred_vector_width_int: cl_uint,
	pub preferred_vector_width_long: cl_uint,
	pub preferred_vector_width_float: cl_uint,
	pub preferred_vector_width_double: cl_uint,
	pub max_clock_frequency: cl_uint,
	pub address_bits: cl_uint,
	pub max_mem_alloc_size: cl_ulong,
	pub image_support: bool,
	pub max_read_image_args: cl_uint,
	pub max_write_image_args: cl_uint,
	pub image2d_max_width: size_t,
	pub image2d_max_height: size_t,
	pub image3d_max_width: size_t,
	pub image3d_max_height: size_t,
	pub image3d_max_depth: size_t,
	pub max_samplers: cl_uint,
	pub max_parameter_size: size_t,
	pub mem_base_addr_align: cl_uint,
	// pub min_data_type_align_size: cl_uint // deprecated 1.2
	pub single_fp_config: DeviceFpConfig,
	pub global_mem_cache_type: DeviceMemCacheType,
	pub global_mem_cacheline_size: cl_uint,
	pub global_mem_cache_size: cl_ulong,
	pub global_mem_size: cl_ulong,
	pub max_constant_buffer_size: cl_ulong,
	pub max_constant_args: cl_uint,
	pub local_mem_type: DeviceLocalMemType,
	pub local_mem_size: cl_ulong,
	pub error_correction_support: bool,
	pub profiling_timer_resolution: size_t,
	pub endian_little: bool,
	// pub queue_properties: CommandQueueProperties // deprecated 2.0
}
impl DeviceInfo {
	pub fn get(id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		get_device_info! {
			id;
			let max_compute_units = CL_DEVICE_MAX_COMPUTE_UNITS::{cl_uint};
			let max_work_item_dimensions = CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS::{cl_uint};
			let max_work_item_sizes = CL_DEVICE_MAX_WORK_ITEM_SIZES::{Vec<size_t>};
			let max_work_group_size = CL_DEVICE_MAX_WORK_GROUP_SIZE::{size_t};
			let preferred_vector_width_char = CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR::{cl_uint};
			let preferred_vector_width_short = CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT::{cl_uint};
			let preferred_vector_width_int = CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT::{cl_uint};
			let preferred_vector_width_long = CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG::{cl_uint};
			let preferred_vector_width_float = CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT::{cl_uint};
			let preferred_vector_width_double = CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE::{cl_uint};
			let max_clock_frequency = CL_DEVICE_MAX_CLOCK_FREQUENCY::{cl_uint};
			let address_bits = CL_DEVICE_ADDRESS_BITS::{cl_uint};
			let max_mem_alloc_size = CL_DEVICE_MAX_MEM_ALLOC_SIZE::{cl_ulong};
			let image_support = CL_DEVICE_IMAGE_SUPPORT::{cl_bool} => get_info_map_bool;
			let max_read_image_args = CL_DEVICE_MAX_READ_IMAGE_ARGS::{cl_uint};
			let max_write_image_args = CL_DEVICE_MAX_WRITE_IMAGE_ARGS::{cl_uint};
			let image2d_max_width = CL_DEVICE_IMAGE2D_MAX_WIDTH::{size_t};
			let image2d_max_height = CL_DEVICE_IMAGE2D_MAX_HEIGHT::{size_t};
			let image3d_max_width = CL_DEVICE_IMAGE3D_MAX_WIDTH::{size_t};
			let image3d_max_height = CL_DEVICE_IMAGE3D_MAX_HEIGHT::{size_t};
			let image3d_max_depth = CL_DEVICE_IMAGE3D_MAX_DEPTH::{size_t};
			let max_samplers = CL_DEVICE_MAX_SAMPLERS::{cl_uint};
			let max_parameter_size = CL_DEVICE_MAX_PARAMETER_SIZE::{size_t};
			let mem_base_addr_align = CL_DEVICE_MEM_BASE_ADDR_ALIGN::{cl_uint};
			let single_fp_config = CL_DEVICE_SINGLE_FP_CONFIG::{cl_device_fp_config} => get_info_map_bitfield::<DeviceFpConfig>;
			let global_mem_cache_type = CL_DEVICE_GLOBAL_MEM_CACHE_TYPE::{cl_device_mem_cache_type} => get_info_map_enum::<DeviceMemCacheType>;
			let global_mem_cacheline_size = CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE::{cl_uint};
			let global_mem_cache_size = CL_DEVICE_GLOBAL_MEM_CACHE_SIZE::{cl_ulong};
			let global_mem_size = CL_DEVICE_GLOBAL_MEM_SIZE::{cl_ulong};
			let max_constant_buffer_size = CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE::{cl_ulong};
			let max_constant_args = CL_DEVICE_MAX_CONSTANT_ARGS::{cl_uint};
			let local_mem_type = CL_DEVICE_LOCAL_MEM_TYPE::{cl_device_local_mem_type} => get_info_map_enum::<DeviceLocalMemType>;
			let local_mem_size = CL_DEVICE_LOCAL_MEM_SIZE::{cl_ulong};
			let error_correction_support = CL_DEVICE_ERROR_CORRECTION_SUPPORT::{cl_bool} => get_info_map_bool;
			let profiling_timer_resolution = CL_DEVICE_PROFILING_TIMER_RESOLUTION::{size_t};
			let endian_little = CL_DEVICE_ENDIAN_LITTLE::{cl_bool} => get_info_map_bool;
		};
		
		Ok(
			DeviceInfo {
				basic: DeviceBasicInfo::get(id)?,
				max_compute_units,
				max_work_item_dimensions,
				max_work_item_sizes,
				max_work_group_size,
				preferred_vector_width_char,
				preferred_vector_width_short,
				preferred_vector_width_int,
				preferred_vector_width_long,
				preferred_vector_width_float,
				preferred_vector_width_double,
				max_clock_frequency,
				address_bits,
				max_mem_alloc_size,
				image_support,
				max_read_image_args,
				max_write_image_args,
				image2d_max_width,
				image2d_max_height,
				image3d_max_width,
				image3d_max_height,
				image3d_max_depth,
				max_samplers,
				max_parameter_size,
				mem_base_addr_align,
				single_fp_config,
				global_mem_cache_type,
				global_mem_cacheline_size,
				global_mem_cache_size,
				global_mem_size,
				max_constant_buffer_size,
				max_constant_args,
				local_mem_type,
				local_mem_size,
				error_correction_support,
				profiling_timer_resolution,
				endian_little
			}
		)
	}
}

#[cfg(feature = "opencl_1_1")]
#[derive(Debug)]
pub struct DeviceInfo11 {
	pub preferred_vector_width_half: cl_uint,
	pub native_vector_width_char: cl_uint,
	pub native_vector_width_short: cl_uint,
	pub native_vector_width_int: cl_uint,
	pub native_vector_width_long: cl_uint,
	pub native_vector_width_float: cl_uint,
	pub native_vector_width_double: cl_uint,
	pub native_vector_width_half: cl_uint,
	// pub host_unified_memory: bool // deprecated 2.0
	// pub opencl_c_version: // deprecated 3.0
}
#[cfg(feature = "opencl_1_1")]
impl DeviceInfo11 {
	pub fn get(_id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		todo!()
	}
}

#[cfg(feature = "opencl_1_2")]
#[derive(Debug)]
pub struct DeviceInfo12 {
	pub image_max_buffer_size: size_t,
	pub image_max_array_size: size_t,
	pub double_fp_config: DeviceFpConfig,
	pub linker_available: bool,
	pub build_in_kernels: Vec<String>,
	pub printf_buffer_size: size_t,
	pub preferred_interop_user_sync: bool,
	pub parent_device: Option<DeviceId>,
	pub partition_max_sub_devices: cl_uint,
	pub partition_properties: Vec<DevicePartitionProperty>, // filter 0
	pub partition_affinity_domain: DeviceAffinityDomain,
	// pub partition_type: Vec<DevicePartitionProperty> // wtf OpenCL, the type is completely wrong
	pub reference_count: cl_uint,

}
#[cfg(feature = "opencl_1_2")]
impl DeviceInfo12 {
	pub fn get(_id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		todo!()
	}
}

#[cfg(feature = "opencl_2_0")]
#[derive(Debug)]
pub struct DeviceInfo20 {
	pub max_read_write_image_args: cl_uint,
	pub max_global_variable_size: size_t,
	pub global_variable_preferred_total_size: size_t,
	pub queue_on_host_properties: CommandQueueProperties,
	pub queue_on_device_properties: CommandQueueProperties,
	pub queue_on_device_preferred_size: cl_uint,
	pub queue_on_device_max_size: cl_uint,
	pub max_on_device_queues: cl_uint,
	pub max_on_device_events: cl_uint,
	pub svm_capabilities: DeviceSvmCapabilities,
	pub preffered_platform_atomic_alignment: cl_uint,
	pub preffered_global_atomic_alignment: cl_uint,
	pub preffered_local_atomic_alignment: cl_uint
}
#[cfg(feature = "opencl_2_0")]
impl DeviceInfo20 {
	pub fn get(_id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		todo!()
	}
}

#[cfg(feature = "opencl_2_1")]
#[derive(Debug)]
pub struct DeviceInfo21 {
	pub il_version: Vec<String>,
	pub image_pitch_alignment: cl_uint,
	pub image_base_address_alignment: cl_uint,
	pub max_pipe_args: cl_uint,
	pub pipe_max_active_reservations: cl_uint,
	pub pipe_max_packet_size: cl_uint,
	pub max_num_sub_groups: cl_uint,
	pub sub_group_independent_forward_progress: bool
}
#[cfg(feature = "opencl_2_1")]
impl DeviceInfo21 {
	pub fn get(_id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		todo!()
	}
}

#[cfg(feature = "opencl_3_0")]
#[derive(Debug)]
pub struct DeviceInfo30 {
	pub ils_with_version: Vec<crate::common::version::NameVersion>,
	pub built_in_kernels_with_version: Vec<crate::common::version::NameVersion>,
	pub numeric_version: crate::common::version::UnpackedVersion,
	pub opencl_c_all_version: Vec<crate::common::version::NameVersion>,
	pub opencl_c_features: Vec<crate::common::version::NameVersion>,
	pub extensions_with_version: Vec<crate::common::version::NameVersion>,
	pub atomic_memory_capabilities: DeviceAtomicCapabilities,
	pub atomic_fence_capabilities: DeviceAtomicCapabilities,
	pub non_uniform_work_group_support: bool,
	pub work_group_collective_functions_support: bool,
	pub generic_address_space_support: bool,
	pub device_enqueue_capabilities: DeviceEnqueueCapabilities,
	pub pipe_support: bool,
	pub preferred_work_group_size_multiple: size_t,
	pub latest_conformance_version_passed: String

}
#[cfg(feature = "opencl_3_0")]
impl DeviceInfo30 {
	pub fn get(_id: DeviceId) -> Result<Self, GetDeviceInfoError> {
		todo!()
	}
}