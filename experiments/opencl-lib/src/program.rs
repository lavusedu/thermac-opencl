use std::{os::raw::c_char, ffi::{CStr, c_void}};

use opencl_ffi::cl_1_0::{
	size_t, cl_int, cl_uint, cl_program,
	clCreateProgramWithSource, clBuildProgram, clReleaseProgram,
	clCreateProgramWithBinary,
	clGetProgramInfo, clGetProgramBuildInfo
};

use crate::prelude::{ClObject, Context, SharedAccess, DeviceId};

cl_error! {
	"create kernel program from sources"
	pub enum KernelProgramFromSourcesError {
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidValue = CL_INVALID_VALUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"create kernel program from binaries"
	pub enum KernelProgramFromBinariesError {
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidValue = CL_INVALID_VALUE,
		InvalidDevice = CL_INVALID_DEVICE,
		InvalidBinary = CL_INVALID_BINARY,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"release kernel program"
	pub enum KernelProgramDropError {
		InvalidProgram = CL_INVALID_PROGRAM,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"build kernel program"
	pub enum KernelProgramBuildError {
		InvalidProgram = CL_INVALID_PROGRAM,
		InvalidValue = CL_INVALID_VALUE,
		InvalidDevice = CL_INVALID_DEVICE,
		InvalidBinary = CL_INVALID_BINARY,
		InvalidBuildOptions = CL_INVALID_BUILD_OPTIONS,
		CompilerNotAvailable = CL_COMPILER_NOT_AVAILABLE,
		BuildProgramFailure = CL_BUILD_PROGRAM_FAILURE,
		InvalidOperation = CL_INVALID_OPERATION,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"retrieve program build info"
	pub enum KernelProgramGetBuildInfoError {
		InvalidDevice = CL_INVALID_DEVICE,
		InvalidValue = CL_INVALID_VALUE,
		InvalidProgram = CL_INVALID_PROGRAM,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"retrieve program info"
	pub enum KernelProgramGetProgramInfoError {
		InvalidValue = CL_INVALID_VALUE,
		InvalidProgram = CL_INVALID_PROGRAM
	}
}

#[derive(Debug)]
pub struct KernelProgram<C: SharedAccess<Context>> {
	context: C,
	program: cl_program
}
impl<C: SharedAccess<Context>> KernelProgram<C> {
	pub fn from_sources<const N: usize>(
		context: C,
		sources: [&str; N]
	) -> Result<Self, KernelProgramFromSourcesError> {
		log::debug!("Building program from sources");
		if log::log_enabled!(log::Level::Trace) {
			for (index, source) in sources.iter().enumerate() {
				log::trace!("Source #{}:\n{:#}", index + 1, source);
			}
		}
		
		let lengths: [size_t; N] = sources.map(
			|s| s.len() as size_t
		);
		let sources: [*const c_char; N] = sources.map(
			|s| s.as_ptr() as *const c_char
		);
		
		let program = unsafe {
			cl_try!(
				noret KernelProgramFromSourcesError: clCreateProgramWithSource(
					context.handle(),
					N as cl_uint,
					sources.as_ptr() as *mut _,
					lengths.as_ptr()
				)
			)?
		};

		Ok(
			KernelProgram {
				context,
				program
			}
		)
	}

	pub fn from_sources_dynamic<'a>(
		context: C,
		sources: impl Iterator<Item = &'a str>
	) -> Result<Self, KernelProgramFromSourcesError> {
		log::debug!("Building program from sources");
		let (sources, lengths): (Vec<_>, Vec<_>) = sources.inspect(
			|s| log::trace!("Source: {}", s)
		).map(
			|source| {
				(
					source.as_ptr() as *const c_char,
					source.len() as size_t
				)
			}
		).unzip();

		let program = unsafe {
			cl_try!(
				noret KernelProgramFromSourcesError: clCreateProgramWithSource(
					context.handle(),
					sources.len() as cl_uint,
					sources.as_ptr() as *mut _,
					lengths.as_ptr()
				)
			)?
		};

		Ok(
			KernelProgram {
				context,
				program
			}
		)
	}

	pub fn from_binaries<const N: usize>(
		context: C,
		binaries: [&[u8]; N]
	) -> Result<Self, KernelProgramFromBinariesError> {
		assert_eq!(N, context.devices().count());
		log::debug!("Building program from binaries");
		if log::log_enabled!(log::Level::Trace) {
			for (index, binary) in binaries.iter().enumerate() {
				log::trace!("Binary #{}:\n{:?}", index + 1, binary);
			}
		}

		let lengths: [size_t; N] = binaries.map(
			|b| b.len() as size_t
		);

		let mut binaries_ptr: [*const u8; N] = binaries.map(
			|b| b.as_ptr()
		);

		let mut devices: [<DeviceId as ClObject>::HandleType; N] = [std::ptr::null_mut(); N];
		for (i, device) in context.devices().enumerate() {
			devices[i] = device.handle();
		}
		
		let mut results: [cl_int; N] = [opencl_ffi::prelude::CL_SUCCESS; N];
		let program = unsafe {
			cl_try!(
				noret KernelProgramFromBinariesError: clCreateProgramWithBinary(
					context.handle(),
					N as cl_uint,
					devices.as_ptr(),
					lengths.as_ptr(),
					binaries_ptr.as_mut_ptr(),
					results.as_mut_ptr()
				)
			)?
		};

		for (result, device) in IntoIterator::into_iter(results).zip(context.devices()) {
			match result {
				opencl_ffi::prelude::CL_SUCCESS => (),
				code => log::error!("Failed to build program from binaries for device {:?}: {}", device.handle(), KernelProgramFromBinariesError::from(code))
			}
		}
		
		Ok(
			KernelProgram {
				context,
				program
			}
		)
	}

	pub fn context(&self) -> &C {
		&self.context
	}

	pub fn build(self) -> Result<BuiltKernelProgram<C>, KernelProgramBuildError> {
		let mut built = BuiltKernelProgram(self);
		built.rebuild()?;

		Ok(
			built
		)
	}
}
impl<C: SharedAccess<Context>> ClObject for KernelProgram<C> {
	type HandleType = cl_program;
	
	fn handle(&self) -> Self::HandleType {
		self.program
	}
}
impl_fallible_drop! {
	impl KernelProgram<C> where [C: SharedAccess<Context>] {
		pub fn release(self) -> Result<(), KernelProgramDropError> {
			unsafe {
				cl_try!(
					KernelProgramDropError: clReleaseProgram(self.program)
				)
			}
		}
	}
}

#[derive(Debug)]
pub struct BuiltKernelProgram<C: SharedAccess<Context>>(KernelProgram<C>);
impl<C: SharedAccess<Context>> BuiltKernelProgram<C> {
	fn get_build_logs(&self) ->  Vec<Result<String, KernelProgramGetBuildInfoError>> {
		let mut results = Vec::new();
		
		for device in self.context.devices() {
			let log = unsafe {
				cl_get_info!(
					clGetProgramBuildInfo(
						self.program,
						device.handle(),
						opencl_ffi::cl_1_0::CL_PROGRAM_BUILD_LOG
					) -> Result<Vec<u8>, KernelProgramGetBuildInfoError>
				)
			}.map(
				|chars| CStr::from_bytes_with_nul(&chars).unwrap().to_string_lossy().into_owned()
			);

			results.push(log);
		}

		results
	}

	pub fn get_program_binaries(&self) -> Result<Vec<Vec<u8>>, KernelProgramGetProgramInfoError> {
		let sizes = unsafe {
			cl_get_info!(
				clGetProgramInfo(
					self.program,
					opencl_ffi::cl_1_0::CL_PROGRAM_BINARY_SIZES
				) -> Result<Vec<size_t>, KernelProgramGetProgramInfoError>
			)?
		};

		let mut buffers: Vec<Vec<u8>> = Vec::with_capacity(sizes.len());
		let mut buffer_pointers: Vec<*mut c_void> = Vec::with_capacity(sizes.len());
		for size in sizes.into_iter() {
			let mut buffer = vec![0; size as usize];
			buffer_pointers.push(buffer.as_mut_ptr() as *mut c_void);
			buffers.push(buffer);
		}
		
		unsafe {
			cl_try!(
				KernelProgramGetProgramInfoError: clGetProgramInfo(
					self.program,
					opencl_ffi::cl_1_0::CL_PROGRAM_BINARIES,
					(buffer_pointers.len() * std::mem::size_of::<*mut c_void>()) as size_t,
					buffer_pointers.as_mut_ptr() as *mut c_void,
					std::ptr::null_mut()
				)
			)?;
		}

		Ok(buffers)
	}

	pub fn rebuild(&mut self) -> Result<(), KernelProgramBuildError> {
		let result = unsafe {
			cl_try!(
				KernelProgramBuildError: clBuildProgram(
					self.program,
					0,
					std::ptr::null(),
					std::ptr::null(),
					None,
					std::ptr::null_mut()
				)
			)
		};

		match result {
			Err(KernelProgramBuildError::BuildProgramFailure) => {
				for (result, device) in self.get_build_logs().into_iter().zip(self.context.devices()) {
					match result {
						Err(err) => log::error!("Failed to retrieve build logs from device {:?}: {}", device.handle(), err),
						Ok(logs) => log::error!("Device {:?} build logs:\n{}", device.handle(), logs)
					}
				}
			}
			_ => ()
		}

		result
	}
	
	pub fn into_inner(self) -> KernelProgram<C> {
		self.0
	}
}
impl<C: SharedAccess<Context>> std::ops::Deref for BuiltKernelProgram<C> {
	type Target = KernelProgram<C>;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl<C: SharedAccess<Context>> std::ops::DerefMut for BuiltKernelProgram<C> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}
