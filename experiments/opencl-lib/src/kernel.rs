use std::ffi::{CString, c_void};

use opencl_ffi::cl_1_0::{cl_uint, cl_kernel, clCreateKernel, clReleaseKernel, clSetKernelArg, size_t, clEnqueueNDRangeKernel};

use crate::prelude::{BuiltKernelProgram, ClObject, Context, SharedAccess, CommandQueue, Event};

cl_error! {
	"create kernel"
	pub enum KernelInitError {
		InvalidProgram = CL_INVALID_PROGRAM,
		InvalidProgramExecutable = CL_INVALID_PROGRAM_EXECUTABLE,
		InvalidKernelName = CL_INVALID_KERNEL_NAME,
		InvalidKernelDefinition = CL_INVALID_KERNEL_DEFINITION,
		InvalidValue = CL_INVALID_VALUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"release kernel"
	pub enum KernelDropError {
		InvalidProgram = CL_INVALID_PROGRAM,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"set kernel argument"
	pub enum KernelSetArgError {
		InvalidKernel = CL_INVALID_KERNEL,
		InvalidArgIndex = CL_INVALID_ARG_INDEX,
		InvalidArgValue = CL_INVALID_ARG_VALUE,
		InvalidMemObject = CL_INVALID_MEM_OBJECT,
		InvalidSampler = CL_INVALID_SAMPLER,
		#[cfg(feature = "opencl_2_0")]
		InvalidDeviceQueue = CL_INVALID_DEVICE_QUEUE,
		InvalidArgSize = CL_INVALID_ARG_SIZE,
		#[cfg(feature = "opencl_2_2")]
		InvalidMaxSizeRestrictionExceeded = CL_MAX_SIZE_RESTRICTION_EXCEEDED,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"enqueue kernel nd range execution"
	pub enum KernelEnqueueNdRangeExecutionError {
		InvalidProgramExecutable = CL_INVALID_PROGRAM_EXECUTABLE,
		InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE,
		InvalidKernel = CL_INVALID_KERNEL,
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidKernelArgs = CL_INVALID_KERNEL_ARGS,
		InvalidWorkDimension = CL_INVALID_WORK_DIMENSION,
		InvalidGlobalWorkSize = CL_INVALID_GLOBAL_WORK_SIZE,
		InvalidGlobalOffset = CL_INVALID_GLOBAL_OFFSET,
		InvalidWorkGroupSize = CL_INVALID_WORK_GROUP_SIZE,
		InvalidWorkItemSize = CL_INVALID_WORK_ITEM_SIZE,
		#[cfg(feature = "opencl_1_1")]
		MisalignedSubBufferOffset = CL_MISALIGNED_SUB_BUFFER_OFFSET,
		InvalidImageSize = CL_INVALID_IMAGE_SIZE,
		ImageFormatNotSupported = CL_IMAGE_FORMAT_NOT_SUPPORTED,
		OutOfResources = CL_OUT_OF_RESOURCES,
		AllocationFailure = CL_MEM_OBJECT_ALLOCATION_FAILURE,
		InvalidEventWaitList = CL_INVALID_EVENT_WAIT_LIST,
		InvalidOperation = CL_INVALID_OPERATION,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

#[derive(Debug)]
pub struct Kernel<C: SharedAccess<Context>, P: SharedAccess<BuiltKernelProgram<C>>> {
	program: P,
	ghost: std::marker::PhantomData<C>,
	kernel: cl_kernel
}
impl<C: SharedAccess<Context>, P: SharedAccess<BuiltKernelProgram<C>>> Kernel<C, P> {
	pub fn new(
		program: P,
		name: &str
	) -> Result<Self, KernelInitError> {
		let c_name = CString::new(name.as_bytes()).unwrap();

		let kernel = unsafe {
			cl_try!(
				noret KernelInitError: clCreateKernel(
					program.handle(),
					c_name.as_ptr()
				)
			)?
		};
		
		Ok(
			Kernel {
				program,
				ghost: std::marker::PhantomData,
				kernel
			}
		)
	}

	pub fn program(&self) -> &P {
		&self.program
	}

	pub fn context(&self) -> &C {
		self.program.context()
	}

	pub fn set_arg<T: ?Sized>(&mut self, index: cl_uint, value: &T) -> Result<(), KernelSetArgError> {
		log::debug!(
			"Setting kernel arg #{}: [u8; {}] {:?}",
			index,
			std::mem::size_of_val(value),
			unsafe {
				std::slice::from_raw_parts(
					value as *const _ as *const u8,
					std::mem::size_of_val(value)
				)
			}
		);
		
		unsafe {
			cl_try!(
				KernelSetArgError: clSetKernelArg(
					self.kernel,
					index,
					std::mem::size_of_val(value) as size_t,
					value as *const _ as *const c_void
				)
			)?;
		}
		
		Ok(())
	}
}
impl<'e, C: SharedAccess<Context> + 'e, P: SharedAccess<BuiltKernelProgram<C>>> Kernel<C, P> {
	impl_enqueue_with_event! {
		pub fn {
			name = enqueue_nd_range,
			name_with_event = enqueue_nd_range_with_event
		} [const DIMS: usize] (
			..,
			work_offset: Option<[size_t; DIMS]>,
			work_size: [size_t; DIMS],
			group_size: Option<[size_t; DIMS]>
		) -> Result<.., KernelEnqueueNdRangeExecutionError> {
			{
				#[cfg(not(feature = "opencl_1_1"))]
				assert!(work_offset.is_none());
			}

			clEnqueueNDRangeKernel(
				..,
				DIMS as cl_uint,
				work_offset.as_ref().map(|v| v.as_ptr()).unwrap_or(std::ptr::null()),
				work_size.as_ptr(),
				group_size.as_ref().map(|v| v.as_ptr()).unwrap_or(std::ptr::null())
			)
		}
	}
}
impl<C: SharedAccess<Context>, P: SharedAccess<BuiltKernelProgram<C>>> ClObject for Kernel<C, P> {
	type HandleType = cl_kernel;
	
	fn handle(&self) -> Self::HandleType {
		self.kernel
	}
}
impl_fallible_drop! {
	impl Kernel<C, P> where [C: SharedAccess<Context>, P: SharedAccess<BuiltKernelProgram<C>>] {
		pub fn release(self) -> Result<(), KernelDropError> {
			unsafe {
				cl_try!(
					KernelDropError: clReleaseKernel(self.kernel)
				)
			}
		}
	}
}
