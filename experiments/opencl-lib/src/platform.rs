use std::ffi::IntoStringError;

use thiserror::Error;

use opencl_ffi::{
	cl_1_0::{cl_platform_id, clGetPlatformIDs, CL_PLATFORM_PROFILE, clGetPlatformInfo}, prelude::{CL_PLATFORM_EXTENSIONS, CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION}
};

use crate::prelude::ClObject;
use crate::common::string::char_array_to_string;

cl_error! {
	"query platforms ids"
	pub enum PlatformIdQueryError {
		InvalidValue = CL_INVALID_VALUE,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"get platform info"
	pub enum PlatformInfoError {
		InvalidPlatform = CL_INVALID_PLATFORM,
		InvalidValue = CL_INVALID_VALUE,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}
#[derive(Debug, Error)]
pub enum GetPlatformInfoError {
	#[error("Failed to convert C string to utf8: {0}")]
	IntoStringError(#[from] IntoStringError),
	#[error(transparent)]
	PlatformInfoError(#[from] PlatformInfoError)
}

#[derive(Debug)]
pub struct PlatformInfo {
	pub profile: String,
	pub version: String,
	pub name: String,
	pub vendor: String,
	pub extensions: Vec<String>,
	#[cfg(feature = "opencl_2_1")]
	pub v21: Result<PlatformInfo21, GetPlatformInfoError>,
	#[cfg(feature = "opencl_3_0")]
	pub v30: Result<PlatformInfo30, GetPlatformInfoError>
}
impl PlatformInfo {
	fn get(id: PlatformId) -> Result<Self, GetPlatformInfoError> {
		macro_rules! get_property {
			(
				$property: expr
			) => {
				{
					char_array_to_string(
						unsafe {
							cl_get_info!(
								clGetPlatformInfo(
									id.handle(),
									$property
								) -> Result<Vec<u8>, PlatformInfoError>
							)?
						}
					)?
				}
			};
		}
		
		let profile = get_property!(CL_PLATFORM_PROFILE);
		let version = get_property!(CL_PLATFORM_VERSION);
		let name = get_property!(CL_PLATFORM_NAME);
		let vendor = get_property!(CL_PLATFORM_VENDOR);
		let extensions = get_property!(CL_PLATFORM_EXTENSIONS).split(' ').map(String::from).collect();

		Ok(
			PlatformInfo {
				profile,
				version,
				name,
				vendor,
				extensions,
				#[cfg(feature = "opencl_2_1")]
				v21: PlatformInfo21::get(id),
				#[cfg(feature = "opencl_3_0")]
				v30: PlatformInfo30::get(id)
			}
		)
	}
}

#[cfg(feature = "opencl_2_1")]
#[derive(Debug)]
pub struct PlatformInfo21 {
	pub host_timer_resolution: opencl_ffi::prelude::cl_ulong
}
#[cfg(feature = "opencl_2_1")]
impl PlatformInfo21 {
	fn get(id: PlatformId) -> Result<Self, GetPlatformInfoError> {
		let host_timer_resolution = unsafe {
			cl_get_info!(
				clGetPlatformInfo(
					id.handle(),
					opencl_ffi::cl_2_1::CL_PLATFORM_HOST_TIMER_RESOLUTION
				) -> Result<opencl_ffi::prelude::cl_ulong, PlatformInfoError>
			)?
		};

		Ok(
			PlatformInfo21 {
				host_timer_resolution
			}
		)
	}
}

#[cfg(feature = "opencl_3_0")]
#[derive(Debug)]
pub struct PlatformInfo30 {
	pub numeric_version: crate::common::version::UnpackedVersion,
	pub extensions_with_version: Vec<crate::common::version::NameVersion>
}
#[cfg(feature = "opencl_3_0")]
impl PlatformInfo30 {
	fn get(id: PlatformId) -> Result<Self, GetPlatformInfoError> {
		use opencl_ffi::cl_3_0::{cl_version, CL_PLATFORM_NUMERIC_VERSION, CL_PLATFORM_EXTENSIONS_WITH_VERSION};
		use crate::common::version::{UnpackedVersion, NameVersionPacked};

		let numeric_version = unsafe {
			cl_get_info!(
				clGetPlatformInfo(
					id.handle(),
					CL_PLATFORM_NUMERIC_VERSION
				) -> Result<cl_version, PlatformInfoError>
			)?
		};

		let name_versions: Vec<NameVersionPacked> = unsafe {
			cl_get_info!(
				clGetPlatformInfo(
					id.handle(),
					CL_PLATFORM_EXTENSIONS_WITH_VERSION
				) -> Result<Vec<NameVersionPacked>, PlatformInfoError>
			)?
		};

		let mut extensions_with_version = Vec::with_capacity(name_versions.len());
		for name_version in name_versions {
			extensions_with_version.push(
				name_version.unpack()?
			);
		}
		
		Ok(
			PlatformInfo30 {
				numeric_version: UnpackedVersion::unpack(numeric_version),
				extensions_with_version
			}
		)
	}
}

#[derive(Debug, Clone, Copy)]
pub struct PlatformId {
	platform_id: cl_platform_id
}
impl PlatformId {
	pub fn query() -> Result<Vec<PlatformId>, PlatformIdQueryError> {
		let len = unsafe {
			let mut len = 0;
			cl_try!(
				PlatformIdQueryError: clGetPlatformIDs(0, std::ptr::null_mut(), &mut len)
			)?;

			len
		};

		let mut platforms: Vec<cl_platform_id> = vec![std::ptr::null_mut(); len as usize];
		unsafe {
			cl_try!(
				PlatformIdQueryError: clGetPlatformIDs(len, platforms.as_mut_ptr(), std::ptr::null_mut())
			)?;
		}

		let platforms = platforms.into_iter().map(
			|id| unsafe { PlatformId::new(id) }
		).collect::<Vec<PlatformId>>();

		Ok(
			platforms
		)
	}

	/// ### Safety
	/// `platform_id` must be a valid platform id handle
	pub unsafe fn new(platform_id: cl_platform_id) -> Self {
		PlatformId {
			platform_id
		}
	}

	pub fn get_info(&self) -> Result<PlatformInfo, GetPlatformInfoError> {
		PlatformInfo::get(*self)
	}
}
impl ClObject for PlatformId {
	type HandleType = cl_platform_id;
	
	fn handle(&self) -> Self::HandleType {
		self.platform_id
	}
}
impl std::fmt::Display for PlatformId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		writeln!(f, "PlatformId({:?}):", self.platform_id)?;

		match self.get_info() {
			Ok(info) => {
				writeln!(f, "info = {:#?}", info)?;
			}
			Err(err) => {
				writeln!(f, "info_error = {}", err)?;
			}
		}

		Ok(())
	}
}