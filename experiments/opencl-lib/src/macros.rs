macro_rules! impl_fallible_drop {
	(
		impl $target: ty $(where [ $($generic_bounds: tt)+ ])? {
			pub fn $method_name: ident($me_ident: ident) -> Result<(), $error_type: ty> {
				$($drop_code: tt)+
			}
		}
	) => {
		impl $(< $($generic_bounds)+ >)? $target {
			pub fn $method_name(self) -> Result<(), $error_type> {
				let mut me = std::mem::ManuallyDrop::new(self);
				
				unsafe {
					me.drop_mut()?;
				}
		
				Ok(())
			}
		
			unsafe fn drop_mut($me_ident: &mut Self) -> Result<(), $error_type> {
				$($drop_code)+
			}
		}
		impl $(< $($generic_bounds)+ >)? Drop for $target {
			fn drop(&mut self) {
				unsafe {
					self.drop_mut().expect("errored in drop");
				}
			}
		}
	}
}

macro_rules! cl_bitfield {
	(
		$vv: vis bitfield($bit_type: ty) $name: ident {
			$(
				$( #[$variant_attr: meta] )*
				$variant_name: ident = $variant_name_ident: ident
			),* $(,)?
		}
	) => {
		#[derive(Clone, Copy, PartialEq, Eq)]
		$vv struct $name($bit_type);
		impl $name {
			$(
				$( #[$variant_attr] )*
				pub const $variant_name: $name = $name(opencl_ffi::prelude::$variant_name_ident as $bit_type);
			)*
		}
		impl $crate::common::bitfield::Bitfield for $name {
			type BitType = $bit_type;
			
			const NONE: $name = $name(0);
			const ALL: $name = $name(
				{
					let value = 0;

					$(
						$( #[$variant_attr] )*
						let value = value | (opencl_ffi::prelude::$variant_name_ident as $bit_type);
					)*

					value
				}
			);

			unsafe fn from_raw_unchecked(raw: $bit_type) -> Self {
				$name(raw)
			}

			fn from_raw(raw: $bit_type) -> Result<Self, $bit_type> {
				let remaining = raw & !Self::ALL.into_raw();
				if remaining != 0 {
					return Err(remaining)
				}

				Ok($name(raw))
			}

			fn into_raw(self) -> $bit_type {
				self.0
			}
		}
		impl std::fmt::Debug for $name {
			fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
				use $crate::common::bitfield::Bitfield;

				write!(
					f,
					concat!(stringify!($name), "({:?} = 0"),
					self.into_raw()
				)?;

				$(
					$( #[$variant_attr] )*
					if (*self & Self::$variant_name) != Self::NONE {
						write!(
							f,
							concat!(" | ", stringify!($variant_name))
						)?;
					}
				)*

				write!(f, ")")?;

				Ok(())
			}
		}
		impl Default for $name {
			fn default() -> Self {
				<Self as $crate::common::bitfield::Bitfield>::NONE
			}
		}
		impl std::ops::BitOr for $name {
            type Output = Self;

            fn bitor(self, rhs: Self) -> Self {
                Self(self.0 | rhs.0)
            }
        }
        impl std::ops::BitOrAssign for $name {
            fn bitor_assign(&mut self, rhs: Self) {
                *self = *self | rhs
            }
        }
        impl std::ops::BitAnd for $name {
            type Output = Self;

            fn bitand(self, rhs: Self) -> Self {
                Self(self.0 & rhs.0)
            }
        }
        impl std::ops::BitAndAssign for $name {
            fn bitand_assign(&mut self, rhs: Self) {
                *self = *self & rhs
            }
        }
        impl std::ops::BitXor for $name {
            type Output = Self;

            fn bitxor(self, rhs: Self) -> Self {
                Self(self.0 ^ rhs.0)
            }
        }
        impl std::ops::BitXorAssign for $name {
            fn bitxor_assign(&mut self, rhs: Self) {
                *self = *self ^ rhs
            }
        }
        impl std::ops::Sub for $name {
            type Output = Self;

            fn sub(self, rhs: Self) -> Self {
                self & !rhs
            }
        }
        impl std::ops::SubAssign for $name {
            fn sub_assign(&mut self, rhs: Self) {
                *self = *self - rhs
            }
        }
        impl std::ops::Not for $name {
            type Output = Self;

            fn not(self) -> Self {
                self ^ <Self as $crate::common::bitfield::Bitfield>::ALL
            }
        }
	}
}

macro_rules! cl_enum {
	(
		$vv: vis enum $name: ident {
			$(
				$( #[$variant_attr: meta] )*
				$variant_name: ident = $variant_code: ident
			),+ $(,)?
		}
	) => {
		#[derive(Debug, Clone, Copy, PartialEq, Eq)]
		$vv enum $name {
			$(
				$( #[$variant_attr] )*
				$variant_name
			),+
		}
		impl std::convert::TryFrom<opencl_ffi::prelude::cl_uint> for $name {
			type Error = opencl_ffi::prelude::cl_uint;

			fn try_from(code: opencl_ffi::prelude::cl_uint) -> Result<Self, Self::Error> {
				match code {
					$(
						$( #[$variant_attr] )*
						opencl_ffi::prelude::$variant_code => Ok($name::$variant_name),
					)+
					code => Err(code)
				}
			}
		}
		impl From<$name> for opencl_ffi::prelude::cl_uint {
			fn from(me: $name) -> Self {
				match me {
					$(
						$( #[$variant_attr] )*
						$name::$variant_name => opencl_ffi::prelude::$variant_code,
					)+
				}
			}
		}
	}	
}

macro_rules! cl_error {
	(
		$message_stem: literal
		pub enum $error_name: ident {
			$(
				$( #[$variant_attr: meta] )*
				$variant_name: ident = $variant_code: ident
			),+ $(,)?
		}
	) => {
		#[derive(Debug, Clone, Copy)]
		pub enum $error_name {
			$(
				$( #[$variant_attr] )*
				$variant_name,
			)+
			Unknown(opencl_ffi::prelude::cl_int)
		}
		impl std::fmt::Display for $error_name {
			fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
				match self {
					$(
						$( #[$variant_attr] )*
						$error_name::$variant_name => write!(
							f,
							concat!("Failed to ", $message_stem, ": ", stringify!($variant_code))
						),
					)+
					$error_name::Unknown(code) => write!(
						f,
						concat!("Failed to ", $message_stem, ": Unknown error code {}"),
						code
					)
				}
			}
		}
		impl std::error::Error for $error_name {}
		impl From<opencl_ffi::prelude::cl_int> for $error_name {
			fn from(code: opencl_ffi::prelude::cl_int) -> Self {
				match code {
					$(
						$( #[$variant_attr] )*
						opencl_ffi::prelude::$variant_code => Self::$variant_name,
					)+
					_ => $error_name::Unknown(code)
				}
			}
		}
		impl From<$error_name> for opencl_ffi::prelude::cl_int {
			fn from(error: $error_name) -> Self {
				match error {
					$(
						$( #[$variant_attr] )*
						$error_name::$variant_name => opencl_ffi::prelude::$variant_code,
					)+
					$error_name::Unknown(code) => code
				}
			}
		}
	}
}

macro_rules! cl_try {
	(
		noret $error_type: ty: $fn_call: ident (
			$(
				$fn_arg: expr
			),* $(,)?
		)
	) => {
		{
			let mut error = opencl_ffi::prelude::CL_SUCCESS;
			let value = $fn_call(
				$( $fn_arg, )*
				&mut error
			);

			match error {
				opencl_ffi::prelude::CL_SUCCESS => Ok(value),
				code => Err(<$error_type>::from(code))
			}
		}
	};
	
	(
		$error_type: ty: $value: expr
	) => {
		match $value {
			opencl_ffi::prelude::CL_SUCCESS => Ok(()),
			code => Err(<$error_type>::from(code))
		}
	};
}

macro_rules! cl_get_info {
	(
		$fn_name: ident (
			$($fn_arg: expr),*
		) -> Result<Vec<$ret_type: ty>, $error_type: ty>
	) => {
		{
			let mut len = 0;
			cl_try!(
				$error_type: $fn_name(
					$($fn_arg,)*
					0,
					std::ptr::null_mut(),
					&mut len
				)
			).and_then(
				|_| {
					let element_count = len as usize / std::mem::size_of::<$ret_type>();
					let mut items = std::vec![<$ret_type>::default(); element_count];
					cl_try!(
						$error_type: $fn_name(
							$($fn_arg,)*
							len,
							items.as_mut_ptr() as *mut std::ffi::c_void,
							std::ptr::null_mut()
						)
					).map(|_| items)
				}
			)
		}
	};

	(
		$fn_name: ident (
			$($fn_arg: expr),*
		) -> Result<$ret_type: ty = $init_value: expr, $error_type: ty>
	) => {
		{
			let mut result = $init_value;
			cl_try!(
				$error_type: $fn_name(
					$($fn_arg,)*
					std::mem::size_of::<$ret_type>() as _,
					&mut result as *mut _ as *mut std::ffi::c_void,
					std::ptr::null_mut()
				)
			).map(
				|_| result
			)
		}
	};

	(
		$fn_name: ident (
			$($fn_arg: expr),*
		) -> Result<$ret_type: ty, $error_type: ty>
	) => {
		{
			let mut result = <$ret_type>::default();
			cl_try!(
				$error_type: $fn_name(
					$($fn_arg,)*
					std::mem::size_of::<$ret_type>() as _,
					&mut result as *mut _ as *mut std::ffi::c_void,
					std::ptr::null_mut()
				)
			).map(
				|_| result
			)
		}
	};
}

#[macro_export]
macro_rules! impl_enqueue_with_event {
	(
		pub fn {
			name = $fn_name: ident,
			name_with_event = $fn_name_with_event: ident
			$(, blocking = $blocking_name: ident)?
		} $([ $($gen_bounds: tt)+ ])? (
			..,
			$( $param_name: ident: $param_type: ty ),+
		) -> Result<.., $error_type: ty> {
			$(
				{ $( $code_before: tt )+ }
			)?

			$cl_fn_name: ident(
				..,
				$( $cl_param: expr ),+
			)
		}
	) => {
		pub fn $fn_name $(< $($gen_bounds)+ >)? (
			&self,
			queue: &CommandQueue<C>,
			$( $param_name: $param_type, )+
			wait_list: impl IntoIterator<Item = &'e Event<C>>,
			$( $blocking_name: bool )?
		) -> Result<(), $error_type> {
			$(
				{ $( $code_before )+ }
			)?

			let (wait_list_len, wait_list) = Event::resolve_wait_list(wait_list);

			unsafe {
				cl_try!(
					$error_type: $cl_fn_name(
						queue.handle(),
						self.handle(),
						$(
							if $blocking_name { CL_TRUE } else { CL_FALSE },
						)?
						$( $cl_param, )+
						wait_list_len,
						wait_list.as_ref().map(|l| l.as_ptr()).unwrap_or(std::ptr::null()),
						std::ptr::null_mut()
					)
				)?;
			}
			
			Ok(())
		}

		pub fn $fn_name_with_event $(< $($gen_bounds)+ >)? (
			&self,
			queue: &CommandQueue<C>,
			$( $param_name: $param_type, )+
			wait_list: impl IntoIterator<Item = &'e Event<C>>,
		) -> Result<Event<C>, $error_type> {
			$(
				{ $( $code_before )+ }
			)?

			let (wait_list_len, wait_list) = Event::resolve_wait_list(wait_list);
			let mut out_event: opencl_ffi::prelude::cl_event = std::ptr::null_mut();

			unsafe {
				cl_try!(
					$error_type: $cl_fn_name(
						queue.handle(),
						self.handle(),
						$(
							{
								let $blocking_name = CL_FALSE;
								$blocking_name
							},
						)?
						$( $cl_param, )+
						wait_list_len,
						wait_list.as_ref().map(|l| l.as_ptr()).unwrap_or(std::ptr::null()),
						&mut out_event
					)
				)?;
			}
			
			Ok(
				unsafe {
					Event::from_raw(self.context().clone(), out_event)
				}
			)
		}
	}
}
