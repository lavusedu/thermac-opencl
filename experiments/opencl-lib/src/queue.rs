use opencl_ffi::cl_1_0::{clCreateCommandQueue, clFinish, clReleaseCommandQueue, cl_command_queue};

use crate::prelude::{Bitfield, ClObject, SharedAccess};

use super::{context::Context, device::DeviceId};

cl_error! {
	"create command queue"
	pub enum CommandQueueInitError {
		InvalidContext = CL_INVALID_CONTEXT,
		InvalidDevice = CL_INVALID_DEVICE,
		InvalidValue = CL_INVALID_VALUE,
		InvalidQueueProperties = CL_INVALID_QUEUE_PROPERTIES,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"release command queue"
	pub enum CommandQueueDropError {
		InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_error! {
	"wait for command queue to finish"
	pub enum CommandQueueFinishError {
		InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE,
		OutOfResources = CL_OUT_OF_RESOURCES,
		OutOfHostMemory = CL_OUT_OF_HOST_MEMORY
	}
}

cl_bitfield! {
	pub bitfield(opencl_ffi::cl_1_0::cl_command_queue_properties) CommandQueueProperties {
		OUT_OF_ORDER_EXECUTION = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE,
		PROFILING_ENABLE = CL_QUEUE_PROFILING_ENABLE
	}
}

#[derive(Debug)]
pub struct CommandQueue<C: SharedAccess<Context>> {
	context: C,
	queue: cl_command_queue
}
impl<C: SharedAccess<Context>> CommandQueue<C> {
	pub fn new(
		context: C,
		device: &DeviceId,
		properties: CommandQueueProperties
	) -> Result<Self, CommandQueueInitError> {
		let queue = unsafe {
			cl_try!(
				noret CommandQueueInitError: clCreateCommandQueue(
					context.handle(),
					device.handle(),
					properties.into_raw()
				)
			)?
		};

		Ok(
			CommandQueue {
				context,
				queue
			}
		)
	}

	pub fn context(&self) -> &C {
		&self.context
	}

	pub fn wait(&mut self) -> Result<(), CommandQueueFinishError> {
		unsafe {
			cl_try!(
				CommandQueueFinishError: clFinish(self.handle())
			)
		}
	}
}
impl<C: SharedAccess<Context>> ClObject for CommandQueue<C> {
	type HandleType = cl_command_queue;
	
	fn handle(&self) -> Self::HandleType {
		self.queue
	}
}
impl_fallible_drop! {
	impl CommandQueue<C> where [C: SharedAccess<Context>] {
		pub fn release(self) -> Result<(), CommandQueueDropError> {
			unsafe {
				cl_try!(
					CommandQueueDropError: clReleaseCommandQueue(self.queue)
				)
			}
		}
	}
}
