## Built-in keys

### #work_name

Arguments: none
Kind: readonly

Name of the work. Corresponds to the key in the `#experiments` dictionary.

### #work_execution_name

Arguments: none
Kind: readonly

Name of the work execution. This encodes all parameter choices and can be used to uniquely identify the work execution within the work (e.g. as a filename).

### #work_directory

Arguments: none
Kind: readonly

Path to the directory created for all output of the work.

### #work_execution_file_path

Arguments: `file_extension`
Kind: readonly

Create a path to a file named `#work_execution_name.file_extension` inside `#work_directory`.

### #fan_speed

Arguments: none
Kind: read/write

Controls fan speed.

### #cpu_profile

Arguments: none
Kind: read/write

Controls which CPU cores are enabled/disabled at the OS level.

### #gpu_profile

Arguments: none
Kind: read/write

Controls which GPU frequency profile is selected at the OS level.
