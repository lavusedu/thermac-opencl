#!/usr/bin/env python3

import sys
import time
import json
import subprocess

RUN_COUNT = 5
def run(x, y):
	command = [
		"./mb2",
		"--platform2", "0",
		"--run-count", str(RUN_COUNT),
		"--run-min-time", "0.0",
		"--work-share-factor", "0.0",
		"--work-share-axis", "x",
		"--width", "3072",
		"--height", "3072",
		"--max-pixel-iter", "3072",
		"--escape-radius", "Inf",
		"--kernel", "pretty",
		"--log-verbosity", "error",
		"--work-group-size-x", str(x),
		"--work-group-size-y", str(y)
	]
	
	time_before = time.clock_gettime(time.CLOCK_MONOTONIC)
	total_work_time = 0.0

	print(f"Running {x}x{y}")
	with subprocess.Popen(
		command, stdout = subprocess.PIPE, stderr = sys.stderr,
		encoding = "utf-8"
	) as proc:
		for line in proc.stdout:
			print("mb2:", line, end = "")
			if line.startswith("work_elapsed="):
				total_work_time += float(line[13:])

	if proc.returncode != 0:
		return (float("nan"), float("nan"))

	time_after = time.clock_gettime(time.CLOCK_MONOTONIC)
	time_elapsed = time_after - time_before

	return (time_elapsed / RUN_COUNT, total_work_time / RUN_COUNT)

def run_all():
	choices = [
		[32, 1], [64, 1], [128, 1], [256, 1], [512, 1], [1024, 1],
		[1, 32], [32, 32], [64, 32], [128, 32], [256, 32], [512, 32], [1024, 32],
		[1, 64], [32, 64], [64, 64], [128, 64], [256, 64], [512, 64], [1024, 64],
		[1, 128], [32, 128], [64, 128], [128, 128], [256, 128], [512, 128], [1024, 128],
		[1, 256], [32, 256], [64, 256], [128, 256], [256, 256], [512, 256], [1024, 256],
		[1, 512], [32, 512], [64, 512], [128, 512], [256, 512], [512, 512], [1024, 512],
		[1, 1024], [32, 1024], [64, 1024], [128, 1024], [256, 1024], [512, 1024], [1024, 1024]
	]

	data = []
	for choice in choices:
		x = choice[0]
		y = choice[1]

		time, work_time = run(x, y)

		dat = {
			"x": x,
			"y": y,
			"time": time,
			"work_time": work_time
		}
		data.append(dat)
		print("Dat:", dat)

	return data

def plot(data):
	import numpy
	import matplotlib.pyplot as plt

	axis_labels = set()
	for dat in data:
		axis_labels.add(dat["x"])
		axis_labels.add(dat["y"])
	axis_labels = sorted(axis_labels)

	x_axis_labels = axis_labels
	y_axis_labels = list(reversed(axis_labels))

	data_matrix = numpy.full(
		(len(x_axis_labels), len(y_axis_labels)),
		numpy.nan, dtype = float
	)
	
	for dat in data:
		x_idx = x_axis_labels.index(dat["x"])
		y_idx = y_axis_labels.index(dat["y"])

		data_matrix[y_idx, x_idx] = dat["time"]

	fig, ax = plt.subplots()
	im = ax.imshow(data_matrix)

	ax.set_xticks(numpy.arange(len(x_axis_labels)), labels = x_axis_labels)
	ax.set_yticks(numpy.arange(len(y_axis_labels)), labels = y_axis_labels)

	cbar = ax.figure.colorbar(im, ax = ax)

	fig.tight_layout()
	plt.show()


if len(sys.argv) > 1:
	if sys.argv[1] == "measure":
		data = run_all()
		with open(sys.argv[2], "w") as file:
			json.dump(data, file)
	elif sys.argv[1] == "plot":
		data = None
		with open(sys.argv[2], "r") as file:
			data = json.load(file)
		plot(data)
else:
	data = run_all()
	plot(data)
