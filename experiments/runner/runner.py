#!/usr/bin/python3

import sys
import json
import types
import argparse
import itertools
import subprocess

import os
import os.path

from abc import ABC

def _fmt_dict(obj):
	if len(obj.keys()) == 0:
		return "{}"

	return "{" + ",".join(
		map(
			lambda kv: f"\n\t{kv[0]}: {kv[1]}",
			obj.items()
		)
	) + "\n}"

def _fmt_command_list(command):
	result = f"[\n\t{command[0]}"
	for element in command[1:]:
		if isinstance(element, str) and element.startswith("--"):
			result += f",\n\t{element}"
		else:
			result += f", {element}"
	
	return result + "\n]"

def _fmt_choices_name(choices):
	def fmt_value(value):
		if isinstance(value, list):
			return "x".join(
				map(str, value)
			)
		
		return value
	
	if len(choices) == 0:
		return "data__profile=default"
	
	return "data__" + "__".join(
		map(
			lambda kv: f"{kv[0]}={fmt_value(kv[1])}",
			choices
		)
	)

class ParamDict:
	def __init__(self, inner = None):
		self.inner = inner
		if self.inner is None:
			self.inner = {}
	
	def __str__(self):
		inner_str = _fmt_dict(self.inner)

		return inner_str

	def __iter__(self):
		return iter(self.inner.items())
	
	def as_dict(self):
		return self.inner
	
	def get(self, key):
		if key not in self.inner:
			raise RuntimeError(f"Invalid key \"{key}\"")

		return self.inner[key]

	def insert(self, key, value):
		self.inner[key] = value

	def remove(self, key):
		if key not in self.inner:
			return None
		
		value = self.inner[value]
		del self.inner[value]

		return value
	
	def copy(self):
		copy = {}
		for key, value in self.inner.items():
			copy[key] = value
		
		return ParamDict(copy)

	def merge(self, other):
		keys = other
		if isinstance(other, ParamDict):
			keys = other.inner
		
		for key, value in keys.items():
			self.insert(key, value)
		
		return self

	def resolve_key(self, key, args):
		value = self.get(key)

		while True:
			if isinstance(value, types.FunctionType):
				return value(self, *args)
			
			if isinstance(value, dict):
				if len(args) < 1:
					raise RuntimeError(f"Argument required to index into a dict.")
				value = value[args[0]]
				args = args[1:]
			
			if isinstance(value, list):
				if len(args) < 1:
					raise RuntimeError(f"Argument required to index into an array.")
				
				index = None
				try:
					index = int(args[0])
				except:
					raise RuntimeError(f"Integer argument required to index into an array.")
				
				value = value[index]
				args = args[1:]

			if len(args) > 0:
				raise RuntimeError(f"Invalid {len(args)} remaining arguments.")
			
			return value
	
	def resolve_definition_value(self, value):
		if isinstance(value, str):
			return value
		
		if isinstance(value, dict):
			if "key" in value:
				args = []
				if "args" in value:
					args = value["args"]
				
				return self.resolve_key(value["key"], args)

		raise RuntimeError(f"Invalid type {type(value)}: expected string or dict with \"key\" property")

class DefinitionBase(ABC):
	def __init__(self, command, parameters, environment):
		self.command = command
		self.parameters = parameters
		self.environment = environment
	
	def prepare_command(self, params):
		result = []
		for value in self.command:
			result.append(
				params.resolve_definition_value(value)
			)
		
		return result
	
	def prepare_environment(self, params):
		result = {}
		for key, value in self.environment.inner.items():
			result[key] = params.resolve_definition_value(value)
		
		return ParamDict(result)

class Runner(DefinitionBase):
	def __str__(self):
		command_str = _fmt_command_list(self.command)

		return f"Runner({command_str}, {self.environment})"

class Experiment(DefinitionBase):
	def __str__(self):
		command_str = _fmt_command_list(self.command)
		return f"Experiment({command_str}, {self.parameters})"

class Measurement(DefinitionBase):
	def __init__(self, command, parameters, environment, runner, experiment, name):
		super(Measurement, self).__init__(command, parameters, environment)
		
		self.runner = runner
		self.experiment = experiment
		self.name = name
		
		self.parameters.insert("#measurement_name", self.name)
		self.parameters.insert("#measurement_directory", os.path.join(".", self.name))
		self.parameters.insert(
			"#measurement_file_path",
			lambda params, ext: os.path.join(
				params.get("#measurement_directory"), params.get("#measurement_name") + f".{ext}"
			)
		)

		self.file_log_stdout = None
		self.file_log_stderr = None
		self.file_log_params = None

	def __str__(self):
		return f"Measurement({self.name},\n{self.runner},\n{self.experiment},\n{self.parameters})"

	def _apply_fan_speed(self, speed):
		self._log(f"Setting fan speed to {speed}")
		subprocess.run(
			["ssh", "imx8fan@c2c1", str(speed)],
			check = True
		)

	def _apply_cpu_profile(self, name):
		PROFILES = {
			"all": [1, 1, 1, 1, 1, 1],
			"little-one": [1, 0, 0, 0, 0, 0],
			"little-all": [1, 1, 1, 1, 0, 0],
			"big-one": [0, 0, 0, 0, 1, 0],
			"big-all": [0, 0, 0, 0, 1, 1]
		}
		profile = PROFILES[name]
		
		def apply_cpu_state(index, state):
			with open(f"/sys/devices/system/cpu/cpu{index}/online", "w") as file:
				print(state, file = file, end = "")
		
		self._log(f"Setting cpu profile to {profile}")
		# first turn on all cpus that are to be turned on so we don't accidentally disable everything
		for (i, v) in enumerate(profile):
			if v == 1:
				apply_cpu_state(i, v)
		# then turn off the remaining cpus
		for (i, v) in enumerate(profile):
			if v == 0:
				apply_cpu_state(i, v)

	def _apply_gpu_profile(self, name):
		PROFILES = {
			"overdrive": "overdrive",
			"nominal": "nominal",
			"underdrive": "underdrive"
		}
		profile = PROFILES[name]
		
		self._log(f"Setting gpu profile to {profile}")
		with open("/sys/bus/platform/drivers/galcore/gpu_govern", "w") as file:
			print(profile, file = file, end = "")
		
	def _apply_proc_scheduler(self, name):
		SCHEDULERS = {
			"fifo": os.SCHED_FIFO,
			"rr": os.SCHED_RR,
			"batch": os.SCHED_BATCH,
			"default": os.SCHED_OTHER
		}
		scheduler = SCHEDULERS[name]

		max_priority = os.sched_get_priority_max(scheduler)
		self._log(f"Setting process scheduler to {scheduler} with priority {max_priority}")
		os.sched_setscheduler(0, scheduler, os.sched_param(max_priority))

	def _apply_proc_niceness(self, value):
		self._log(f"Setting process niceness to {value}")
		os.nice(value)

	def _setup(self, allow_continue):
		measurement_dir = self.parameters.get("#measurement_directory")
		print("Creating directory", measurement_dir)
		os.makedirs(measurement_dir, exist_ok = allow_continue)

		self.file_log_stdout = open(os.path.join(measurement_dir, "stdout.txt"), "a")
		self.file_log_stderr = open(os.path.join(measurement_dir, "stderr.txt"), "a")
		self.file_log_params = open(os.path.join(measurement_dir, "params.txt"), "a")
	
	def _log(self, *args, **kwargs):
		if self.file_log_params is not None:
			kwargs["file"] = self.file_log_params
			print(*args, **kwargs)
		
		kwargs["file"] = sys.stderr
		print(*args, **kwargs)

	def _execute(self, parameters, command, env):
		self._apply_fan_speed(parameters.get("#fan_speen"))
		self._apply_cpu_profile(parameters.get("#cpu_profile"))
		self._apply_gpu_profile(parameters.get("#gpu_profile"))
		self._apply_proc_scheduler(parameters.get("#proc_scheduler"))
		self._apply_proc_niceness(parameters.get("#proc_niceness"))

		# TODO: maybe want? os.sched_setaffinity(0, [1, 2, 3, 4, 5])
		
		command_str = [str(x) for x in command]
		with subprocess.Popen(
			command_str, env = env.as_dict(),
			stdout = self.file_log_stdout, stderr = self.file_log_stderr
		) as proc:
			pass

	def _teardown(self):
		self._apply_fan_speed(0.0)
		self._apply_cpu_profile("all")
		self._apply_gpu_profile("overdrive")

		self.file_log_stdout.close()
		self.file_log_stderr.close()
		self.file_log_params.close()
	
	def _generate_execution_parameters(self):
		parameters = self.parameters.copy()
		
		choice_pool_names = []
		choice_pools = []
		for key, value in parameters:
			if isinstance(value, dict) and "choices" in value:
				choice_pool_names.append(key)
				choice_pools.append(value["choices"])
		
		for choice_draw in itertools.product(*choice_pools):
			choices = list(zip(choice_pool_names, choice_draw))
			name = _fmt_choices_name(choices)

			for key, value in choices:
				parameters.insert(key, value)
			parameters.insert("#measurement_name", name)

			yield self.runner.parameters.copy().merge(self.experiment.parameters).merge(parameters)

	def run(self, dry_run = False, combination_indices = None):
		allow_continue = combination_indices is not None
		
		if not dry_run:
			self._setup(allow_continue = allow_continue)

		combinations = list(self._generate_execution_parameters())
		if combination_indices is None:
			combination_indices = range(len(combinations))
		for i in combination_indices:
			parameters = combinations[i]

			command = self.runner.prepare_command(parameters) + self.experiment.prepare_command(parameters) + self.prepare_command(parameters)
			env = self.runner.prepare_environment(parameters).merge(self.experiment.prepare_environment(parameters)).merge(self.prepare_environment(parameters))
			
			self._log("Measurement execution:")
			self._log("Parameters:", parameters)
			self._log("Command:", " ".join(map(str, command)))
			self._log("Env:", env)
			self._log()
			if not dry_run:
				self._execute(parameters, command, env)

		if not dry_run:
			self._teardown()

def parse_definitions(obj):
	def _iter_definitions(obj):
		for key, value in obj.items():
			if key.startswith("#"):
				continue
			
			yield key, value

	runners = {}
	for key, runner in _iter_definitions(obj["#runners"]):
		runners[key] = Runner(runner["command"], ParamDict(runner["parameters"]), ParamDict(runner["environment"]))

	experiments = {}
	common_params = ParamDict(obj["#experiments"]["#common"]["parameters"])
	common_env = ParamDict(obj["#experiments"]["#common"]["environment"])
	for key, experiment in _iter_definitions(obj["#experiments"]):
		experiments[key] = Experiment(
			experiment["command"],
			common_params.copy().merge(experiment["parameters"]),
			common_env.copy().merge(experiment["environment"])
		)

	measurements = {}
	for key, measurement in _iter_definitions(obj["#measurements"]):
		measurements[key] = Measurement(
			[],
			ParamDict(measurement["parameters"]),
			ParamDict(measurement["environment"]),
			runners[measurement["runner"]],
			experiments[measurement["experiment"]],
			name = key
		)
	
	return measurements

def try_notif(message, dry_run = False):
	options = [
		"./notif.sh",
		"/home/root/stage/notif.sh"
	]
	
	file = None
	for opt in options:
		if os.path.isfile(opt):
			file = opt
			break
	
	if not dry_run:
		if file is None:
			return False
		subprocess.run([file, message], stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)
	else:
		print("Notification:", message, file = sys.stderr)

	return True

def main(
	definitions_path,
	measurement_name = None,
	dry_run = False,
	combination_indices = None
):
	definitions = None
	with open(definitions_path, "r") as file:
		definitions = json.load(file)
	
	measurements = parse_definitions(definitions)

	if measurement_name is None:
		print("Available measurements:")
		for key in measurements.keys():
			print(f"\t{key}")

		return

	measurement = measurements[measurement_name]

	try_notif(f"Starting measurement \"{measurement_name}\" (indices {combination_indices})", dry_run = dry_run)
	measurement.run(dry_run = dry_run, combination_indices = combination_indices)
	try_notif(f"Done measurement \"{measurement_name}\"", dry_run = dry_run)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Script for launching thermac experiments.")

	parser.add_argument(
		"DEFINITIONS",
		help = "Path to definitions file."
	)
	parser.add_argument(
		"--dry-run", dest = "DRY_RUN",
		action = "store_true",
		help = "Don't actually run anything, only print what would happen."
	)
	parser.add_argument(
		"--run-index", "-i", dest = "RUN_INDEX",
		action = "append", type = int,
		help = "If present only run combination at specified index. Can be specified multiple times."
	)
	parser.add_argument(
		"MEASUREMENT",
		default = None, nargs = "?",
		help = "Name of the measurement to execute."
	)

	args = parser.parse_args()
	main(args.DEFINITIONS, args.MEASUREMENT, dry_run = args.DRY_RUN, combination_indices = args.RUN_INDEX)
