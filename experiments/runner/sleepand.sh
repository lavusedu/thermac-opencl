#!/bin/sh

time=$1
shift

printf 'Sleeping for %s...\n' $time
sleep $time

# execute the remaining command
"$@"
