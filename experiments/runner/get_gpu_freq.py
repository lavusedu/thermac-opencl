#!/usr/bin/python3

import sys
import time

GPU_GOVERN_PATH = "/sys/bus/platform/drivers/galcore/gpu_govern"
# GPU_GOVERN_PATH = "./get_gpu_freq_test.txt"

#GPU support 3 modes
#overdrive:	core_clk frequency: 800000000	shader_clk frequency: 1000000000
#nominal:	core_clk frequency: 650000000	shader_clk frequency: 700000000
#underdrive:	core_clk frequency: 400000000	shader_clk frequency: 400000000
#Currently GPU runs on mode overdrive

def initialize():
	mode_lines = []
	with open(GPU_GOVERN_PATH, "r") as file:
		mode_lines = list(file.readlines())
		mode_lines = mode_lines[1:-1]
	
	modes = {}
	for line in mode_lines:
		columns = line.split("\t")

		name = columns[0][:-1].rstrip(":")
		core_clk = int(columns[1].split(" ")[-1].strip())
		shader_clk = int(columns[2].split(" ")[-1].strip())

		modes[name] = {
			"core_clk": core_clk,
			"shader_clk": shader_clk
		}
	
	return modes
	
def measure():
	last_line = None
	with open(GPU_GOVERN_PATH, "r") as file:
		for line in file.readlines():
			last_line = line
	
	last_word = last_line.split(" ")[-1].strip()
	return last_word
		
if __name__ == "__main__":
	modes = initialize()
	while True:
		mode_name = measure()
		core_clk = modes[mode_name]["core_clk"]
		shader_clk = modes[mode_name]["shader_clk"]

		print(f"gpu_core_clk={core_clk}")
		print(f"gpu_shader_clk={shader_clk}")
		sys.stdout.flush()

		# :shrug:
		time.sleep(30)

