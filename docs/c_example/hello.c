// Source: https://github.com/rsnemmen/OpenCL-examples/tree/master/sum_array

#define PROGRAM_FILE "hello.cl"
#define KERNEL_FUNC "hello"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif

const char* load_program() {
	FILE* program_handle;
	char* program_buffer;
	size_t program_size;
	
	/* Read program file and place content into buffer */
	program_handle = fopen(PROGRAM_FILE, "r");
	if(program_handle == NULL) {
		perror("Couldn't find the program file");
		exit(1);
	}
	fseek(program_handle, 0, SEEK_END);
	program_size = ftell(program_handle);
	rewind(program_handle);

	program_buffer = (char*)malloc(program_size + 1);
	program_buffer[program_size] = '\0';

	fread(program_buffer, sizeof(char), program_size, program_handle);
	fclose(program_handle);

	return program_buffer;
}

void print_device_info(cl_device_id device) {
	char output[64] = { 0 }; 

	clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(output), &output, NULL);
	printf("Device name: %s\n", output);

	clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(output), &output, NULL);
	printf("Device vendor: %s\n", output);

	clGetDeviceInfo(device, CL_DEVICE_VERSION, sizeof(output), &output, NULL);
	printf("Device version: %s\n", output);

	clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(output), &output, NULL);
	printf("Driver version: %s\n", output);
}

int main(int argc, char* argv[])
{
	const char* kernel_source = load_program();

	// Length of vectors
	unsigned int n = 128;

	// Host input vectors
	float* h_a;
	float* h_b;
	// Host output vector
	float* h_c;

	// Device input buffers
	cl_mem d_a;
	cl_mem d_b;
	// Device output buffer
	cl_mem d_c;

	cl_platform_id cpPlatform;		  // OpenCL platform
	cl_device_id device_id;			  // device ID
	cl_context context;					// context
	cl_command_queue queue;			  // command queue
	cl_program program;					// program
	cl_kernel kernel;					  // kernel

	// Size, in bytes, of each vector
	size_t bytes = n * sizeof(float);

	// Allocate memory for each vector on host
	h_a = (float*) malloc(bytes);
	h_b = (float*) malloc(bytes);
	h_c = (float*) malloc(bytes);

	// Initialize vectors on host
	for (int i = 0; i < n; i++) {
		h_a[i] = 1;
		h_b[i] = 2;
	}

	size_t globalSize, localSize;
	cl_int err;

	// Number of work items in each local work group
	localSize = 64;

	// Number of total work items - localSize must be devisor
	globalSize = ceil(n / (float)localSize) * localSize;

	// Bind to platform
	err = clGetPlatformIDs(1, &cpPlatform, NULL);
	printf("clGetPlatformIDs() = %d, cpPlatform = %p\n", err, cpPlatform);

	// Get ID for the device
	// err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_ALL, 1, &device_id, NULL);
	printf("clGetDeviceIDs() = %d, device_id = %p\n", err, device_id);
	print_device_info(device_id);

	// Create a context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

	// Create a command queue
	queue = clCreateCommandQueue(context, device_id, 0, &err);

	// Create the compute program from the source buffer
	program = clCreateProgramWithSource(context, 1, (const char **) &kernel_source, NULL, &err);

	// Build the program executable
	clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

	// Create the compute kernel in the program we wish to run
	kernel = clCreateKernel(program, KERNEL_FUNC, &err);

	// Create the input and output arrays in device memory for our calculation
	d_a = clCreateBuffer(context, CL_MEM_READ_ONLY, bytes, NULL, NULL);
	d_b = clCreateBuffer(context, CL_MEM_READ_ONLY, bytes, NULL, NULL);
	d_c = clCreateBuffer(context, CL_MEM_WRITE_ONLY, bytes, NULL, NULL);

	// Write our data set into the input array in device memory
	err = clEnqueueWriteBuffer(queue, d_a, CL_TRUE, 0, bytes, h_a, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(queue, d_b, CL_TRUE, 0, bytes, h_b, 0, NULL, NULL);

	// Set the arguments to our compute kernel
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_a);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_b);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_c);
	err |= clSetKernelArg(kernel, 3, sizeof(unsigned int), &n);

	// Execute the kernel over the entire range of the data set
	printf("global=%zu, local=%zu\n", globalSize, localSize);
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

	// Wait for the command queue to get serviced before reading back results
	clFinish(queue);

	// Read the results from the device
	clEnqueueReadBuffer(queue, d_c, CL_TRUE, 0, bytes, h_c, 0, NULL, NULL);

	printf("Result:");
	for (int i = 0; i < n; i++) {
		printf(" %.1f,", h_c[i]);
	}
	printf("\n");

	printf("Expected:");
	for (int i = 0; i < n; i++) {
		printf(" %.1f,", h_a[i] + h_b[i]);
	}
	printf("\n");

	// release OpenCL resources
	clReleaseMemObject(d_a);
	clReleaseMemObject(d_b);
	clReleaseMemObject(d_c);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);

	//release host memory
	free(h_a);
	free(h_b);
	free(h_c);
	free((void*) kernel_source);

	return 0;
}
