# Thermac OpenCL Experiments

Yocto layer and OpenCL experiments for IMX8 board for the thermac project.

## Contents

### [opencl](opencl/README.md)

### docs

- Documents about the board
- Documents about OpenCL
- Example OpenCL program written in C (link to github source in hello.c).

### [system](system/README.md)

## Thermobench

https://ctu-iig.github.io/thermobench/dev/
https://github.com/CTU-IIG/thermobench

### Fan

ssh imx8fan@c2c1 [0 ..= 1]

### Thermal camera

Thermal camera realtime view is available at https://thermac@imx8cam.iid.ciirc.cvut.cz/
